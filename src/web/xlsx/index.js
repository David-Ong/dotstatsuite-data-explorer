import * as R from 'ramda';
import FileSaver from 'file-saver';
import XlsxPopulate from 'xlsx-populate';

const SHEET1 = 'Table';
const SHEET2 = 'Flags';

const formatFlags = R.pipe(
  R.pluck('label'),
  R.join('\n'),
);

const getPosition = (row, column) => {
  let id = '';
  for (let a = 1, b = 26; (column -= a) >= 0; a = b, b *= 26) {
    id = String.fromCharCode(parseInt((column % b) / a) + 65) + id;
  }
  return `${id}${row}`;
};

const formatSection = R.map(entry => ({
  label: `${R.pathOr('', ['dimension', 'label'], entry)}: ${R.pathOr(
    '',
    ['value', 'label'],
    entry,
  )}`,
  flags: R.pathOr([], ['value', 'flags'], entry),
}));

export const createExcelFile = ({ cells, chartConfig, header, headerData, sectionsData }) => {
  return XlsxPopulate.fromBlankAsync()
    .then(workbook => {
      workbook.addSheet(SHEET1, 0);
      workbook.addSheet(SHEET2, 1);

      const addStyle = (position, style) => {
        workbook
          .sheet(0)
          .cell(position)
          .style(style);

        workbook
          .sheet(1)
          .cell(position)
          .style(style);
      };

      const registerCell = (position, cell, style = {}) => {
        //console.log('position', position);
        //console.log('cell', cell);
        addStyle(position, style);

        workbook
          .sheet(0)
          .cell(position)
          .value(cell.value || cell.label);
        if (!R.isEmpty(R.propOr([], 'flags', cell))) {
          workbook
            .sheet(1)
            .cell(position)
            .value(formatFlags(cell.flags))
            .hyperlink(`Table!${position}`);
          workbook
            .sheet(0)
            .cell(position)
            .style({ ...style, underline: true })
            .hyperlink(`Flags!${position}`);
        }
      };

      const registerSequence = (row, col, values, style) => {
        R.addIndex(R.forEach)((value, index) => {
          registerCell(getPosition(row, col + index), value, style);
        }, values);
      };

      const cellsInRow = R.length(headerData);
      const dimsInSection = R.pipe(
        R.pathOr([], [0, 0, 'data']),
        R.length,
      )(sectionsData);
      const dimsInRow = R.pipe(
        R.pathOr([], [0, 1, 0, 'data']),
        R.length,
      )(sectionsData);
      const tableWidth = R.max(dimsInSection + 2, dimsInRow + cellsInRow + 1);

      registerCell(getPosition(1, 2), R.propOr({}, 'title', header), { bold: true });

      registerSequence(2, 2, R.propOr([], 'subtitle', header));

      workbook
        .sheet(0)
        .cell('B3')
        .value(R.propOr('', 'uprs', header));

      let rowIndex = 5;

      /*
      header
              | dim1 | col1 val | ... | coln val |
                           ...
              | dimn | col1 val | ... | coln val |
      */
      const transposedHeaderData = R.pipe(
        R.pluck('data'),
        R.transpose,
      )(headerData);
      R.forEach(headerRow => {
        const dim = R.pathOr({}, [0, 'dimension'], headerRow);
        const dimWidth = tableWidth - cellsInRow;
        workbook
          .sheet(0)
          .range(`${getPosition(rowIndex, 2)}:${getPosition(rowIndex, dimWidth + 1)}`)
          .merged(true)
          .style({ border: true, bold: true })
          .value(dim.label);

        workbook
          .sheet(1)
          .range(`${getPosition(rowIndex, 2)}:${getPosition(rowIndex, dimWidth + 1)}`)
          .merged(true)
          .style({ border: true, bold: true });

        registerSequence(rowIndex, dimWidth + 2, R.pluck('value', headerRow), { border: true });

        rowIndex++;
      }, transposedHeaderData);

      //rows header | dim1 | ... | dimn| | col1Flags| ... | colnFlags |
      const rowDimensions = R.pipe(
        R.pathOr([], [0, 1, 0, 'data']),
        R.pluck('dimension'),
      )(sectionsData);
      registerSequence(rowIndex, 2, rowDimensions, { border: true, bold: true });
      const rowIntersectionSize = tableWidth - (dimsInRow + cellsInRow);
      if (rowIntersectionSize > 1) {
        workbook
          .sheet(0)
          .range(
            `${getPosition(rowIndex, dimsInRow + 2)}:${getPosition(
              rowIndex,
              dimsInRow + rowIntersectionSize + 1,
            )}`,
          )
          .style({ border: true })
          .merged(true);

        workbook
          .sheet(1)
          .range(
            `${getPosition(rowIndex, dimsInRow + 2)}:${getPosition(
              rowIndex,
              dimsInRow + rowIntersectionSize + 1,
            )}`,
          )
          .style({ border: true })
          .merged(true);
      }
      registerSequence(rowIndex, dimsInRow + rowIntersectionSize + 2, headerData, { border: true });
      rowIndex++;

      /*
        sections
            | section dim 1 val |               ...              | section dim X val | | section flags |
            | row 1 dim 1 val | ... | row 1 dim Y val | row 1 flags | row 1 cell 1| ... | row 1 cell Z |
                                      ...
            | row N dim 1 val | ... | row N dim Y val| row N flags | row N cell 1 | ... | row N cell Z |
      */
      const sectionIntersectionSize = tableWidth - (dimsInSection + 1);
      R.forEach(([section, rows]) => {
        if (!R.isEmpty(R.propOr([], 'data', section))) {
          registerSequence(rowIndex, 2, formatSection(section.data), {
            border: { top: true, bottom: true },
          });
          addStyle(getPosition(rowIndex, 2), { border: { top: true, bottom: true, left: true } });

          workbook
            .sheet(0)
            .range(
              `${getPosition(rowIndex, dimsInSection + 2)}:${getPosition(
                rowIndex,
                dimsInSection + sectionIntersectionSize + 1,
              )}`,
            )
            .merged(true)
            .style({ border: { top: true, bottom: true } });

          workbook
            .sheet(1)
            .range(
              `${getPosition(rowIndex, dimsInSection + 2)}:${getPosition(
                rowIndex,
                dimsInSection + sectionIntersectionSize + 1,
              )}`,
            )
            .merged(true)
            .style({ border: { top: true, bottom: true } });

          registerCell(
            getPosition(rowIndex, dimsInSection + sectionIntersectionSize + 2),
            section,
            { border: { top: true, bottom: true, right: true } },
          );
          rowIndex++;
        }

        R.forEach(row => {
          registerSequence(rowIndex, 2, R.pluck('value', row.data), { border: true });

          workbook
            .sheet(0)
            .range(
              `${getPosition(rowIndex, dimsInRow + 2)}:${getPosition(
                rowIndex,
                dimsInRow + rowIntersectionSize + 1,
              )}`,
            )
            .merged(true)
            .style({ border: true });

          workbook
            .sheet(1)
            .range(
              `${getPosition(rowIndex, dimsInRow + 2)}:${getPosition(
                rowIndex,
                dimsInRow + rowIntersectionSize + 1,
              )}`,
            )
            .merged(true)
            .style({ border: true });

          registerCell(getPosition(rowIndex, dimsInRow + 2), row);

          registerSequence(
            rowIndex,
            dimsInRow + rowIntersectionSize + 2,
            R.map(col => R.pathOr({}, [col.key, section.key, row.key, 0], cells), headerData),
            { border: true },
          );
          rowIndex++;
        }, rows);
      }, sectionsData);

      workbook
        .sheet(0)
        .cell(getPosition(rowIndex + 1, 2))
        .value(`© ${R.path(['terms', 'label'], chartConfig)}`)
        .style({ fontColor: '0563c1', underline: true })
        .hyperlink(R.path(['terms', 'link'], chartConfig));

      workbook.activeSheet(SHEET1);
      return workbook.outputAsync(workbook);
    })
    .then(blob => {
      FileSaver.saveAs(blob, `${R.pathOr('table', ['title', 'label'])(header)}.xlsx`);
    });
};
