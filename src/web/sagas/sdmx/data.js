import * as R from 'ramda';
import { select, put, call, takeLatest } from 'redux-saga/effects';
import { getData } from '@sis-cc/dotstatsuite-sdmxjs';
import { rules } from '@sis-cc/dotstatsuite-components';
import { getDataRequestArgs } from '../../selectors/sdmx';
import {
  HANDLE_DATA,
  REQUEST_DATA,
  PARSE_DATA,
  FLUSH_DATA,
  REQUEST_STRUCTURE,
} from '../../ducks/sdmx';
import { setPending, pushLog } from '../../ducks/app';
import { locales } from '../../lib/settings';

const isDev = process.env.NODE_ENV === 'development';

function* requestData({ shouldRequestStructure }) {
  const pendingId = 'getData';

  if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console

  try {
    yield put({ type: FLUSH_DATA });
    if (shouldRequestStructure) yield put({ type: REQUEST_STRUCTURE });
    const args = yield select(getDataRequestArgs);
    yield put(setPending(pendingId, true));
    const data = yield call(getData, args);
    yield put(setPending(pendingId, false));
    yield put({ type: PARSE_DATA, data, locale: R.prop('locale')(args) });
  } catch (error) {
    yield put(setPending(pendingId, false));

    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data); // eslint-disable-line no-console
      console.log(error.response.status); // eslint-disable-line no-console
      console.log(error.response.headers); // eslint-disable-line no-console
      yield put(pushLog({ type: 'response', log: { ...error.response } }));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request); // eslint-disable-line no-console
      yield put(pushLog({ type: 'request', log: error }));
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message); // eslint-disable-line no-console
      yield put(pushLog({ type: 'error', log: error }));
    }
  }
}

function* parseData({ data, locale }) {
  try {
    yield put({
      type: HANDLE_DATA,
      data: R.prop(
        'data',
        rules.v8Transformer(data, { timeFormat: R.path([locale, 'timeFormat'])(locales) }),
      ),
    });
  } catch (error) {
    yield put(pushLog({ type: 'parse', log: error }));
  }
}

export function* watchRequestData() {
  yield takeLatest(REQUEST_DATA, requestData);
}

export function* watchParseData() {
  yield takeLatest(PARSE_DATA, parseData);
}
