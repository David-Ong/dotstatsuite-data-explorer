import * as R from 'ramda';
import { select, put, call, takeLatest } from 'redux-saga/effects';
import {
  getDataflowStructure,
  parseStructure as sdmxjsParseStructure,
  getDataquery,
} from '@sis-cc/dotstatsuite-sdmxjs';
import { getStructureRequestArgs } from '../../selectors/sdmx';
import { getConstraints } from '../../selectors/router';
import { getVisLayout } from '../../selectors';
import { changeFilter } from '../../ducks/router';
import { HANDLE_STRUCTURE, REQUEST_STRUCTURE, PARSE_STRUCTURE } from '../../ducks/sdmx';
import { setPending, pushLog } from '../../ducks/app';
import { getDefaultSelection } from '../../lib/search/sdmx-constraints-bridge';
import { getDefaultRouterParams } from '../../lib/sdmx';

const isDev = process.env.NODE_ENV === 'development';

function* requestStructure() {
  const pendingId = 'getStructure';

  if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console

  try {
    const args = yield select(getStructureRequestArgs);
    yield put(setPending(pendingId, true));
    const structure = yield call(getDataflowStructure, args);
    yield put(setPending(pendingId, false));
    yield put({ type: PARSE_STRUCTURE, structure, locale: R.prop('locale')(args) });
  } catch (error) {
    yield put(setPending(pendingId, false));

    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data); // eslint-disable-line no-console
      console.log(error.response.status); // eslint-disable-line no-console
      console.log(error.response.headers); // eslint-disable-line no-console
      yield put(pushLog({ type: 'response', log: { ...error.response } }));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request); // eslint-disable-line no-console
      yield put(pushLog({ type: 'request', log: error }));
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message); // eslint-disable-line no-console
      yield put(pushLog({ type: 'error', log: error }));
    }
  }
}

function* parseStructure({ structure, locale }) {
  try {
    const { dimensions, layout, params, selection, name, timePeriod } = sdmxjsParseStructure(
      structure,
      locale,
    );
    const constraints = yield select(getConstraints);
    const currentLayout = yield select(getVisLayout);
    yield put({
      type: HANDLE_STRUCTURE,
      structure: {
        dimensions,
        layout: R.defaultTo(currentLayout, layout),
        params: getDefaultRouterParams(
          params,
          getDataquery(dimensions, getDefaultSelection(dimensions, selection, constraints)),
        ),
        title: name,
        timePeriod,
      },
      pushHistory: '/vis',
    });
    yield put(changeFilter());
  } catch (error) {
    yield put(pushLog({ type: 'parse', log: error }));
  }
}

export function* watchRequestStructure() {
  yield takeLatest(REQUEST_STRUCTURE, requestStructure);
}

export function* watchParseStructure() {
  yield takeLatest(PARSE_STRUCTURE, parseStructure);
}
