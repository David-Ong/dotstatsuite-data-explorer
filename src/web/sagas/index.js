import { all } from 'redux-saga/effects';
import { watchRequestStructure, watchParseStructure } from './sdmx/structure';
import { watchRequestData, watchParseData } from './sdmx/data';

export default function* rootSaga() {
  yield all([watchRequestStructure(), watchParseStructure(), watchRequestData(), watchParseData()]);
}
