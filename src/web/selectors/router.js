import { createSelector } from 'reselect';
import * as R from 'ramda';
import { locales } from '../lib/settings';

//------------------------------------------------------------------------------------------------#0
const getRouter = R.prop('router');

//------------------------------------------------------------------------------------------------#1
export const getParams = createSelector(
  getRouter,
  R.propOr({}, 'params'),
);

export const getLocation = createSelector(
  getRouter,
  R.propOr({}, 'location'),
);

//------------------------------------------------------------------------------------------------#2
export const getTerm = createSelector(
  getParams,
  R.prop('term'),
);

export const getConstraints = createSelector(
  getParams,
  R.propOr({}, 'constraints'),
);

export const getFacet = createSelector(
  getParams,
  R.prop('facet'),
);

export const getFilter = createSelector(
  getParams,
  R.prop('filter'),
);

export const getLocale = createSelector(
  getParams,
  R.prop('locale'),
);

export const getDataflow = createSelector(
  getParams,
  R.prop('dataflow'),
);

export const getDataquery = createSelector(
  getParams,
  R.prop('dataquery'),
);

export const getViewer = createSelector(
  getParams,
  R.propOr('table', 'viewer'),
);

export const getStart = createSelector(
  getParams,
  R.prop('start'),
);

export const getIsRtl = createSelector(
  getLocale,
  localeId =>
    R.pipe(
      R.prop(localeId),
      R.prop('isRtl'),
      R.equals('true'),
    )(locales),
);

export const getLastNObservations = createSelector(
  getParams,
  R.prop('lastNObservations'),
);

export const getMap = createSelector(
  getParams,
  R.prop('map'),
);
