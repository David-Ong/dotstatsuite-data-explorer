import * as R from 'ramda';
import * as S from '../.';

jest.mock('../../lib/settings', () => ({
  getSdmxAttribute: id => {
    switch (id) {
      case 'flags':
        return ['OBS_STATUS'];
      case 'footnotes':
        return ['TIME_FORMAT'];
      case 'prefscale':
        return 'PREF_SCALE';
      case 'decimals':
        return 'DECIMALS';
    }
  },
  theme: { visFont: 'toto' },
}));

describe('selectors | vis', () => {
  describe('getCustomAttributes', () => {
    const obsStatusAttr = {
      id: 'OBS_STATUS',
      values: [{ id: 'B', name: 'Break' }],
    };
    const timeFormatAttr = {
      id: 'TIME_FORMAT',
      values: [{ id: 'P1Y', name: 'Annual' }],
    };
    const decimalsAttr = { id: 'DECIMALS', values: [{ id: '1', name: 'One' }] };
    const prefScaleAttr = {
      id: 'PREF_SCALE',
      values: [{ id: '-3', name: 'Thousand' }],
    };

    test('should get all custom attributes', () => {
      const state = {
        sdmx: {
          data: {
            structure: {
              attributes: {
                observation: [obsStatusAttr, timeFormatAttr, decimalsAttr, prefScaleAttr],
              },
            },
          },
        },
        vis: {
          dimensionGetter: 'label',
        },
      };

      const expectedState = {
        flags: {
          0: { ...obsStatusAttr, label: '[OBS_STATUS]', index: 0, isFootnote: false },
          1: { ...timeFormatAttr, label: '[TIME_FORMAT]', index: 1, isFootnote: true },
        },
        decimals: { ...decimalsAttr, label: '[DECIMALS]', index: 2 },
        prefscale: { ...prefScaleAttr, label: '[PREF_SCALE]', index: 3 },
      };
      expect(S.getCustomAttributes()(state)).toEqual(expectedState);
    });
  });
});
