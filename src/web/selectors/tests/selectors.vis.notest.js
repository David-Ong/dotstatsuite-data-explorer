import * as S from '../.';
import structure from '../../mocks/structures';
import data from '../../mocks/data';
import {
  spaces,
  dataflow,
  layout,
  viewerId,
  actionId,
  period,
  filters,
  selection,
  getters,
  outputDimension,
  dataflow1,
  isTimeDimensionInverted,
  dimensionGetter,
  config,
} from '../../mocks/vis';
import * as R from 'ramda';

describe('selectors | vis | 23', () => {
  describe('getSpaces', () => {
    test('spaces empty', () => {
      expect(
        S.getSpaces()({
          vis: { spaces: { disseminate: {}, ecb: {}, process: {} } },
        }),
      ).toEqual({
        disseminate: {},
        ecb: {},
        process: {},
      });
    });
  });
  describe('getDataflow', () => {
    test('dataflow', () => {
      expect(S.getDataflow()({ vis: { dataflow } })).toEqual({ ...dataflow });
    });
  });
  describe('getStructure', () => {
    test('structure', () => {
      expect(S.getStructure()({ vis: { structure } })).toEqual({
        ...structure,
      });
    });
  });
  describe('getVisSelection', () => {
    test('selection', () => {
      expect(S.getVisSelection()({ vis: { selection } })).toEqual({
        ...selection,
      });
    });
  });
  describe('getVisActiveFilterId', () => {
    test('activeFilterId empty', () => {
      expect(S.getVisActiveFilterId()({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getVisViewerId', () => {
    test('activeFilterId empty', () => {
      expect(S.getVisViewerId()({ vis: {} })).toEqual(undefined);
    });
    test('activeFilterId BarChart', () => {
      expect(S.getVisViewerId()({ vis: { viewerId } })).toEqual(viewerId);
    });
  });
  describe('getVisActionId', () => {
    test('activeFilterId empty', () => {
      expect(S.getVisActionId()({ vis: {} })).toEqual(undefined);
    });
    test('activeFilterId api', () => {
      expect(S.getVisActionId()({ vis: { actionId } })).toEqual('api');
    });
  });
  describe('getVisData', () => {
    test('data', () => {
      expect(S.getVisData()({ vis: { data } })).toEqual(data);
    });
  });
  describe('getIsFull', () => {
    test('is full', () => {
      expect(S.getIsFull()({ vis: { isFull: true } })).toEqual(true);
    });
  });
  describe('getVisLayout', () => {
    test('layout', () => {
      expect(S.getVisLayout()({ vis: { layout } })).toEqual(layout);
    });
  });
  describe('getPeriod', () => {
    test('period', () => {
      expect(S.getPeriod()({ vis: { period } })).toEqual(period);
    });
  });
  describe('getTimeDimensionInverted', () => {
    test('is time dimensions Inverted', () => {
      expect(
        S.getTimeDimensionInverted()({
          vis: { timeDimensionInverted: { TIME_PERIOD: true } },
        }),
      ).toEqual({
        TIME_PERIOD: true,
      });
    });
  });
  describe('getVisDimensionGetter', () => {
    test('format (NAME, CODE, BOTH)', () => {
      expect(S.getVisDimensionGetter()({ vis: { dimensionGetter } })).toEqual(dimensionGetter);
    });
  });
  describe('getVisDimensionFormat', () => {
    // [function anonymous]
    // test('format (NAME, CODE, BOTH)', () => {
    //   expect(S.getVisDimensionFormat()({ vis: { dimensionGetter } })).toEqual(R.flip(R.prop)('label'));
    // });
  });
  describe('getVisDataDimensions', () => {
    test('has no dimensions', () => {
      expect(S.getVisDataDimensions()({ vis: {} })).toEqual({
        many: {},
        one: {},
        zero: {},
      });
    });
    test('has dimensions empty', () => {
      expect(S.getVisDataDimensions()({ vis: { data: {} } })).toEqual({
        many: {},
        one: {},
        zero: {},
      });
    });
    /*test("dimensions", () => {
      expect(S.getVisDataDimensions()({ vis: { data: data } })).toEqual(
        outputDimension
      );
    });*/
  });
  describe('getVisTableLayoutDimensions', () => {
    test('has no dimensions', () => {
      expect(S.getVisTableLayoutDimensions()({ vis: {} })).toEqual({});
    });
    test('has dimensions empty', () => {
      expect(S.getVisTableLayoutDimensions()({ vis: { data: {} } })).toEqual({});
    });
    test('dimensions', () => {
      const expected = {
        LOCATION: {
          id: 'LOCATION',
          isTimePeriod: false,
          label: 'Country',
          value: 'LOCATION',
        },
        SUBJECT: {
          id: 'SUBJECT',
          isTimePeriod: false,
          label: 'Subject',
          value: 'SUBJECT',
        },
      };
      expect(
        S.getVisTableLayoutDimensions()({
          vis: { data: data, selection, dimensionGetter },
        }),
      ).toEqual(expected);
    });
  });
  describe('getVisTableLayout', () => {
    test("layout doesn't exist", () => {
      expect(S.getVisTableLayout()({ vis: {} })).toEqual({
        rows: [],
        header: [],
        sections: [],
      });
    });
    // crash
    // test('layout empty', () => {
    //   expect(S.getVisTableLayout()({ vis: { layout: {}}})).toEqual({x: undefined, "y": [], z: []});
    // });
    test('layout with empty values', () => {
      expect(S.getVisTableLayout()({ vis: { layout } })).toEqual({
        rows: [],
        header: [],
        sections: [],
      });
    });
    /*test("layout with data", () => {
      const layout1 = { rows: ["LOCATION"], header: ["SUBJECT"], sections: [] };
      expect(S.getVisTableLayout()({ vis: { layout: layout1, data } })).toEqual(
        {
          rows: ["LOCATION"],
          header: ["SUBJECT"],
          sections: []
        }
      );
    });*/
  });
  describe('getVisPeriod', () => {
    test('period empty', () => {
      expect(
        S.getVisPeriod()({
          vis: { period: [] },
          config,
        }),
      ).toEqual([]);
    });
    test('period no default period', () => {
      expect(S.getVisPeriod()({ vis: { period: [] }, config: {} })).toEqual(undefined);
    });
    test('period in config', () => {
      expect(
        S.getVisPeriod()({
          vis: { period: [] },
          config,
        }),
      ).toEqual(period);
    });
    test('period', () => {
      expect(S.getVisPeriod()({ vis: { period } })).toEqual(period);
    });
  });
  describe('getEndpoint', () => {
    test('endpoint', () => {
      expect(S.getEndpoint()({ vis: { dataflow, spaces } })).toEqual(
        'http://dotstatcor-dev1.main.oecd.org/Demo2NsiWebserviceDisseminationExternal/rest',
      );
    });
    test('endpoint undefined if only dataflow', () => {
      expect(S.getEndpoint()({ vis: { dataflow } })).toEqual(undefined);
    });
    test('endpoint undefined if spaces empty', () => {
      expect(S.getEndpoint()({ vis: { dataflow, spaces: {} } })).toEqual(undefined);
    });
    test('endpoint undefined if dataflow and spaces are empty', () => {
      expect(S.getEndpoint()({ vis: { dataflow: {}, spaces: {} } })).toEqual(undefined);
    });
  });
  describe('getDataflowQuery', () => {
    const defaultSdmxId = {
      agencyId: undefined,
      code: undefined,
      version: undefined,
    };
    test('dataflow empty', () => {
      expect(S.getDataflowQuery('/', null, defaultSdmxId)({ vis: { dataflow: {} } })).toEqual(
        undefined,
      );
    });
    test('dataflow', () => {
      expect(S.getDataflowQuery('/', null)({ vis: { dataflow } })).toEqual(
        'OECD/Dataflow_GENERAL_DATASET_3DIM_PUBLIC/1.4',
      );
    });
    test('sdmxId', () => {
      const sdmxId = { agencyId: 'toto', code: 'tutu', version: 'tata' };
      expect(S.getDataflowQuery('/', null, sdmxId)({ vis: {} })).toEqual('toto/tutu/tata');
    });
  });
  describe('getReferencePartial', () => {
    test('has Reference partial', () => {
      expect(S.getReferencePartial()({ vis: { dataflow: dataflow1, spaces } })).toEqual(
        '&detail=referencepartial',
      );
    });
    test("don't has Reference partial", () => {
      expect(S.getReferencePartial()({ vis: { dataflow, spaces } })).toEqual('');
    });
  });
  describe('getStructureQuery', () => {
    test('structure Query ', () => {
      expect(S.getStructureQuery()({ vis: { dataflow, spaces } })).toEqual(
        'http://dotstatcor-dev1.main.oecd.org/Demo2NsiWebserviceDisseminationExternal/rest/dataflow/OECD/Dataflow_GENERAL_DATASET_3DIM_PUBLIC/1.4/?references=all',
      );
    });
  });
  describe('getDataQuery', () => {
    test('data Query', () => {
      const vis = { dataflow, spaces, selection, filters, period };
      const expectedQuery =
        'http://dotstatcor-dev1.main.oecd.org/Demo2NsiWebserviceDisseminationExternal/rest/data/OECD,Dataflow_GENERAL_DATASET_3DIM_PUBLIC,1.4/.EUR...E+R/?startPeriod=1998&endPeriod=2016';
      expect(S.getDataQuery()({ vis })).toEqual(expectedQuery);
    });
    test('data Query with argument uri is undefined', () => {
      const vis = { dataflow, spaces, selection, filters, period };
      const _config = { isApi: true, isFull: true, uri: undefined };
      const expectedQuery =
        'http://dotstatcor-dev1.main.oecd.org/Demo2NsiWebserviceDisseminationExternal/rest/data/OECD,Dataflow_GENERAL_DATASET_3DIM_PUBLIC,1.4/all/?dimensionAtObservation=AllDimensions';
      expect(S.getDataQuery(_config)({ vis })).toEqual(expectedQuery);
    });
    test('data Query with no selection', () => {
      expect(
        S.getDataQuery()({
          vis: { dataflow, spaces, filters, period },
          config,
        }),
      ).toEqual(
        'http://dotstatcor-dev1.main.oecd.org/Demo2NsiWebserviceDisseminationExternal/rest/data/OECD,Dataflow_GENERAL_DATASET_3DIM_PUBLIC,1.4/all/?startPeriod=1998&endPeriod=2016',
      );
    });
  });
  describe('getDataApiParams', () => {
    const vis = { dataflow, spaces, selection, filters, period };
    const config = { 'sdmx.range': [1, 10] };
    const i18n = { locale: 'en' };
    test('should download without range 1', () => {
      const expectApiparam = {
        headers: {
          Accept: 'application/vnd.sdmx.data+csv;file=true',
          'Accept-Language': 'en',
        },
        responseType: 'blob',
        url:
          'http://dotstatcor-dev1.main.oecd.org/Demo2NsiWebserviceDisseminationExternal/rest/data/OECD,Dataflow_GENERAL_DATASET_3DIM_PUBLIC,1.4/all/?dimensionAtObservation=AllDimensions',
      };
      const args = {
        asCSV: true,
        isFull: true,
        spaceId: 'disseminate',
        dataflowId: null,
      };
      expect(S.getDataApiParams({ ...args })({ i18n, vis, config })).toEqual(expectApiparam);
    });
    test('should download without range 2', () => {
      const expectApiparam = {
        headers: {
          Accept: 'application/vnd.sdmx.data+csv;file=true',
          'Accept-Language': 'en',
        },
        responseType: 'blob',
        url:
          'http://dotstatcor-dev1.main.oecd.org/Demo2NsiWebserviceDisseminationExternal/rest/data/OECD,Dataflow_GENERAL_DATASET_3DIM_PUBLIC,1.4/all/?dimensionAtObservation=AllDimensions',
      };
      const args = {
        asCSV: true,
        isFull: true,
        spaceId: 'FEDev1',
        dataflowId: null,
      };
      expect(S.getDataApiParams({ ...args })({ i18n, vis, config })).toEqual(expectApiparam);
    });
    test('should fetch data with range, ', () => {
      const expectApiparam = {
        headers: {
          Accept: 'application/vnd.sdmx.data+json;version=1.0.0-wd',
          'Accept-Language': 'en',
          Range: 'values=1-10',
        },
        responseType: null,
        url:
          'http://dotstatcor-dev1.main.oecd.org/Demo2NsiWebserviceDisseminationExternal/rest/data/OECD,Dataflow_GENERAL_DATASET_3DIM_PUBLIC,1.4/all/?dimensionAtObservation=AllDimensions',
      };
      const args = {
        asCSV: false,
        isFull: true,
        spaceId: 'FEDev1',
        dataflowId: null,
      };
      expect(
        S.getDataApiParams({ ...args })({
          i18n,
          vis: { ...vis, dataflow: dataflow1 },
          config,
        }),
      ).toEqual(expectApiparam);
    });
    test('data api param isfull false', () => {
      const expectApiparam = {
        headers: {
          Accept: 'application/vnd.sdmx.data+json;version=1.0.0-wd',
          'Accept-Language': 'en',
          Range: 'values=1-10',
        },
        responseType: null,
        url:
          'http://dotstatcor-dev1.main.oecd.org/Demo2NsiWebserviceDisseminationExternal/rest/data/OECD,Dataflow_GENERAL_DATASET_3DIM_PUBLIC,1.4/.EUR...E+R/?startPeriod=1998&endPeriod=2016&dimensionAtObservation=AllDimensions',
      };
      const args = {
        asCSV: false,
        isFull: false,
        spaceId: 'FEDev1',
        dataflowId: null,
      };
      expect(
        S.getDataApiParams({ ...args })({
          i18n,
          vis: { ...vis, dataflow: dataflow1 },
          config,
        }),
      ).toEqual(expectApiparam);
    });
  });
  describe('getVisFilters', () => {
    test('filters', () => {
      expect(S.getVisFilters()({ vis: { filters } })).toEqual([0, 1, 2, 3, 4]);
    });
    test('filters empty', () => {
      expect(S.getVisFilters()({ vis: { filters: [] } })).toEqual([]);
    });
  });
  describe('getVisFiltersCurrent', () => {
    test('filters current with one values', () => {
      const filters = [
        {
          id: 'FREQ',
          index: 0,
          label: 'Frequency',
          roles: [],
          tag: '0/1',
          values: [{ id: 'A', parent: undefined, label: 'Annual' }],
        },
      ];
      const selection = {
        '1281accbcedc6ddd8f36508b71ae6043': { filterIndex: 0, valueId: 'A' },
      };
      const expectedFiltersCurrent = [
        {
          id: 'FREQ',
          index: 0,
          label: 'Frequency',
          roles: [],
          tag: '0/1',
          values: [{ id: 'A', label: 'Annual', parent: undefined }],
        },
      ];
      expect(S.getVisFiltersCurrent()({ vis: { filters, selection } })).toEqual(
        expectedFiltersCurrent,
      );
    });
    test('filters current with many values', () => {
      const filters = [
        {
          id: 'COU',
          index: 0,
          roles: [],
          tag: '4/40',
          hasSpotlight: true,
          values: [
            {
              id: 'AUS',
              label: 'Australia',
            },
            {
              id: 'AUT',
              label: 'Austria',
            },
            {
              id: 'BEL',
              label: 'Belgium',
            },
          ],
          label: 'Country',
        },
      ];

      const selection = {
        '89704d57e3313b61d1976cff9ac41912': { filterIndex: 0, valueId: 'BEL' },
        '47131e19de031138bcd160a7bee1f799': { filterIndex: 0, valueId: 'AUT' },
        db1dd0499e4b2271617e4efd3ac4615e: { filterIndex: 0, valueId: 'AUS' },
      };

      const expectedFiltersCurrent = [
        {
          hasSpotlight: true,
          id: 'COU',
          index: 0,
          label: 'Country',
          roles: [],
          tag: '4/40',
          values: [
            { id: 'BEL', label: 'Belgium' },
            { id: 'AUT', label: 'Austria' },
            { id: 'AUS', label: 'Australia' },
          ],
        },
      ];
      expect(S.getVisFiltersCurrent()({ vis: { filters, selection } })).toEqual(
        expectedFiltersCurrent,
      );
    });
    test('filters current but the filters values is wrong (one element)', () => {
      const filters = [
        {
          id: 'FREQ',
          index: 0,
          label: 'Frequency',
          roles: [],
          tag: '0/1',
          values: [{ id: 'A', parent: undefined, label: 'Annual' }],
        },
      ];
      const selection = {
        '1281accbcedc6ddd8f36508b71ae6043': { filterIndex: 0, valueId: 'WRONG' },
      };
      const expectedFiltersCurrent = [
        {
          id: 'FREQ',
          index: 0,
          label: 'Frequency',
          roles: [],
          tag: '0/1',
          values: [],
        },
      ];
      expect(S.getVisFiltersCurrent()({ vis: { filters, selection } })).toEqual(
        expectedFiltersCurrent,
      );
    });
    test('filters current but the filters values is wrong (many element)', () => {
      const filters = [
        {
          id: 'REF_AREA',
          index: 0,
          roles: [],
          tag: '0/26',
          hasSpotlight: true,
          values: [
            {
              id: 'ASIKHM',
              label: 'Cambodia',
            },
            {
              id: 'ASIKHM001',
              parent: 'ASIKHM',
              label: 'Banteay Meanchey',
            },
            {
              id: 'ASIKHM002',
              parent: 'ASIKHM',
              label: 'Battambang',
            },
            {
              id: 'ASIKHM003',
              parent: 'ASIKHM',
              label: 'Kampong Cham',
            },
          ],
          label: 'Reference area',
        },
      ];
      const selection = {
        '3364dd048452c323034d03d6d691a204': { filterIndex: 0, valueId: 'POL' },
        eafce1b9fa1a590a78e9b361ddd22a52: { filterIndex: 0, valueId: 'TUR' },
        f5ad4b3eedb830aa3377eafd4966c86c: { filterIndex: 0, valueId: 'USA' },
        '6b9444f3ffaf56b79cdef9008b240e38': { filterIndex: 0, valueId: 'OECD' },
      };
      const expectedFiltersCurrent = [
        {
          hasSpotlight: true,
          id: 'REF_AREA',
          index: 0,
          label: 'Reference area',
          roles: [],
          tag: '0/26',
          values: [],
        },
      ];
      expect(S.getVisFiltersCurrent()({ vis: { filters, selection } })).toEqual(
        expectedFiltersCurrent,
      );
    });
    test('filters current but the middle selection id is wrong', () => {
      const filters = [
        {
          id: 'COU',
          index: 0,
          roles: [],
          tag: '4/40',
          hasSpotlight: true,
          values: [
            {
              id: 'AUS',
              label: 'Australia',
            },
            {
              id: 'AUT',
              label: 'Austria',
            },
            {
              id: 'BEL',
              label: 'Belgium',
            },
          ],
          label: 'Country',
        },
      ];

      const selection = {
        '89704d57e3313b61d1976cff9ac41912': { filterIndex: 0, valueId: 'BEL' },
        '47131e19de031138bcd160a7bee1f799': {
          filterIndex: 0,
          valueId: 'WRONG',
        },
        db1dd0499e4b2271617e4efd3ac4615e: { filterIndex: 0, valueId: 'AUS' },
      };

      const expectedFiltersCurrent = [
        {
          hasSpotlight: true,
          id: 'COU',
          index: 0,
          label: 'Country',
          roles: [],
          tag: '4/40',
          values: [{ id: 'BEL', label: 'Belgium' }, { id: 'AUS', label: 'Australia' }],
        },
      ];
      expect(S.getVisFiltersCurrent()({ vis: { filters, selection } })).toEqual(
        expectedFiltersCurrent,
      );
    });
    test('filters current empty', () => {
      expect(S.getVisFiltersCurrent()({ vis: { filters: [] } })).toEqual([]);
    });
  });
  describe('getDataFromSelection', () => {
    test('sdmx data request string from selection hash (warning: value order is luck)', () => {
      expect(S.getDataFromSelection()({ vis: { selection, filters } })).toEqual('.EUR...E+R');
    });
    test('sdmx data request string from selection hash that is empty', () => {
      expect(S.getDataFromSelection()({ vis: { selection: {}, filters } })).toEqual(undefined);
    });
    test('sdmx data request string from selection hash that no exist', () => {
      expect(S.getDataFromSelection()({ vis: {} })).toEqual(undefined);
    });
    test('filters no exist return data from dataflow', () => {
      expect(
        S.getDataFromSelection()({
          vis: { selection, dataflow: { data: '..' } },
        }),
      ).toEqual('..');
    });
  });
  describe('getDataflowAsString', () => {
    const vis = {
      spaces,
      dataflow,
      layout,
      viewerId,
      actionId,
      period,
      filters,
      selection,
      getters,
    };
    test('transform data flow to string but dataflow unexist', () => {
      expect(S.getDataflowAsString()({ vis: {} })).toEqual('//');
    });
    test('transform data flow to string but dataflow is empty', () => {
      expect(S.getDataflowAsString()({ vis: { dataflow: {} } })).toEqual('//');
    });
    test('transform data flow to string', () => {
      expect(S.getDataflowAsString()({ vis })).toEqual(
        'Dataflow_GENERAL_DATASET_3DIM_PUBLIC/OECD/1.4',
      );
    });
    test('should transform data flow to string with name of dataflow', () => {
      expect(S.getDataflowAsString({ withName: true })({ vis })).toEqual(
        'GENERAL_DATASET_3DIM_PUBLIC Dataflow_GENERAL_DATASET_3DIM_PUBLIC/OECD/1.4',
      );
    });
  });
  describe('getCustomAttributes', () => {
    const obsStatusAttr = {
      id: 'OBS_STATUS',
      values: [{ id: 'B', name: 'Break' }],
    };
    const timeFormatAttr = {
      id: 'TIME_FORMAT',
      values: [{ id: 'P1Y', name: 'Annual' }],
    };
    const decimalsAttr = { id: 'DECIMALS', values: [{ id: '1', name: 'One' }] };
    const prefScaleAttr = {
      id: 'PREF_SCALE',
      values: [{ id: '-3', name: 'Thousand' }],
    };

    xit('should get all custom attributes', () => {
      const state = {
        vis: {
          data: {
            structure: {
              attributes: {
                observation: [obsStatusAttr, timeFormatAttr, decimalsAttr, prefScaleAttr],
              },
            },
          },
        },
        config,
      };
      const expectedState = {
        flags: {
          0: { ...obsStatusAttr, index: 0, isFootnote: false },
          1: { ...timeFormatAttr, index: 1, isFootnote: true },
        },
        decimals: { ...decimalsAttr, index: 2 },
        prefscale: { ...prefScaleAttr, index: 3 },
      };
      expect(S.getCustomAttributes()(state)).toEqual(expectedState);
    });

    xit('should get footnote over flag', () => {
      const state = {
        vis: {
          data: {
            structure: {
              attributes: { observation: [obsStatusAttr, timeFormatAttr] },
            },
          },
        },
        config,
      };
      const expectedState = {
        flags: {
          0: { ...obsStatusAttr, index: 0, isFootnote: true },
          1: { ...timeFormatAttr, index: 1, isFootnote: true },
        },
      };
      expect(S.getCustomAttributes()(state)).toEqual(expectedState);
    });
  });
});
