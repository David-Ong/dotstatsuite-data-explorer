import * as S from '../.';
import * as Sdmx from '../sdmx';
import * as R from 'ramda';

jest.mock('../../lib/settings', () => ({
  sdmxPeriod: ['2010', '2015'],
  theme: { visFont: 'totot' },
}));

describe('selectors | for data and structure', () => {
  describe('getDataDimensions', () => {
    test("no crash if data doesn't exist", () => {
      expect(S.getDataDimensions()({})).toEqual([]);
    });
    test("no crash if values doesn't have values", () => {
      const data = { structure: { dimensions: { observation: [{}] } } };
      const expected = [{ index: 0, values: [] }];
      expect(S.getDataDimensions()({ sdmx: { data } })).toEqual(expected);
    });
    test('add index to dimensions', () => {
      const data = { structure: { dimensions: { observation: [{ values: [{}, {}] }] } } };
      const expected = [{ index: 0, values: [{ index: 0 }, { index: 1 }] }];
      expect(S.getDataDimensions()({ sdmx: { data } })).toEqual(expected);
    });
  });
  describe('getVisDataDimensions', () => {
    test('separate one values and many values in two groups', () => {
      const data = {
        structure: {
          dimensions: {
            observation: [{ id: 'test1', values: [{}, {}] }, { id: 'test2', values: [{}] }],
          },
        },
      };
      const expected = {
        many: { test1: { id: 'test1', index: 0, values: [{ index: 0 }, { index: 1 }] } },
        one: { test2: { id: 'test2', index: 1, values: [{ index: 0 }] } },
      };
      expect(S.getVisDataDimensions()({ sdmx: { data } })).toEqual(expected);
    });
  });
  describe('getFrequency', () => {
    const state = (dimensions, dataquery) => ({
      sdmx: { dimensions },
      router: { params: { dataquery } },
    });
    test('get Frequency from dataquery', () => {
      expect(Sdmx.getFrequency(state([], undefined))).toEqual(undefined);
      expect(Sdmx.getFrequency(state([{}, {}, { id: 'FREQ' }, {}], '..A.'))).toEqual('A');
      expect(Sdmx.getFrequency(state([{}, {}, {}, { id: 'FREQ' }], '...B'))).toEqual('B');
      expect(Sdmx.getFrequency(state([{}, {}, {}, { id: 'FREQ' }], '...'))).toEqual(undefined);
    });
    test('get Frequency from frequency dimensions', () => {
      const freq1 = { id: 'FREQ', values: [{ id: 'A', isDefaultSelected: true }] };
      expect(Sdmx.getFrequency(state([{}, {}, freq1, {}], '....'))).toEqual('A');
      const freq2 = { id: 'FREQ', values: [{ id: 'A', isDefaultSelected: false }] };
      expect(Sdmx.getFrequency(state([{}, {}, freq2, {}], '....'))).toEqual(undefined);
      const freq3 = {
        id: 'FREQ',
        values: [{ id: 'A', isDefaultSelected: false }, { id: 'C', isDefaultSelected: true }],
      };
      expect(Sdmx.getFrequency(state([{}, {}, freq3, {}], '....'))).toEqual('C');
    });
  });
  describe('getPeriod', () => {
    const state = (period, freq = 'A') => ({
      sdmx: { dimensions: [{ id: 'FREQ' }] },
      router: { params: { dataquery: freq, period } },
    });
    it('should fallback to boundaries', () => {
      expect(Sdmx.getPeriod(state())).toEqual(['2010', '2015']);
      expect(Sdmx.getPeriod(state([], 'Q'))).toEqual(['2010-Q1', '2015-Q1']);
    });
  });
});
