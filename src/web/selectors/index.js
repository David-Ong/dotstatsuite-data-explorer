import { createSelector } from 'reselect';
import * as R from 'ramda';
import { dimensionFormats, withIndex } from '../utils';
import { getLocale, getDataflow, getDataquery, getViewer } from './router';
import { getData } from './sdmx';
import * as Settings from '../lib/settings';
import * as Viewer from '../lib/viewer';
import * as Layout from '../lib/layout';
import { rules } from '@sis-cc/dotstatsuite-components';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';

//-----------------------------------------------------------------------------------------------vis
const getVisState = R.prop('vis');
export const getVisActionId = () =>
  createSelector(
    getVisState,
    R.prop('actionId'),
  );
export const getIsFull = () =>
  createSelector(
    getVisState,
    R.prop('isFull'),
  );
export const getVisLayout = createSelector(
  getVisState,
  R.prop('layout'),
);
export const getTimeDimensionInverted = () =>
  createSelector(
    getVisState,
    R.prop('timeDimensionInverted'),
  );
export const getShareState = () =>
  createSelector(
    getVisState,
    R.prop('share'),
  );
export const getDataflowTitle = createSelector(
  getVisState,
  R.prop('title'),
);
export const getShareMode = () =>
  createSelector(
    getShareState(),
    R.prop('mode'),
  );
export const getShareMail = () =>
  createSelector(
    getShareState(),
    R.prop('mail'),
  );
export const getIsSharing = () =>
  createSelector(
    getShareState(),
    R.prop('isSharing'),
  );
export const getHasShared = () =>
  createSelector(
    getShareState(),
    R.prop('hasShared'),
  );
export const getShareError = () =>
  createSelector(
    getShareState(),
    R.prop('error'),
  );

export const getShareConfig = () =>
  createSelector(
    [getDataApiParams({ asCSV: false, isFull: false })],
    request => ({
      ...Viewer.options,
      sourceHeaders: R.prop('headers', request),
      source: R.prop('url', request),
    }),
  );
export const getShareLocale = () =>
  createSelector(
    getLocale,
    R.flip(R.prop)(Settings.locales),
  );
export const getShareOptions = () =>
  createSelector(
    [getViewer, getShareConfig(), getShareMode(), getVisDimensionGetter()],
    (type, config, mode, display) => ({
      type,
      endpoint: Settings.share.endpoint,
      config,
      mode,
      display,
    }),
  );
export const getVisTableData = () =>
  createSelector(
    getTimeDimensionInverted(),
    getData,
    getVisLayout,
    (timeDimensionInverted, data, layout) => {
      const tableData = R.uncurryN(2, rules.getTableData)(timeDimensionInverted, data);
      const lastRowId = R.pipe(
        R.propOr([], 'rows'),
        R.last,
      )(layout);
      return R.when(
        R.always(R.not(R.isNil(lastRowId))),
        R.over(R.lensPath(['dimensions', lastRowId]), SDMXJS.withFlatHierarchy),
      )(tableData);
    },
  );
export const getVisDimensionGetter = () =>
  createSelector(
    getVisState,
    R.prop('dimensionGetter'),
  );
export const getVisDimensionFormat = () =>
  createSelector(
    [getVisDimensionGetter()],
    R.flip(R.prop)(dimensionFormats),
  );
export const getDataDimensions = () =>
  createSelector(
    [getData],
    R.pipe(
      R.pathOr([], ['structure', 'dimensions', 'observation']),
      R.addIndex(R.map)((dimension, index) =>
        R.pipe(
          R.assoc('index', index),
          R.set(R.lensProp('values'), withIndex(R.propOr([], 'values')(dimension))),
        )(dimension),
      ),
    ),
  );
export const getVisDataDimensions = () =>
  createSelector(
    getDataDimensions(),
    R.pipe(
      R.partition(
        R.pipe(
          R.propOr([], 'values'),
          R.length,
          R.flip(R.gt)(1),
        ),
      ),
      R.converge(
        (many, one) => ({
          many: R.indexBy(R.prop('id'))(many),
          one: R.indexBy(R.prop('id'))(one),
        }),
        [R.head, R.last],
      ),
    ),
  );
export const getTableConfigDimensions = () =>
  createSelector(
    getVisDataDimensions(),
    ({ many, one }) => R.mergeRight(Layout.oneFormat(one), Layout.manyFormat(many)),
  );

export const getVisTableConfigLayout = () =>
  createSelector(
    [getVisLayout, getVisDataDimensions()],
    (visLayout = {}, { many, one }) => {
      const layout = R.mergeRight({ sections: [], rows: [], header: [] }, visLayout);
      const oneIds = R.keys(one);
      const dimensionsIds = R.concat(R.keys(many), oneIds);
      const getMissingIds = R.difference(dimensionsIds);
      const layoutIds = Layout.getValuesFlat(layout);
      if (Layout.isInvalid(dimensionsIds, layoutIds, layout))
        return Layout.getDefaultLayout(many, getMissingIds);

      const missingIds = getMissingIds(layoutIds);
      if (R.isEmpty(missingIds)) return Layout.adjustment(oneIds, layout);

      return Layout.getLayout(missingIds, oneIds)(layout);
    },
  );

export const getVisDimensionLayout = createSelector(
  getTableConfigDimensions(),
  getVisTableConfigLayout(),
  (dimension, layout) => {
    if (R.isEmpty(dimension)) return {};
    return R.map(R.map(R.flip(R.prop)(dimension)), layout);
  },
);
export const getVisTableLayout = () =>
  createSelector(
    [getVisTableConfigLayout(), getVisDataDimensions()],
    (layout, { many }) => {
      const idsNotInMany = R.difference(Layout.getValuesFlat(layout), R.keys(many));
      return R.map(R.flip(R.difference)(idsNotInMany))(layout);
    },
  );

export const getVisChoroMap = createSelector(
  getVisState,
  R.prop('map'),
);
export const getVisIsLoadingMap = createSelector(
  getVisState,
  R.prop('isLoadingMap'),
);

export const getEndpoint = spaceId =>
  createSelector(
    [getDataflow],
    ({ datasourceId } = {}) => {
      if (R.and(R.isNil(spaceId), R.isNil(datasourceId))) throw new Error('No datasource');
      return Settings.getDatasource(spaceId ? spaceId : datasourceId).url;
    },
  );
export const getReferencePartial = () =>
  createSelector(
    getDataflow,
    ({ datasourceId } = {}) => {
      if (R.isNil(datasourceId)) throw new Error('No datasource');
      const datasource = Settings.getDatasource(datasourceId);
      return R.propEq('supportsReferencePartial', true, datasource)
        ? '&detail=referencepartial'
        : '';
    },
  );
export const getDataflowQuery = (joint = '/', defaultVersion = 'latest', sdmxId) =>
  createSelector(
    [getDataflow],
    ({ agencyId = '', code = '', version = '' } = {}) => {
      if (R.all(R.complement(R.isEmpty), [agencyId, code, version])) {
        return getFormatedDataflowQuery({
          agencyId,
          code,
          version,
          joint,
          defaultVersion,
        });
      }
      if (R.all(R.isNil, R.props(['agencyId', 'code', 'version'], sdmxId))) return undefined;
      return getFormatedDataflowQuery({ ...sdmxId, joint, defaultVersion });
    },
  );
export const getFormatedDataflowQuery = ({ agencyId, code, version, joint, defaultVersion }) =>
  R.pipe(
    R.reject(R.isNil),
    R.join(joint),
  )([agencyId, code, R.defaultTo(defaultVersion, version)]);
export const getDataApiParams = ({ asCSV }) =>
  createSelector(
    //[getLocale, getDataQuery({ isApi: true, isFull, spaceId, agencyId, code, version })],
    [getLocale, getDataquery],
    (locale, url) => {
      const hasRangeHeader = false; //Settings.getDatasource(spaceId).hasRangeHeader;
      const request = {
        url,
        headers: {
          Accept: asCSV
            ? 'application/vnd.sdmx.data+csv;file=true'
            : 'application/vnd.sdmx.data+json;version=1.0.0-wd',
          'Accept-Language': locale,
          Range: `values=${R.head(Settings.sdmxRange)}-${R.last(Settings.sdmxRange)}`,
        },
        responseType: asCSV ? 'blob' : null, // Edge does not understand axios' responseType for JSON
      };
      return R.or(asCSV, !hasRangeHeader) ? R.dissocPath(['headers', 'Range'], request) : request;
    },
  );

const is = id =>
  R.pipe(
    R.prop('id'),
    R.flip(R.contains)(Settings.getSdmxAttribute(id)),
  );
export const getCustomAttributes = () =>
  createSelector(
    getData,
    getVisDimensionFormat(),
    (data, format) =>
      R.pipe(
        R.pathOr([], ['structure', 'attributes', 'observation']),
        R.addIndex(R.reduce)(
          (acc, attr, index) => {
            const indexedAttr = R.pipe(
              R.assoc('index')(index),
              R.assoc('label')(format(attr)),
            )(attr);
            const flags = R.prop('flags')(acc);
            const isFootnoted = is('footnotes')(attr);
            return {
              ...acc,
              prefscale: R.ifElse(is('prefscale'), R.identity, R.always(R.prop('prefscale')(acc)))(
                indexedAttr,
              ),
              decimals: R.ifElse(is('decimals'), R.identity, R.always(R.prop('decimals')(acc)))(
                indexedAttr,
              ),
              flags: R.or(is('flags')(indexedAttr), isFootnoted)
                ? R.assoc(index, R.assoc('isFootnote', isFootnoted, indexedAttr), flags)
                : flags,
            };
          },
          { flags: {} },
        ),
      )(data),
  );

export const getTablePreparedData = () =>
  createSelector(
    getData,
    data => rules.prepareData({ data }, Settings.customAttributes),
  );

export const getHeaderProps = () =>
  createSelector(
    getTablePreparedData(),
    getDataflow,
    getData,
    getVisDimensionGetter(),
    ({ dataflowName, dataflowAttributes, dimensions }, dataflow, data, display) => ({
      ...rules.getHeaderProps({
        dataflowName,
        dataflowCode: dataflow.dataflowId,
        dataflowAttributes,
        display,
        customAttributes: Settings.customAttributes,
        oneValueDimensions: dimensions.one,
      }),
      uprs: rules.getUprs(data, display),
    }),
  );

export const getTableProps = () =>
  createSelector(
    getTablePreparedData(),
    getVisTableLayout(),
    getVisDimensionGetter(),
    (data, layoutIds, display) =>
      rules.getTableProps(data, layoutIds, display, Settings.customAttributes, Settings.cellsLimit),
  );
