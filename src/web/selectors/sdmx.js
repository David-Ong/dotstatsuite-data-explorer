import { createSelector } from 'reselect';
import * as R from 'ramda';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import Set from 'es6-set';
import dateFns from 'date-fns';
import fr from 'date-fns/locale/fr';
import en from 'date-fns/locale/en';
import es from 'date-fns/locale/es';
import ar from 'date-fns/locale/ar';
import it from 'date-fns/locale/it';
import { getDataquery, getDataflow } from './router';
import { rules } from '@sis-cc/dotstatsuite-components';
import { getSdmxPeriod, getDateFromSdmxPeriod } from '../lib/sdmx/frequency';
import { getDatasource, sdmxRange, locales } from '../lib/settings';
import { getLastNObservations, getLocale, getParams } from './router';
import {
  getUsedFilterPeriod as getFilterPeriod,
  getUsedFilterFrequency as getFilterFrequency,
} from '../utils/used-filter';
import { defaultFrequency, sdmxPeriod } from '../lib/settings';
//------------------------------------------------------------------------------------------------#0
const getSdmx = R.prop('sdmx');
//------------------------------------------------------------------------------------------------#1
export const getDimensions = createSelector(
  getSdmx,
  R.prop('dimensions'),
);

export const getData = createSelector(
  getSdmx,
  R.prop('data'),
);

export const getTimePeriod = createSelector(
  getSdmx,
  R.prop('timePeriod'),
);

//------------------------------------------------------------------------------------------------#3
export const getFilters = createSelector(
  getDimensions,
  getDataquery,
  R.pipe(
    R.useWith(
      (filters, dataquery) =>
        R.addIndex(R.map)((filter, index) => {
          if (
            R.pipe(
              R.nth(index),
              R.anyPass([R.isEmpty, R.isNil]),
            )(dataquery)
          )
            return filter;
          const valueIdsSet = new Set(
            R.pipe(
              R.nth(index),
              R.split('+'),
            )(dataquery),
          );
          return R.over(
            R.lensProp('values'),
            R.map(
              R.ifElse(({ id }) => valueIdsSet.has(id), R.assoc('isSelected', true), R.identity),
            ),
            filter,
          );
        }, filters),
      [R.identity, R.ifElse(R.isNil, R.always([]), R.split('.'))],
    ),
    R.reject(
      R.pipe(
        R.prop('id'),
        rules.isFreqDimension,
      ),
    ),
    R.filter(
      R.pipe(
        R.prop('values'),
        R.length,
        R.lt(1),
      ),
    ),
  ),
);

export const getDataflowName = createSelector(
  getData,
  getDataflow,
  (data, dataflow) => (R.isNil(dataflow) ? null : R.path(['structure', 'name'], data)),
);

export const getFrequencyDimension = createSelector(
  getDimensions,
  R.find(
    R.pipe(
      R.prop('id'),
      rules.isFreqDimension,
    ),
  ),
);

export const getRefAreaDimension = createSelector(
  getDimensions,
  R.find(
    R.pipe(
      R.prop('id'),
      rules.isAreaDimension,
    ),
  ),
);

export const getStructureRequestArgs = createSelector(
  getDataflow,
  getLocale,
  (dataflow, locale) => ({
    datasource: getDatasource(R.prop('datasourceId', dataflow)),
    identifiers: {
      code: R.prop('dataflowId', dataflow),
      ...R.pick(['agencyId', 'version'], dataflow),
    },
    locale,
  }),
);

export const getFullDataflowFileRequestArgs = dataflow =>
  createSelector(
    getLocale,
    locale => ({
      datasource: getDatasource(R.prop('datasourceId', dataflow)),
      identifiers: {
        code: R.prop('dataflowId', dataflow),
        ...R.pick(['agencyId', 'version'], dataflow),
      },
      locale,
    }),
  );

//------------------------------------------------------------------------------------------------#4
export const getSelection = createSelector(
  getFilters,
  R.pipe(
    R.map(R.over(R.lensProp('values'), R.filter(R.propEq('isSelected', true)))),
    R.filter(
      R.pipe(
        R.prop('values'),
        R.length,
        R.flip(R.gt)(0),
      ),
    ),
  ),
);

export const getFrequencyOptions = createSelector(
  getFrequencyDimension,
  R.pipe(
    R.propOr([], 'values'),
    R.reduce(
      (acc, { id, label }) =>
        R.pipe(
          R.over(R.lensProp('ids'), R.append(id)),
          R.over(R.lensProp('labels'), R.assoc(id, label || id)),
        )(acc),
      { ids: [], labels: {} },
    ),
  ),
);

export const getFrequency = createSelector(
  getDimensions,
  getDataquery,
  getFrequencyDimension,
  (dimensions, dataquery, frequencyDimension) => {
    if (R.isEmpty(dimensions)) return defaultFrequency;
    const dimensionIndex = R.findIndex(({ id }) => rules.isFreqDimension(id), dimensions);
    // if dataflow doesn't have time dimension. sdmx period will be annual
    if (R.lt(dimensionIndex)(0)) return defaultFrequency;
    const dataqueryfrequency = R.pipe(
      R.when(R.isNil, R.always('')),
      R.split('.'),
      R.view(R.lensIndex(dimensionIndex)),
      R.identity,
    )(dataquery);
    return R.either(R.isNil, R.isEmpty)(dataqueryfrequency)
      ? R.pipe(
          R.propOr([], 'values'),
          R.find(R.propEq('isDefaultSelected', true)),
          R.prop('id'),
        )(frequencyDimension)
      : dataqueryfrequency;
  },
);

//------------------------------------------------------------------------------------------------#5
export const getPeriod = createSelector(
  getParams,
  getFrequency,
  (params, frequency) =>
    R.pipe(
      R.prop('period'),
      R.ifElse(
        R.pipe(
          R.length,
          R.equals(2),
        ),
        R.pipe(
          getDateFromSdmxPeriod(frequency),
          R.map(date => getSdmxPeriod(frequency, date)),
        ),
        R.always(R.map(date => getSdmxPeriod(frequency, new Date(date)))(sdmxPeriod)),
      ),
    )(params),
);

export const getTimelineAxisProc = createSelector(
  getFrequency,
  getLocale,
  (frequency, locale) => {
    if (frequency === 'M') {
      const format = R.pathOr('YYYY MMM', [locale, 'timeFormat'], locales);
      const dateFnsLocales = { ar, en, es, fr, it };
      const dateLocale = R.ifElse(R.has(locale), R.prop(locale), R.prop('en'))(dateFnsLocales);
      return date => dateFns.format(date, format, { locale: dateLocale });
    }
    return date => getSdmxPeriod(frequency, date);
  },
);

//------------------------------------------------------------------------------------------------#6

export const getDatesFromSdmxPeriod = createSelector(
  getFrequency,
  getPeriod,
  (frequency, period) => getDateFromSdmxPeriod(frequency, period),
);

export const getUsedFilterFrequency = createSelector(
  getFrequency,
  getFrequencyOptions,
  getFrequencyDimension,
  (frequencyType, frequencyOptions, freqDimension) =>
    getFilterFrequency(frequencyType, frequencyOptions)(freqDimension),
);

export const getUsedFilterPeriod = createSelector(
  getPeriod,
  getLastNObservations,
  getLocale,
  getTimePeriod,
  (period, lastN, locale, timePeriod) =>
    getFilterPeriod(
      period,
      lastN,
      rules.getTimePeriodLabel(locale, R.path([locale, 'timeFormat'])(locales)),
      timePeriod,
    ),
);

export const getDataRequestParams = createSelector(
  getPeriod,
  getLastNObservations,
  getFrequency,
  (period, lastNObservations, frequency) => {
    if (R.either(R.isEmpty, R.isNil)(frequency)) return { lastNObservations };
    return {
      startPeriod: R.head(period),
      endPeriod: R.last(period),
      lastNObservations,
    };
  },
);

export const getDataRequestArgs = createSelector(
  getStructureRequestArgs,
  getDataquery,
  getDataRequestParams,
  (args, dataquery, params) => ({
    ...args,
    dataquery,
    params,
    ...(R.path(['datasource', 'hasRangeHeader'], args) ? { range: sdmxRange } : {}),
  }),
);

export const getStructureUrl = createSelector(
  getStructureRequestArgs,
  args => SDMXJS.getSDMXUrl({ ...args, type: 'dataflow' }),
);

export const getDataUrl = ({ agnostic }) =>
  createSelector(
    getDataRequestArgs,
    args => SDMXJS.getSDMXUrl({ ...args, agnostic, type: 'data' }),
  );
