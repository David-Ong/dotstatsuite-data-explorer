import { createSelector } from 'reselect';
import * as R from 'ramda';

//------------------------------------------------------------------------------------------------#0
const getApp = R.prop('app');

//------------------------------------------------------------------------------------------------#1
export const getPending = createSelector(
  getApp,
  R.prop('pending'),
);

//------------------------------------------------------------------------------------------------#2
export const getIsPending = id =>
  createSelector(
    getPending,
    R.pipe(
      R.prop(id),
      R.equals(true),
    ),
  );
