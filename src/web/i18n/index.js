import numeral from 'numeral';
import * as R from 'ramda';
import { addLocaleData } from 'react-intl';
import arLocaleData from 'react-intl/locale-data/ar';
import enLocaleData from 'react-intl/locale-data/en';
import frLocaleData from 'react-intl/locale-data/fr';
import esLocaleData from 'react-intl/locale-data/es';
import itLocaleData from 'react-intl/locale-data/it';

const model = locale => `${locale}/${locale}`;

export const setLocale = locale => numeral.locale(model(locale));

export const initialize = ({ locales = [], localeId = 'en' }) => {
  addLocaleData(arLocaleData);
  addLocaleData(enLocaleData);
  addLocaleData(frLocaleData);
  addLocaleData(esLocaleData);
  addLocaleData(itLocaleData);

  setLocale(localeId);

  R.forEach(locale => {
    const delimiters = R.prop('delimiters')(locale);
    if (R.isNil(delimiters)) return;
    numeral.register('locale', model(R.prop('id')(locale)), { delimiters });
  }, R.values(locales));
};

export { default as I18nProvider } from './provider';
export { default as withLocale } from './with-locale';
