import * as Router from '../router';
import * as R from 'ramda';

describe('selectors | router ', () => {
  const baseParams = {
    facet: 'eco',
    locale: 'en',
    tenant: 'oecd',
  };
  test('getEvolvedParams | with constraints', () => {
    const params = {
      ...baseParams,
      constraints: {
        hash: { value1: 'toto', value2: 'tutu' },
      },
    };
    const expected = { ...baseParams, constraints: { 0: ['toto', 'tutu'] } };
    expect(Router.getEvolvedParams(params)).toEqual(expected);
  });
  test('getEvolvedParams | constraints are not, return just all others params', () => {
    const params = {
      ...baseParams,
    };
    const expected = { ...baseParams };
    expect(Router.getEvolvedParams(params)).toEqual(expected);
  });
  test('getEvolvedParams | empty constraints, return just all others params', () => {
    const params = {
      ...baseParams,
      constraints: {},
    };
    const expected = { ...baseParams, constraints: {} };
    expect(Router.getEvolvedParams(params)).toEqual(expected);
  });
});
