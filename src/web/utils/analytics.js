import ReactGA from 'react-ga';
import { or, isEmpty, isNil } from 'ramda';
import { SHARE_CHART } from '../ducks/vis';
import { CHANGE_LOCATION } from '../ducks/router';

//------------------------------------------------------------------------------------event-category
const VIEW_DATAFLOW = 'VISUALISATION';
const HOME = 'HOME';
const DOWNLOAD = 'DOWNLOAD';
const CHART_SHARE = 'CHART_SHARE';
const CHART_VIEW = 'CHART_VIEW';
const CSV = 'CSV';

//----------------------------------------------------------------------------------------initialize
export const initialize = ({ token }) => {
  if (or(isEmpty(token), isNil(token))) return;
  if (process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line no-console
    console.info(`google-analytics initialized with ${token}`);
  }
  ReactGA.initialize(token, { debug: false /*process.env.NODE_ENV === 'development'*/ });
};

//-------------------------------------------------------------------------------------------helpers
const sendEvent = (category, action, label, hasInteraction) => {
  ReactGA.event({
    category,
    action,
    label,
    nonInteraction: !!hasInteraction,
  });
};

//-----------------------------------------------------------------------------------------switchman
// sendEvent(CHART_SHARE, titleDataflow, options.label)
// sendEvent(DOWNLOAD, titleDataflow, options.label)

export const event = (type, options) => {
  switch (type) {
    case CHANGE_LOCATION:
      if (!options.isVis) return ReactGA.pageview(options.url, HOME);
      ReactGA.pageview(options.url, options.dataflow);
      sendEvent(VIEW_DATAFLOW, options.dataflow, '', true);
      sendEvent(CHART_VIEW, options.dataflow, options.viewerId, true);
      break;
    case 'CHANGE_VIEWER_ID':
      return sendEvent(CHART_VIEW, options.dataflow, options.viewerId);
    case 'FETCH_DATA':
      if (options.isDownload) return sendEvent(DOWNLOAD, options.dataflow, CSV);
      break;
    case SHARE_CHART:
      return sendEvent(CHART_SHARE, options.dataflow, options.label);
    default:
      // eslint-disable-next-line no-console
      console.log('unknown action type', type);
      break;
  }
};
