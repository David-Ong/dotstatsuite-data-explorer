import * as R from 'ramda';
import qs from 'qs';

const getDataflowElements = R.pick(['datasourceId', 'dataflowId', 'agencyId', 'version']);

export const getEvolvedParams = R.evolve({
  constraints: R.pipe(
    R.values,
    R.mapObjIndexed(R.values),
  ),
  dataflow: getDataflowElements,
});

export const getQuery = params =>
  qs.stringify(R.reject(R.isEmpty, getEvolvedParams(params)), {
    encodeValuesOnly: true,
    skipNulls: true,
    arrayFormat: 'comma',
  });

export const getVisUrl = (params, dataflow = {}) =>
  `/vis?${getQuery({
    ...params,
    dataflow: getDataflowElements(dataflow),
  })}`;
