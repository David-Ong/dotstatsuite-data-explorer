import * as R from 'ramda';
export const PERIOD = 'PERIOD';
export const LASTN = 'LASTN';
export const START_PERIOD = 'START_PERIOD';
export const END_PERIOD = 'END_PERIOD';

export const getUsedFilterPeriod = (periods = [], lastN, labelRenderer, timePeriod) => {
  const getLastNValues = lastN => {
    if (R.or(R.equals('0', lastN), R.isNil(lastN))) return [];
    return [
      {
        id: LASTN,
        label: lastN,
      },
    ];
  };

  const getPeriodsValues = (periods = [], labelRenderer) => {
    const ids = [START_PERIOD, END_PERIOD];
    return R.addIndex(R.reduce)((acc, period, index) => {
      if (R.isNil(period)) return acc;
      return R.append({
        id: `${R.nth(index)(ids)}`,
        label: `${labelRenderer(period)}`,
      })(acc);
    }, [])(periods);
  };

  const values = [...getPeriodsValues(periods, labelRenderer), ...getLastNValues(lastN)];
  return R.isEmpty(values) ? [] : [{ id: PERIOD, label: R.prop('label')(timePeriod), values }];
};

export const getUsedFilterFrequency = (frequencyType, frequencyOptions) => freqDimension =>
  R.ifElse(R.isNil, R.always([]), label => [
    {
      id: 'FREQ',
      ...freqDimension,
      isNotRemovable: true,
      values: [
        {
          id: 'frequency_type',
          label: label,
          isNotRemovable: true,
        },
      ],
    },
  ])(R.path(['labels', frequencyType])(frequencyOptions));

export const addI18nLabels = translations => item => {
  if (R.isEmpty(item)) return [];
  return R.pipe(
    R.head,
    R.over(
      R.lensProp('values'),
      R.map(
        R.ifElse(
          R.pipe(
            R.prop('id'),
            R.flip(R.includes)(['START_PERIOD', 'END_PERIOD', 'LASTN']),
          ),
          item =>
            R.over(
              R.lensProp('label'),
              R.ifElse(
                R.always(R.is(Array)(R.prop(item.id, translations))),
                R.pipe(
                  R.flip(R.intersperse)(R.prop(item.id, translations)),
                  R.join(' '),
                ),
                R.concat(`${R.prop(item.id, translations)}: `),
              ),
            )(item),
          R.identity,
        ),
      ),
    ),
    R.of,
  )(item);
};
