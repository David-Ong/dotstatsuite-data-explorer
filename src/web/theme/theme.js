import { createMuiTheme } from '@material-ui/core/styles';

export const Colors = {
  GREY1: '#F0F0F0',
  GREY2: '#CCCCCC',
  GREY3: '#494444',
  GREY4: '#A4A1A1',
  GREY5: '#666666',
  BLUE1: '#2973BD',
  BLUE2: '#0965C1',
  BLUE3: '#2F75B5',
  BLUE4: '#BDD7EE',
  BLUE5: '#DDEBF7',
  BLUE6: '#1E226A',
  WHITE1: '#FFFFFF',
  BLACK1: '#000000',
  GREEN1: '#8CC841',
  ORANGE: '#f7a32c',
};

export const FontSizes = {
  SIZE1: 18,
  SIZE2: 14,
  SIZE3: 12,
};

export const Formats = {
  WIDE: 'initial',
  NARROW: 400,
  APP: 1000,
  APP_MIN_SIZE: 360,
  ITEM_MIN_SIZE: 350,
};

export const Layout = {
  PADDING: '5%',
  SIDE_WIDTH: 300,
  FILTER_MAX_HEIGHT: 250,
};

export const theme = (rtl = 'ltr') =>
  createMuiTheme({
    palette: {
      primary: {
        main: '#137cbd',
        mainAlpha: '#137cbd26',
        light: '#5dabf0',
        dark: '#00508c',
        contrastText: '#ffffff',
      },
      secondary: {
        main: '#bdd7ee',
        light: '#f0ffff',
        dark: '#8ca6bc',
        contrastText: '#ff0000',
      },
      action: {
        hover: '#bdd7ee',
        selected: '#137cbd',
        active: 'rgba(19,124,189,.3)',
      },
      typography: {
        fontFamily: ['Segoe UI'],
      },
      raisedButton: {
        textColor: '#ffffff',
        primaryTextColor: '#ffffff',
      },
      default: {
        backgroundColor: '#00000026', // grey with alpha
      },
      configLabelBG: '#B5CEEB',
      configLabelCol: '#1C2768',
    },
    app: {
      appSize: Formats.APP,
      appMinSize: Formats.APP_MIN_SIZE,
      itemMinSize: Formats.ITEM_MIN_SIZE,
    },
    scopeListContainer: {
      maxHeight: 250,
    },
    scopeList: {
      divider: '#000000',
    },
    direction: rtl,
    typography: {
      // until v4 https://material-ui.com/style/typography/#migration-to-typography-v2
      useNextVariants: true,
    },
    configLabel: {
      backgroundColor: '#B5CEEB',
      color: '#43679F',
      fontFamily: 'Segoe UI',
      fontSize: 16,
    },
    configButtonRegular: {
      backgroundColor: '#B5CEEB',
      color: '#43679F',
      fontFamily: 'Segoe UI',
      fontSize: 16,
      fontWeight: 'inherit',
      textTransform: 'none',
    },
    configButtonSelected: {
      backgroundColor: '#778899',
      color: '#B5CEEB',
      fontFamily: 'Segoe UI',
      fontSize: 16,
      fontWeight: 'inherit',
      textTransform: 'none',
    },
    panelSummary: {
      backgroundColor: '#137cbd',
      color: 'white',
      fontFamily: 'Segoe UI',
      fontSize: 16,
    },
    panelIcon: {
      color: 'white',
    },
    configInputs: {
      backgroundColor: 'white',
    },
    alert: {
      color: '#c23030',
      hover: {
        backgroundColor: 'rgba(219,55,55,.15)',
      },
      active: {
        backgroundColor: 'rgba(219,55,55,.3)',
      },
    },
    iconButton: {
      color: Colors.BLACK1,
      backgroundColor: 'transparent',
      '&:hover': {
        backgroundColor: 'transparent',
      },
      '&:active': {
        backgroundColor: 'transparent',
      },
    },
    table: {
      yBg: '#B5CEEB',
      yBgHover: '#c3d7ef',
      yBgActive: '#7e90a4',
      yFontHeader: '#1C2768',
      yFont: '#43679F',
      zBg: '#386CAA',
      zBgHover: '#5f89bb',
      zBgActive: '#274b76',
      zFontHeader: '#A2C2E4',
      zFont: Colors.WHITE1,
      xBg: Colors.WHITE1,
      xBgHeader: '#D7E6F4',
      xBgHover: '#dfebf6',
      xBgActive: '#96a1aa',
      xFontHeader: '#1C2768',
      xFont: '#43679F',
      oFont: Colors.GREY3,
      sBg: Colors.GREY1,
    },
  });
