import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { mergeDeepRight } from 'ramda';
import { ThemeProvider } from 'glamorous';
import { mainTheme } from '@sis-cc/dotstatsuite-ui-components';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { getIsRtl } from '../selectors/router';
import { theme as muiTheme } from './theme';
import { Rtl } from './jss-provider';

const Provider = ({ theme, children, isRtl }) => (
  <Rtl>
    <MuiThemeProvider theme={muiTheme(isRtl ? 'rtl' : 'ltr')}>
      <ThemeProvider theme={mergeDeepRight(mainTheme, theme)}>
        {React.Children.only(children)}
      </ThemeProvider>
    </MuiThemeProvider>
  </Rtl>
);

Provider.propTypes = {
  theme: PropTypes.object,
  children: PropTypes.element.isRequired,
  isRtl: PropTypes.bool,
};

Provider.defaultProps = {
  theme: mainTheme,
  children: PropTypes.element.isRequired,
};

export default connect(createStructuredSelector({ isRtl: getIsRtl }))(Provider);
