import valueParser from '../valueParser';
import md5 from 'md5';

describe('valueParser', () => {
  it('should pass without constraints', () => {
    expect(valueParser({ facetId: 1 })({ val: 11, count: 10 })).toEqual({
      id: 11,
      label: '11',
      count: 10,
      isSelected: false,
    });
  });

  it('should pass and be selected', () => {
    expect(
      valueParser({ facetId: 1, constraints: { [md5(`${1}${11}`)]: 'selection' } })({
        val: 11,
        count: 10,
      }),
    ).toEqual({ id: 11, label: '11', count: 10, isSelected: true });
  });
});
