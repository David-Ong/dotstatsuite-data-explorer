import dataflowParser from '../dataflowParser';

describe('dataflowParser', () => {
  it('should pass', () => {
    expect(
      dataflowParser({ highlighting: { '4/5/3/6': 8 } })({
        name: 1,
        description: 2,
        agencyId: 3,
        dataflowId: 4,
        version: 5,
        datasource_id: 6,
        indexationDate: 7,
      }),
    ).toEqual({
      id: '4/5/3/6',
      name: 1,
      description: 2,
      agencyId: 3,
      dataflowId: 4,
      version: 5,
      datasourceId: 6,
      indexationDate: 7,
      highlights: 8,
    });
  });
});
