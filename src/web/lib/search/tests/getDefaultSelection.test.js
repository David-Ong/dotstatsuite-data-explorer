import { getDefaultSelection } from '../sdmx-constraints-bridge';

describe('getDefaultSelection', () => {
  it('should pass', () => {
    const dimensions = [{ id: 'DIM', values: [{ id: 'DIM_ID1' }] }];
    expect(getDefaultSelection(dimensions, {}, {})).toEqual({});
  });
  it("should add everything of structureSelection even if doesn't exist in dimensions ", () => {
    const dimensions = [{ id: 'DIM', values: [{ id: 'DIM_ID1' }] }];
    const structureSelection = {
      COLLECTION: ['STI'],
      FREQ: ['A', 'B', 'C'],
    };
    expect(getDefaultSelection(dimensions, structureSelection, {})).toEqual(structureSelection);
  });
  it("should apply constraints even if doesn't exist in dimensions", () => {
    const dimensions = [{ id: 'FREQ', values: [{ id: 'A' }] }];
    const structureSelection = {
      COLLECTION: ['STI'],
      FREQ: ['A', 'B', 'C'],
    };
    const constraints = {
      hash: { facetId: 'freq', constraintId: 'Annual#B#' },
    };
    expect(getDefaultSelection(dimensions, structureSelection, constraints)).toEqual({
      COLLECTION: ['STI'],
      FREQ: ['B'],
    });
  });
});
