import * as R from 'ramda';
import dataflowParser from './dataflowParser';
import facetsParser from './facetsParser';

export default ({ config: { translations } = {}, constraints }) => ({
  dataflows = [],
  facets = {},
  highlighting = {},
  numFound = 0,
} = {}) => ({
  dataflows: R.map(dataflowParser({ highlighting }))(dataflows),
  facets: facetsParser({ translations, constraints })(facets),
  numFound,
});
