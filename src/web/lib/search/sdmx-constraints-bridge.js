import * as R from 'ramda';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import { defaultFrequency } from '../settings';
import { searchConstants } from './';

export const searchConstraintsToVisSelection = (dimensions, constraints) => {
  const indexedConstraints = R.pipe(
    R.values,
    R.map(
      R.evolve({
        constraintId: R.pipe(
          R.match(searchConstants.FACET_VALUE_MASK),
          R.tail,
          R.head,
        ),
      }),
    ),
    R.groupBy(R.prop('facetId')),
  )(constraints);

  return R.reduce(
    (acc, dim) => {
      const id = R.prop('id', dim);
      const loweredId = R.toLower(id);
      return R.ifElse(
        R.has(loweredId),
        R.pipe(
          R.prop(loweredId),
          R.map(R.prop('constraintId')),
          selection => R.assoc(id, selection, acc),
        ),
        R.always(acc),
      )(indexedConstraints);
    },
    {},
    dimensions,
  );
};

export const getDefaultSelection = (dimensions, structureSelection, constraints) => {
  const searchSelection = searchConstraintsToVisSelection(dimensions, constraints);
  const freqDim = R.find(SDMXJS.isFrequency, dimensions) || [];

  return R.pipe(
    R.merge(structureSelection),
    R.cond([
      [R.always(R.isEmpty(freqDim)), R.identity],
      [
        R.has(R.prop('id')(freqDim)),
        R.over(
          R.lensProp(R.prop('id')(freqDim)),
          R.pipe(
            R.head,
            R.of,
          ),
        ),
      ],
      [
        R.T,
        R.assoc(
          R.prop('id')(freqDim),
          R.find(R.propEq('id', defaultFrequency), freqDim)
            ? [defaultFrequency]
            : [R.path(['values', 0, 'id'])(freqDim)],
        ),
      ],
    ]),
  )(searchSelection);
};
