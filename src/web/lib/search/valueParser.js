import * as R from 'ramda';
import md5 from 'md5';
import C from './constants';

const setFormat = (string = '') =>
  R.converge((match, str) => R.replace(R.head(match), ` (${R.last(match)})`, str), [
    R.match(C.FACET_VALUE_MASK),
    R.identity,
  ])(`${string}`);

export default ({ facetId, constraints = {} }) => ({ val, count, label }) => ({
  id: val,
  label: setFormat(R.defaultTo(val, label)),
  count,
  isSelected: R.has(md5(`${facetId}${val}`), constraints),
});
