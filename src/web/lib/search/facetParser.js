import * as R from 'ramda';
import valueParser from './valueParser';
import valueTreeParser from './valueTreeParser';

export default ({ translations, constraints = {} }) => ([id, { type, buckets = [] }]) => {
  const parser = R.equals(type, 'tree') ? valueTreeParser : valueParser;
  const values = R.map(parser({ facetId: id, constraints }))(buckets);
  const count = R.pipe(
    R.filter(R.propEq('isSelected', true)),
    R.length,
  )(values);

  return {
    id,
    values,
    count,
    label: R.prop(`sfs.facet.${id}`, translations),
    hasPath: R.equals(type, 'tree'),
  };
};
