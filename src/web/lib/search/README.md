# search lib

> about [sdmx-faceted-search](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search) parsing

## overview
```mermaid
graph LR
search-lib --> configParser
search-lib --> searchParser
searchParser --> dataflows((dataflows))
dataflows --> dataflowParser
searchParser --> facetsParser
facetsParser --> facetParser
facetParser --> values((values))
values --> valueParser
values --> valueTreeParser
valueTreeParser --> valueParser
searchParser --> numFound((numFound))
```
