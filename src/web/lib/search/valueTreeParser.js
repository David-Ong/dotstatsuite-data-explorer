import * as R from 'ramda';
import valueParser from './valueParser';

export default ({ facetId, constraints }) => ({ val, count }) =>
  R.pipe(
    R.split('|'),
    R.converge(
      (label, path, level) => ({
        ...valueParser({ facetId, constraints })({ val, label, count }),
        parentId: R.ifElse(
          R.isNil,
          R.identity,
          // add an accessor to scopelist to remove the following code
          // adjust the current level to build a valid parentId
          R.pipe(
            R.prepend(R.dec(level)),
            R.join('|'),
          ),
        )(path),
        level,
        path,
      }),
      [
        R.last,
        R.pipe(
          R.init,
          R.tail,
          R.ifElse(R.isEmpty, R.always(null), R.identity),
        ),
        R.pipe(
          R.head,
          Number,
        ),
      ],
    ),
  )(val);
