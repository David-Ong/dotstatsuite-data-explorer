import * as R from 'ramda';

export default ({ highlighting }) => ({
  datasource_id,
  dataflowId,
  agencyId,
  version,
  ...rest
}) => {
  const id = R.join('/', [dataflowId, version, agencyId, datasource_id]);

  return {
    ...rest,
    datasourceId: datasource_id,
    dataflowId,
    agencyId,
    version,
    id,
    highlights: R.propOr({}, id, highlighting),
  };
};
