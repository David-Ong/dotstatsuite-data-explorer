import * as R from 'ramda';
import facetsParser from './facetsParser';

export default ({ locale, i18n = {}, facets = {} } = {}) => {
  const translations = R.propOr({}, locale, i18n);

  return {
    locale,
    translations,
    facets: facetsParser({ translations })(facets),
  };
};
