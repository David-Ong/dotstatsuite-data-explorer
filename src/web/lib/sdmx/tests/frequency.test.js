import * as Freq from '../frequency';
jest.mock('../../settings', () => ({
  defaultFrequency: 'A',
  sdmxPeriodBoundaries: ['2017', '2020'],
}));

describe('Frequency', () => {
  /*test('differents sdmx period include in frequency Dimension', () => {
    expect(Freq.getDateFromSdmxPeriod('A', ['2018', '2018'])).toEqual([
      new Date('2018'),
      new Date('2018'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('M', ['2018-05', '2018-08'])).toEqual([
      new Date('2018 05'),
      new Date('2018 08'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('D', ['2018-05-12', '2018-08-30'])).toEqual([
      new Date('2018 05 12'),
      new Date('2018 08 30'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('H', ['2018-05-12T12:00', '2018-08-30T18:00'])).toEqual([
      new Date('2018 05 12 12:00'),
      new Date('2018 08 30 18:00'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('N', ['2018-05-12T12:50', '2018-08-30T12:55'])).toEqual([
      new Date('2018 05 12 12:50'),
      new Date('2018 08 30 12:55'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('Q', ['2018-Q1', '2018-Q3'])).toEqual([
      new Date('2018'),
      new Date('2018 07'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('W', ['2018-01', '2018-10'])).toEqual([
      new Date('2018'),
      new Date('2018 03 05'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('S', ['2018-S1', '2018-S2'])).toEqual([
      new Date('2018'),
      new Date('2018 07'),
    ]);
  });*/
  test('is valid Date', () => {
    expect(Freq.getSdmxPeriod('A')(new Date('2018'))).toEqual('2018');
    expect(Freq.getSdmxPeriod('S')(new Date('2018 01 01'))).toEqual('2018-S1');
    expect(Freq.getSdmxPeriod('Q')(new Date('2018 01 01'))).toEqual('2018-Q1');
    expect(Freq.getSdmxPeriod('M')(new Date('2018 01 01'))).toEqual('2018-01');
    expect(Freq.getSdmxPeriod('W')(new Date('2018 01 01'))).toEqual('2018-01');
    expect(Freq.getSdmxPeriod('D')(new Date('2018 01 01'))).toEqual('2018-01-01');
    expect(Freq.getSdmxPeriod('H')(new Date('2018 01 01'))).toEqual('2018-01-01T00:00:00');
    expect(Freq.getSdmxPeriod('N')(new Date('2018 01 01'))).toEqual('2018-01-01T00:00:00');
  });
  test('return only one frequency from constraint', () => {
    expect(Freq.getOneFreq(['A', 'B', 'C'])).toEqual(['A']);
    expect(Freq.getOneFreq(['B', 'V', 'A'])).toEqual(['A']);
    expect(Freq.getOneFreq(['B', 'V', 'C'])).toEqual(['B']);
    expect(Freq.getOneFreq(['V', 'B', 'C'])).toEqual(['V']);
  });
  test('set only one frequency to isDefaultSelected true', () => {
    const dimensions = values => [{ id: 'FREQ', values }];
    expect(Freq.setFrequencySelected(dimensions([]))).toEqual([{ id: 'FREQ', values: [] }]);
    expect(Freq.setFrequencySelected(dimensions([{ id: 'A' }]))).toEqual(
      dimensions([{ id: 'A', isDefaultSelected: true }]),
    );
    expect(Freq.setFrequencySelected(dimensions([{ id: 'Z' }, { id: 'A' }]))).toEqual(
      dimensions([{ id: 'Z', isDefaultSelected: true }, { id: 'A', isDefaultSelected: false }]),
    );
    expect(
      Freq.setFrequencySelected(dimensions([{ id: 'A' }, { id: 'X', isDefaultSelected: true }])),
    ).toEqual(
      dimensions([{ id: 'A', isDefaultSelected: false }, { id: 'X', isDefaultSelected: true }]),
    );
    expect(
      Freq.setFrequencySelected(
        dimensions([{ id: 'A', isDefaultSelected: true }, { id: 'X', isDefaultSelected: true }]),
      ),
    ).toEqual(
      dimensions([{ id: 'A', isDefaultSelected: true }, { id: 'X', isDefaultSelected: false }]),
    );
  });
  test('is valid date', () => {
    const date = new Date('2018');
    expect(Freq.getValidOuput(date)).toEqual(date);
    expect(Freq.getValidOuput('2018')).toEqual('2018');
    expect(Freq.getValidOuput('')).toEqual(undefined);
    expect(Freq.getValidOuput('2018A2')).toEqual(undefined);
    expect(Freq.getValidOuput('2018 2')).toEqual('2018 2');
    expect(Freq.getValidOuput('2018 2 10')).toEqual('2018 2 10');
  });
  test('parse sdmx periods to dates', () => {
    expect(Freq.getDateFromSdmxPeriod('A', ['2018', '2019'])).toEqual([
      new Date('2018'),
      new Date('2019'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('S', ['2018-S1', '2019-S1'])).toEqual([
      new Date('2018'),
      new Date('2019'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('Q', ['2018-Q1', '2019-Q1'])).toEqual([
      new Date('2018'),
      new Date('2019'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('W', ['2018-W1', '2019-W1'])).toEqual([
      new Date('2018'),
      new Date('2019'),
    ]);
  });
  test('counter of period following the frequency', () => {
    expect(Freq.getPeriodCounter('A', [new Date('2018'), new Date('2019')])).toEqual('2 / 4');
    expect(Freq.getPeriodCounter('S', [new Date('2018'), new Date('2019')])).toEqual('3 / 7');
    expect(Freq.getPeriodCounter('Q', [new Date('2018'), new Date('2019')])).toEqual('5 / 13');
    expect(Freq.getPeriodCounter('W', [new Date('2018'), new Date('2019')])).toEqual('53 / 157');
    expect(Freq.getPeriodCounter(undefined, [new Date('2018'), new Date('2019')])).toEqual('2 / 4');
    expect(Freq.getPeriodCounter(undefined, [undefined, new Date('2019')])).toEqual('3 / 4');
    expect(Freq.getPeriodCounter(undefined, [undefined, undefined])).toEqual('4 / 4');
  });
});
