import * as R from 'ramda';
import { sdmxPeriod as defaultSettingsPeriod } from '../settings';
import { FILENAME_MAX_LENGTH } from '../../utils/constants';

export const getFilename = R.pipe(
  R.converge(R.append, [
    R.pipe(
      R.prop('dataquery'),
      R.ifElse(R.isNil, R.always('all'), R.identity),
    ),
    R.pipe(
      R.prop('identifiers'),
      R.props(['agencyId', 'code', 'version']),
    ),
  ]),
  R.join('_'),
  R.slice(0, FILENAME_MAX_LENGTH),
);

export const getDefaultRouterParams = (params, dataquery) => {
  const period = R.ifElse(
    R.anyPass([R.has('startPeriod'), R.has('endPeriod')]),
    R.converge((start, end) => [start, end], [R.prop('startPeriod'), R.prop('endPeriod')]),
    R.always(defaultSettingsPeriod),
  )(params);
  return {
    ...R.pick(['lastNObservations'], params),
    dataquery,
    period,
  };
};
