import * as R from 'ramda';
import dateFns from 'date-fns';
import { rules } from '@sis-cc/dotstatsuite-components';
import { defaultFrequency, sdmxPeriodBoundaries } from '../settings';

const sdmxFormat = {
  A: 'YYYY',
  S: 'YYYY-[S][SEMESTER]',
  Q: 'YYYY-[Q]Q',
  M: 'YYYY-MM',
  W: 'YYYY-WW',
  D: 'YYYY-MM-DD',
  H: 'YYYY-MM-DD[T]HH:mm:ss',
  N: 'YYYY-MM-DD[T]HH:mm:ss',
};

export const datesBoundaries = R.map(period => new Date(period))(sdmxPeriodBoundaries || []);

//-----------------------------------------------------------------------------------------DataQuery
export const getOneFreq = freqIds => {
  if (R.includes(defaultFrequency, freqIds)) return [defaultFrequency];
  return R.pipe(
    R.head,
    R.flip(R.append)([]),
  )(freqIds);
};
// ----------------------------------------------------------------------------------------Structure
const setFrequency = frequency =>
  R.map(
    R.ifElse(
      R.propEq('id', frequency),
      R.set(R.lensProp('isDefaultSelected'), R.T()),
      R.set(R.lensProp('isDefaultSelected'), R.F()),
    ),
  );
const setOnlyOneSelected = (values, defaultsSelected) => {
  if (R.includes(defaultFrequency, defaultsSelected)) return setFrequency(defaultFrequency)(values);
  if (R.isEmpty(defaultsSelected)) {
    const firstId = R.pipe(
      R.head,
      R.prop('id'),
    )(values);
    return setFrequency(firstId)(values);
  }
  return setFrequency(R.head(defaultsSelected))(values);
};
const getAllDefaultSelected = R.pipe(
  R.filter(R.propEq('isDefaultSelected', true)),
  R.pluck('id'),
);

export const setFrequencySelected = R.map(
  R.ifElse(
    R.pipe(
      R.prop('id'),
      rules.isFreqDimension,
    ),
    R.over(
      R.lensProp('values'),
      R.converge(setOnlyOneSelected, [R.identity, getAllDefaultSelected]),
    ),
    R.identity,
  ),
);

// --------------------------------------------------------------------------------------Sdmx period
const isValidDate = period =>
  R.and(dateFns.isDate, date => R.not(R.equals(date.getTime(date), NaN)))(new Date(period));

const getSemesterValue = R.ifElse(
  R.pipe(
    Number,
    R.inc,
    R.gte(6),
  ),
  R.always(1),
  R.always(2),
);
const getSemester = R.pipe(
  date => dateFns.getMonth(date),
  getSemesterValue,
);

const applyFormat = frequency => date => {
  const hasFormat = R.includes(frequency)(R.keys(sdmxFormat));
  const format = hasFormat ? R.prop(frequency)(sdmxFormat) : R.prop(defaultFrequency)(sdmxFormat);
  return R.pipe(
    () => dateFns.format(date, format),
    R.replace(/SEMESTER/, getSemester(date)),
  )(date);
};

export const getSdmxPeriod = R.curry((frequency, date) =>
  R.ifElse(isValidDate, applyFormat(frequency), R.always(undefined))(date),
);

export const getValidOuput = date => R.ifElse(isValidDate, R.identity, R.always(undefined))(date);

const dec = R.ifElse(R.gte(0), R.identity, R.dec);
const parseDate = date => dateFns.parse(date);
const addQuarters = quarter => date => dateFns.addQuarters(date, dec(quarter));
const addSemesters = (semester = 0) => date => {
  const number = dec(Number(semester));
  if (R.lte(number)(0)) return date;
  return dateFns.addMonths(date, R.multiply(6)(dec(Number(semester))));
};
const addWeeks = week => date => dateFns.addWeeks(date, dec(week));
const slicerSQ = R.pipe(
  R.last,
  R.slice(1, Infinity),
);

const getSQW = (fn, last) =>
  R.map(period => {
    if (R.isEmpty(R.head(period))) return undefined;
    const year = R.head(period);
    const hasAdded = last(period);
    return R.pipe(
      parseDate,
      fn(hasAdded),
    )(year);
  });

const getWeekNumber = R.ifElse(
  R.pipe(
    Number,
    R.equals(NaN),
  ),
  R.always(1),
  R.identity,
);

const parseDateFromSdmxPeriod = R.curry((frequency, sdmxPeriod = []) => {
  const periodSQW = R.map((stringPeriod = '') => R.split('-')(stringPeriod))(sdmxPeriod);
  if (
    R.or(
      R.complement(R.includes)(frequency)(['Q', 'S', 'W']),
      R.pipe(
        R.flatten,
        R.length,
        R.equals(2),
      )(periodSQW),
    )
  ) {
    return R.map(parseDate)(sdmxPeriod);
  }
  if (R.equals('W')(frequency)) {
    return getSQW(
      addWeeks,
      R.pipe(
        R.last,
        getWeekNumber,
      ),
    )(periodSQW);
  }
  if (R.equals('Q')(frequency)) return getSQW(addQuarters, slicerSQ)(periodSQW);
  return getSQW(addSemesters, slicerSQ)(periodSQW);
});

export const getDateFromSdmxPeriod = R.curry((frequencyType, period) =>
  R.map(getValidOuput)(parseDateFromSdmxPeriod(frequencyType, period)),
);
// --------------------------------------------------------------------------------------Counter
const sdmxCounter = {
  A: dateFns.differenceInYears,
  S: (end, start) => dateFns.differenceInMonths(end, start) / 6,
  Q: dateFns.differenceInQuarters,
  M: dateFns.differenceInMonths,
  W: dateFns.differenceInWeeks,
  D: dateFns.differenceInDays,
  H: dateFns.differenceInHours,
  N: dateFns.differenceInMinutes,
};

const getDifference = (fn, dates) => {
  return fn(R.last(dates) || R.last(datesBoundaries), R.head(dates) || R.head(datesBoundaries)) + 1;
};

export const getPeriodCounter = (frequency, dates = []) => {
  const hasFormat = R.includes(frequency)(R.keys(sdmxCounter));
  const difference = hasFormat
    ? R.prop(frequency)(sdmxCounter)
    : R.prop(defaultFrequency)(sdmxCounter);
  return `${getDifference(difference, dates)} / ${getDifference(difference, datesBoundaries)}`;
};
