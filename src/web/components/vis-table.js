import React from 'react';
import PropTypes from 'prop-types';
import { Table as UiTable, ValueCell } from '@sis-cc/dotstatsuite-ui-components';
import SisHeader from '@sis-cc/dotstatsuite-ui-header';
import SisFooter from '@sis-cc/dotstatsuite-ui-footer';
import glamorous from 'glamorous';
import * as R from 'ramda';

const TableContainer = glamorous.div({
  border: '1px solid #0297C9',
  borderLeftWidth: 0,
  borderRightWidth: 0,
  marginTop: 10, // sync style with chart
});

const Cell = glamorous.div(
  {
    width: '100%',
    height: '100%',
    padding: '4px 6px',
    fontSize: 16,
    textAlign: 'right',
  },
  ({ theme }) => ({
    color: theme.table.oFont,
    borderTop: `1px solid ${theme.table.border}`,
    borderLeft: `1px solid ${theme.table.border}`,
    borderRight: `1px solid ${theme.table.border}`,
    borderBottom: `1px solid ${theme.table.border}`,
    backgroundColor: theme.table.xBg,
  }),
);

const Table = ({
  chartConfig,
  header,
  isNotATable,
  footnotes,
  headerData,
  sectionsData,
  cells,
}) => {
  return (
    <TableContainer>
      <SisHeader {...header} />
      <div style={{ direction: 'ltr' }}>
        {/* remove style when table will be rtl compliant */}
        {isNotATable ? (
          <div>
            <Cell>
              <ValueCell {...R.pathOr({}, ['', '', '', 0], cells)} />
            </Cell>
          </div>
        ) : (
          <UiTable cells={cells} sectionsData={sectionsData} headerData={headerData} />
        )}
      </div>
      <SisFooter
        {...chartConfig}
        fonts={chartConfig.fonts.footer}
        source={{ link: footnotes.source, label: footnotes.sourceLabel }}
      />
    </TableContainer>
  );
};

Table.propTypes = {
  header: PropTypes.object,
  chartConfig: PropTypes.object,
  source: PropTypes.object,
  data: PropTypes.object,
  footnotes: PropTypes.object,
  layoutIds: PropTypes.object,
  renderObservation: PropTypes.func,
  itemFormatter: PropTypes.func,
};

export default Table;
