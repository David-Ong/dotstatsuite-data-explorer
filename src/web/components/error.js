import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  DELayout,
  DELayoutHeader,
  DELayoutFooter,
  DELayoutContent,
  DESplash,
} from '@sis-cc/dotstatsuite-ui-components';
import Header from './header';
import Footer from './footer';
import { getAsset } from '../lib/settings';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false, logo: getAsset('subheader') };
  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  render() {
    if (!this.state.hasError) return this.props.children;

    if (this.props.isFinal) return <h1>Whoops, something went wrong on our end.</h1>;

    return (
      <DELayout isHome>
        <DELayoutHeader>
          <Header />
        </DELayoutHeader>
        <DELayoutContent>
          <DESplash title={<FormattedMessage id="de.error.title" />} logo={this.state.logo} />
        </DELayoutContent>
        <DELayoutFooter>
          <Footer />
        </DELayoutFooter>
      </DELayout>
    );
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node,
  isFinal: PropTypes.bool,
};

export default ErrorBoundary;
