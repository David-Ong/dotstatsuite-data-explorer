import React from 'react';
import PropTypes from 'prop-types';
import { Chart as OecdChart } from '@sis-cc/dotstatsuite-components';
import glamorous from 'glamorous';

const Container = glamorous.div({
  marginTop: 10,
});

const Chart = ({ chartData, chartOptions, chartConfig, type }) => {
  return (
    <Container>
      <OecdChart data={chartData} config={chartConfig} options={chartOptions} type={type} />
    </Container>
  );
};

Chart.propTypes = {
  chartConfig: PropTypes.object,
  type: PropTypes.string,
  chartData: PropTypes.object,
  chartOptions: PropTypes.object,
  config: PropTypes.object,
};

export default Chart;
