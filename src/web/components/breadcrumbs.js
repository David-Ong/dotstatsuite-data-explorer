import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, branch, renderNothing, mapProps } from 'recompose';
import { injectIntl } from 'react-intl';
import * as R from 'ramda';
import { DEBreadcrumbs } from '@sis-cc/dotstatsuite-ui-components';
import { resetSearch } from '../ducks/search';
import { getHasNoSearchParams, getNumFound } from '../selectors/search';
import { resetDataflow } from '../ducks/router';
import { getDataflowName } from '../selectors/sdmx';
import { getIsPending } from '../selectors/app';
import { hasNoSearch } from '../lib/settings';
import { withLocale } from '../i18n';

export default compose(
  injectIntl,
  connect(
    createStructuredSelector({
      dataflowName: getDataflowName,
      numFound: getNumFound,
      isConfigLoading: getIsPending('getConfig'),
      isSearchLoading: getIsPending('getSearch'),
      hasNoSearchParams: getHasNoSearchParams,
    }),
    { resetSearch, resetDataflow },
  ),
  branch(R.always(hasNoSearch), renderNothing),
  mapProps(
    ({
      resetSearch,
      resetDataflow,
      intl,
      dataflowName,
      numFound,
      isConfigLoading,
      isSearchLoading,
      hasNoSearchParams,
    }) => ({
      items: R.reject(R.isNil)([
        { text: intl.formatMessage({ id: 'de.search.breadcrumb.home' }), onClick: resetSearch },
        hasNoSearchParams
          ? null
          : {
              text: R.or(isConfigLoading, isSearchLoading)
                ? intl.formatMessage({ id: 'de.search.list.loading' })
                : R.either(R.isNil, R.equals(0))(numFound)
                ? intl.formatMessage({ id: 'de.search.breadcrumb.blank' })
                : intl.formatMessage({ id: 'de.search.breadcrumb.search' }, { size: numFound }),
              onClick: resetDataflow,
              disabled: R.isNil(dataflowName),
            },
        R.isNil(dataflowName) ? null : { text: dataflowName, disabled: true },
      ]),
    }),
  ),
  withLocale,
)(DEBreadcrumbs);
