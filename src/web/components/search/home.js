import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { FormattedMessage } from 'react-intl';
import {
  DELayout,
  DELayoutHeader,
  DELayoutFooter,
  DELayoutContent,
  DESplash,
  VXSpotlight,
  DESearchFacets,
} from '@sis-cc/dotstatsuite-ui-components';
import CircularProgress from '@material-ui/core/CircularProgress';
import Header from '../header';
import Footer from '../footer';

const Content = ({ changeConstraints, changeTerm, facets, intl, isLoading, isNarrow, isRtl }) => {
  if (isLoading) {
    return <CircularProgress />;
  }
  return (
    <Fragment>
      <VXSpotlight
        isRtl={isRtl}
        isNarrow={isNarrow}
        hasClearAll
        hasCommit
        isHome
        changeSpotlight={changeTerm}
        placeholder={intl.formatMessage({ id: 'vx.spotlight.placeholder' })}
      />
      {R.isEmpty(facets) ? null : (
        <DESearchFacets
          items={facets}
          isRtl={isRtl}
          isNarrow={isNarrow}
          selectTopic={changeConstraints}
          browseLabel={<FormattedMessage id="de.search.topics.browse" />}
          blankLabel={<FormattedMessage id="de.search.topics.blank" />}
        />
      )}
    </Fragment>
  );
};

const Home = ({
  isNarrow,
  isRtl,
  isLoading,
  logo,
  intl,
  facets,
  changeConstraints,
  changeTerm,
  hasNoSearch,
}) => (
  <DELayout isNarrow={isNarrow} isRtl={isRtl} isHome>
    <DELayoutHeader>
      <Header isNarrow={isNarrow} isRtl={isRtl} />
    </DELayoutHeader>
    <DELayoutContent>
      <DESplash
        isRtl={isRtl}
        isNarrow={isNarrow}
        title={<FormattedMessage id="de.search.splash" />}
        logo={logo}
      />
      {hasNoSearch ? null : (
        <Content
          changeConstraints={changeConstraints}
          changeTerm={changeTerm}
          facets={facets}
          intl={intl}
          isLoading={isLoading}
          isNarrow={isNarrow}
          isRtl={isRtl}
        />
      )}
    </DELayoutContent>
    <DELayoutFooter>
      <Footer isNarrow={isNarrow} isRtl={isRtl} />
    </DELayoutFooter>
  </DELayout>
);

Home.propTypes = {
  isNarrow: PropTypes.bool,
  isRtl: PropTypes.bool,
  isLoading: PropTypes.bool,
  changeConstraints: PropTypes.func,
  changeTerm: PropTypes.func,
  facets: PropTypes.array,
  hasNoSearch: PropTypes.bool,
  logo: PropTypes.string,
  intl: PropTypes.object,
};

Content.propTypes = Home.propTypes;

export default Home;
