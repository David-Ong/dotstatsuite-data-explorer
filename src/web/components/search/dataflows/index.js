import React from 'react';
import * as R from 'ramda';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, branch, renderComponent } from 'recompose';
import { VXNoData, VXLoader } from '@sis-cc/dotstatsuite-ui-components';
import Dataflows from './dataflows';
import { getIsPending, getPending } from '../../../selectors/app';
import { requestDataFile } from '../../../ducks/sdmx';
import { getDataflows } from '../../../selectors/search';
import { changeDataflow } from '../../../ducks/router';

export default compose(
  connect(
    createStructuredSelector({
      dataflows: getDataflows,
      isSearchLoading: getIsPending('getSearch'),
      isConfigLoading: getIsPending('getConfig'),
      pending: getPending, // downloads
    }),
    { changeDataflow, requestDataFile },
  ),
  branch(
    R.pipe(
      R.pick(['isSearchLoading', 'isConfigLoading']),
      R.any(R.equals(true)),
    ),
    renderComponent(() => (
      <VXLoader isLarge message={<FormattedMessage id="de.search.list.loading" />} />
    )),
  ),
  branch(
    R.pipe(
      R.prop('dataflows'),
      R.isEmpty,
    ),
    renderComponent(() => <VXNoData message={<FormattedMessage id="de.search.list.blank" />} />),
  ),
)(Dataflows);
