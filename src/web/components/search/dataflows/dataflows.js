import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Dataflow from './dataflow';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    borderTop: '2px solid #CCCCCC',
  },
};

const Dataflows = ({ dataflows, pending, classes, changeDataflow, requestDataFile }) => (
  <div className={classes.root}>
    {R.map(
      dataflow => (
        <Dataflow
          key={R.prop('id', dataflow)}
          {...dataflow}
          isDownloading={R.pipe(
            R.prop(`getDataFile/${R.prop('id', dataflow)}`),
            R.equals(true),
          )(pending)}
          changeDataflow={event => {
            event.stopPropagation();
            if (event.ctrlKey) return;
            event.preventDefault();
            return changeDataflow(dataflow);
          }}
          requestDataFile={() => requestDataFile({ isDownloadAllData: true, dataflow })}
        />
      ),
      dataflows,
    )}
  </div>
);

Dataflows.propTypes = {
  classes: PropTypes.object.isRequired,
  pending: PropTypes.object,
  dataflows: PropTypes.array,
  requestDataFile: PropTypes.func.isRequired,
  changeDataflow: PropTypes.func.isRequired,
};

export default withStyles(styles)(Dataflows);
