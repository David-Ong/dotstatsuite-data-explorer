import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { compose, pure } from 'recompose';
import { FormattedMessage, FormattedDate } from 'react-intl';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import VerticalAlignBottomIcon from '@material-ui/icons/VerticalAlignBottom';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import { getDatasource } from '../../../lib/settings';

const styles = {
  card: {
    borderBottom: '1px solid #CCCCCC',
    paddingBottom: 16,
  },
  cardHeader: {
    padding: 0,
    paddingTop: 8,
  },
  cardContent: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  cardActions: {
    padding: 0,
    justifyContent: 'center',
  },
  button: {
    textTransform: 'inherit',
  },
  buttonWrapper: {
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  highlight: {
    '& em': {
      color: '#8CC841',
    },
  },
};

const Dataflow = ({
  name,
  id,
  description,
  indexationDate,
  datasourceId,
  highlights,
  changeDataflow,
  requestDataFile,
  isDownloading,
  classes,
  url,
}) => (
  <Card elevation={0} className={classes.card}>
    <CardHeader
      className={classes.cardHeader}
      title={
        <Typography variant="h6" inline color="primary">
          <a rel="noopener noreferrer" href={url} onClick={changeDataflow}>
            {R.defaultTo(id, name)}
          </a>
        </Typography>
      }
      subheader={
        <React.Fragment>
          <Typography variant="body2" color="textSecondary">
            <FormattedMessage id="de.search.dataflow.source" />:{' '}
            {R.prop('label', getDatasource(datasourceId))}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            <FormattedMessage
              id="de.search.dataflow.last.updated"
              values={{
                date: (
                  <FormattedDate value={indexationDate} year="numeric" month="long" day="2-digit" />
                ),
              }}
            />
          </Typography>
        </React.Fragment>
      }
    />
    {R.or(R.not(R.isEmpty(description)), R.not(R.isEmpty(highlights))) && (
      <CardContent className={classes.cardContent}>
        {R.not(R.isEmpty(description)) && (
          <React.Fragment>
            <Typography variant="body2" align="justify">
              <span dangerouslySetInnerHTML={{ __html: description }} />
            </Typography>
            {R.not(R.isEmpty(highlights)) && <br />}
          </React.Fragment>
        )}
        {R.pipe(
          R.toPairs,
          R.map(([field, highlight]) => (
            <Typography
              variant="body2"
              key={field}
              color="textSecondary"
              className={classes.highlight}
            >
              {field}:{' '}
              {R.map(
                h => (
                  <span key={h} dangerouslySetInnerHTML={{ __html: h }} />
                ),
                highlight,
              )}
            </Typography>
          )),
        )(highlights)}
      </CardContent>
    )}
    <CardActions className={classes.cardActions}>
      <div className={classes.buttonWrapper}>
        <Button
          color="primary"
          size="small"
          onClick={requestDataFile}
          disabled={isDownloading}
          className={classes.button}
        >
          <VerticalAlignBottomIcon />
          <FormattedMessage id="de.visualisation.toolbar.action.download.csv.all" />
        </Button>
        {isDownloading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </div>
    </CardActions>
  </Card>
);

Dataflow.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string,
  description: PropTypes.string,
  indexationDate: PropTypes.string,
  datasourceId: PropTypes.string,
  isDownloading: PropTypes.bool,
  requestDataFile: PropTypes.func.isRequired,
  changeDataflow: PropTypes.func.isRequired,
  highlights: PropTypes.object,
  classes: PropTypes.object.isRequired,
  url: PropTypes.string,
};

export default compose(
  withStyles(styles),
  pure,
)(Dataflow);
