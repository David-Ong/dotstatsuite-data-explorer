import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { compose, withProps, pure } from 'recompose';
import * as R from 'ramda';
import {
  DELayout,
  DELayoutHeader,
  DELayoutFooter,
  DELayoutContent,
  DESearchHeader,
  DELayoutSide,
  DELayoutMain,
  VXFiltersCurrent,
  spotlightScopeListEngine,
  VXScopeList,
  VXSpotlight,
  DESearchDataflows,
  VXFilterContainer,
  DEVisualisationToolbar,
} from '@sis-cc/dotstatsuite-ui-components';
import Header from '../header';
import Footer from '../footer';
import Breadcrumbs from '../breadcrumbs';
import Dataflows from './dataflows';
import Pagination from './pagination';

const Facet = pure(VXScopeList);
const Facets = compose(
  withProps(({ intl }) => ({
    optionsSpotlight: {
      hasClearAll: true,
      mainPlaceholder: intl.formatMessage({ id: 'vx.spotlight.placeholder.primary' }),
      secondaryPlaceholder: intl.formatMessage({ id: 'vx.spotlight.placeholder.secondary' }),
    },
    spotlight: {
      engine: spotlightScopeListEngine,
      placeholder: intl.formatMessage({ id: 'vx.spotlight.placeholder' }),
      fields: {
        'vx.spotlight.field.label': {
          id: 'vx.spotlight.field.label',
          accessor: R.pipe(
            R.propOr(null, 'label'),
            R.ifElse(R.isNil, R.always(''), R.identity),
          ),
          isSelected: true,
        },
      },
    },
    topElementComponent: values => (R.gte(R.length(values), 8) ? VXSpotlight : null),
  })),
)(({ topElementComponent, facets = [], intl, ...parentProps }) =>
  R.map(
    ({ id, label, values, hasPath, isPinned }) => (
      <Facet
        {...parentProps}
        id={id}
        key={id}
        label={`${isPinned ? '*' : ''}${
          R.isNil(label) ? intl.formatMessage({ id: `de.search.${id}` }) : label
        }`}
        items={values}
        hasPath={hasPath}
        TopElementComponent={topElementComponent(values)}
      />
    ),
    facets,
  ),
);

const Results = ({
  isNarrow,
  isRtl,
  isLoading,
  isBlank,
  intl,
  facets,
  constraints,
  changeConstraints,
  term,
  changeTerm,
  changeFacet,
  facet,
  logo,
  actionId,
  changeActionId,
}) => {
  const side = (
    <Fragment>
      <VXFiltersCurrent
        items={constraints}
        changeFilters={changeConstraints}
        titleLabel={<FormattedMessage id="vx.filters.current.title" />}
        clearAllLabel={<FormattedMessage id="vx.filters.current.clear" />}
        blankLabel={<FormattedMessage id="vx.filters.current.blank" />}
        isNarrow={isNarrow}
        isRtl={isRtl}
        noScroll
      />
      <Facets
        intl={intl}
        facets={facets}
        activePanelId={facet}
        onChangeActivePanel={changeFacet}
        changeSelection={changeConstraints}
      />
    </Fragment>
  );

  return (
    <DELayout isNarrow={isNarrow} isRtl={isRtl}>
      <DELayoutHeader>
        <Header isNarrow={isNarrow} isRtl={isRtl} />
        <DESearchHeader
          isNarrow={isNarrow}
          isRtl={isRtl}
          logo={logo}
          term={term || ''}
          changeSpotlight={changeTerm}
          placeholder={intl.formatMessage({ id: 'vx.spotlight.placeholder' })}
        />
      </DELayoutHeader>
      <DELayoutContent>
        <Breadcrumbs />
        {isLoading ? (
          <DESearchDataflows
            isNarrow={isNarrow}
            isRtl={isRtl}
            isLoading
            loadingLabel={<FormattedMessage id="de.search.list.loading" />}
          />
        ) : null}
        {isBlank && !isLoading ? (
          <DESearchDataflows
            isNarrow={isNarrow}
            isRtl={isRtl}
            items={[]}
            blankLabel={<FormattedMessage id="de.search.list.blank" />}
          />
        ) : null}
      </DELayoutContent>
      {isNarrow ? null : R.or(isLoading, isBlank) ? null : <DELayoutSide>{side}</DELayoutSide>}
      {R.or(isLoading, isBlank) ? null : (
        <DELayoutMain>
          {isNarrow ? (
            <DEVisualisationToolbar
              isNarrow
              isRtl={isRtl}
              selectedActionId={actionId}
              actions={[
                {
                  icon: 'filter-list',
                  id: 'filters',
                  label: <FormattedMessage id="de.side.filters.action" />,
                  action: () => changeActionId('filters'),
                },
              ]}
            />
          ) : null}
          {isNarrow ? (
            <VXFilterContainer isOpen={actionId === 'filters'} noScroll noHeader>
              {side}
            </VXFilterContainer>
          ) : null}
          <Dataflows />
          <Pagination />
        </DELayoutMain>
      )}
      <DELayoutFooter>
        <Footer isNarrow={isNarrow} isRtl={isRtl} />
      </DELayoutFooter>
    </DELayout>
  );
};

Results.propTypes = {
  isNarrow: PropTypes.bool,
  isRtl: PropTypes.bool,
  isLoading: PropTypes.bool,
  isBlank: PropTypes.bool,
  intl: PropTypes.object,
  constraints: PropTypes.array,
  facets: PropTypes.array,
  changeConstraints: PropTypes.func,
  term: PropTypes.string,
  changeTerm: PropTypes.func,
  changeFacet: PropTypes.func,
  facet: PropTypes.string,
  logo: PropTypes.string,
  actionId: PropTypes.string,
  changeActionId: PropTypes.func,
};

export default Results;
