import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, branch, renderNothing, withProps } from 'recompose';
import * as R from 'ramda';
import { getCurrentRows, getPage, getPages } from '../../selectors/search';
import { changeStart } from '../../ducks/router';
import { FormattedMessage } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import LastPageIcon from '@material-ui/icons/LastPage';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  container: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  form: {
    alignItems: 'center',
    display: 'flex',
  },
  input: {
    width: 40,
    marginBottom: '0 !important',
    marginTop: '0 !important',
    marginLeft: 5,
    marginRight: 5,
    padding: '0 !important',
    justifyContent: 'center',
    '& fieldset': {
      padding: '0 !important',
    },
    '& input': {
      padding: '0 !important',
      height: 24,
    },
  },
  iconButton: {
    ...theme.iconButton,
  },
});

//= ({ page, pages, onChangePage }) => {
class PageForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: String(props.page) };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  componentWillReceiveProps = props => {
    this.setState({ value: String(props.page) });
  };

  onChange = e => {
    this.setState({ value: e.target.value });
  };

  onSubmit = () => {
    const intValue = parseInt(this.state.value);
    if (
      R.isEmpty(this.state.value) ||
      isNaN(intValue) ||
      R.isNil(this.state.value) ||
      intValue <= 0 ||
      intValue > this.props.pages
    ) {
      this.setState({ value: this.props.page });
    } else {
      this.props.onChangePage(intValue);
    }
  };

  onKeyPress = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      this.onSubmit();
    }
  };

  render = () => {
    return (
      <div className={this.props.classes.form}>
        <FormattedMessage id="de.search.page" />
        <TextField
          className={this.props.classes.input}
          value={this.state.value}
          disabled={this.props.pages === 1}
          onChange={this.onChange}
          onKeyPress={this.onKeyPress}
          type="text"
          variant="outlined"
        />
        <FormattedMessage id="de.search.totalPages" values={{ pages: this.props.pages }} />
      </div>
    );
  };
}

PageForm.propTypes = {
  classes: PropTypes.object,
  page: PropTypes.number,
  pages: PropTypes.number,
  onChangePage: PropTypes.func.isRequired,
};

const Pagination = ({ classes, page, pages, onChangePage, theme }) => {
  return (
    <div className={classes.container}>
      <IconButton
        className={classes.iconButton}
        color="inherit"
        disabled={page === 1}
        disableRipple
        onClick={() => onChangePage(1)}
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        className={classes.iconButton}
        color="inherit"
        disabled={page === 1}
        disableRipple
        onClick={() => onChangePage(page - 1)}
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <PageForm classes={classes} page={page} pages={pages} onChangePage={onChangePage} />
      <IconButton
        className={classes.iconButton}
        color="inherit"
        disabled={page === pages}
        disableRipple
        onClick={() => onChangePage(page + 1)}
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        className={classes.iconButton}
        color="inherit"
        disabled={page === pages}
        disableRipple
        onClick={() => onChangePage(pages)}
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
};

Pagination.propTypes = {
  classes: PropTypes.object,
  page: PropTypes.number,
  pages: PropTypes.number,
  onChangePage: PropTypes.func.isRequired,
  theme: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  page: getPage,
  pages: getPages,
  rows: getCurrentRows,
});

const mapDispatchToProps = {
  changeStart,
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  branch(({ pages, rows }) => R.isNil(rows) || R.isNil(pages) || pages === 1, renderNothing),
  withProps(({ rows, changeStart }) => ({
    onChangePage: page => changeStart(rows * (page - 1)),
  })),
  withStyles(styles, { withTheme: true }),
)(Pagination);
