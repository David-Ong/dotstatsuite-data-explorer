import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, onlyUpdateForKeys, withProps } from 'recompose';
import * as R from 'ramda';
import { FormattedMessage, injectIntl } from 'react-intl';
import {
  VXFilterContainer,
  DETableConfig,
  DEVisualisationToolbar,
  DEConfig,
} from '@sis-cc/dotstatsuite-ui-components';
import {
  changeActionId,
  changeLayout,
  changeDimensionGetter,
  changeIsTimeDimensionInverted,
  downloadExcel,
} from '../ducks/vis';
import {
  getVisActionId,
  getVisDimensionGetter,
  getIsSharing,
  getVisDimensionLayout,
  getVisDimensionFormat,
  getTimeDimensionInverted,
} from '../selectors';
import { getRefAreaDimension } from '../selectors/sdmx';
import { getIsPending } from '../selectors/app';
import { getViewer, getDataflow, getIsRtl } from '../selectors/router';
import { changeViewer } from '../ducks/router';
import { requestDataFile } from '../ducks/sdmx';
import { getDimensionGetters } from '../utils';
import APIQueries from './vis/api-queries';
import Side from './vis-side';
import ShareView from './vis/share';
import { mapOptions } from '../lib/settings';

//-----------------------------------------------------------------------------------------Constants
const API = 'api';
const TABLE = 'table';
const FILTERS = 'filters';
const CONFIG = 'config';
const FULLSCREEN = 'fullscreen';
const DOWNLOAD = 'download';
const SHARE = 'share';
const MENUS = ({ hasRefAreaDimension }) => [
  {
    label: <FormattedMessage id="de.visualisation.toolbar.table" />,
    id: 'table',
  },
  {
    label: <FormattedMessage id="de.visualisation.toolbar.chart" />,
    id: 'chart',
    children: R.concat(
      [
        {
          icon: 'timeline-bar-chart',
          id: 'BarChart',
          label: <FormattedMessage id="de.visualisation.toolbar.chart.bar" />,
        },
        {
          icon: 'horizontal-bar-chart',
          id: 'RowChart',
          label: <FormattedMessage id="de.visualisation.toolbar.chart.row" />,
        },

        {
          icon: 'scatter-plot',
          label: <FormattedMessage id="de.visualisation.toolbar.chart.scatter" />,
          id: 'ScatterChart',
        },
        {
          icon: 'gantt-chart',
          label: <FormattedMessage id="de.visualisation.toolbar.chart.horizontalsymbol" />,
          id: 'HorizontalSymbolChart',
        },
        {
          icon: 'waterfall-chart',
          label: <FormattedMessage id="de.visualisation.toolbar.chart.verticalsymbol" />,
          id: 'VerticalSymbolChart',
        },
        {
          icon: 'timeline-line-chart',
          label: <FormattedMessage id="de.visualisation.toolbar.chart.timeline" />,
          id: 'TimelineChart',
        },
        {
          icon: 'stacked-chart',
          label: <FormattedMessage id="de.visualisation.toolbar.chart.stacked" />,
          id: 'StackedBarChart',
        },
      ],
      hasRefAreaDimension
        ? R.map(
            option => ({
              id: option.id,
              label: (
                <FormattedMessage
                  id="chart.choropleth"
                  values={{
                    map: <FormattedMessage id={`map.${option.mapId}.${option.levelId}`} />,
                  }}
                />
              ),
              icon: 'globe',
              map: R.pick(['mapId', 'levelId'], option),
            }),
            mapOptions,
          )
        : [],
    ),
  },
];
//---------------------------------------------------------------------------------------------Utils
const actions = ({
  changeActionId,
  requestDataFile,
  downloadExcel,
  dataflow,
  isFull,
  isNarrow,
  excelData,
}) => {
  const actions = [
    {
      icon: 'cog',
      id: 'config',
      label: <FormattedMessage id="de.visualisation.toolbar.action.customize" />,
      action: () => changeActionId(CONFIG),
    },
    {
      icon: 'share',
      id: 'share',
      label: <FormattedMessage id="de.visualisation.toolbar.action.share" />,
      action: () => changeActionId(SHARE),
    },
    {
      icon: 'import',
      id: 'download',
      label: <FormattedMessage id="de.visualisation.toolbar.action.download" />,
      children: [
        {
          id: 'excel-selection',
          label: <FormattedMessage id="de.visualisation.toolbar.action.download.excel.selection" />,
          action: () => downloadExcel(excelData),
        },
        {
          id: 'csv-selection',
          label: <FormattedMessage id="de.visualisation.toolbar.action.download.csv.selection" />,
          action: () => requestDataFile({ dataflow }),
        },
        {
          id: 'csv-full',
          label: <FormattedMessage id="de.visualisation.toolbar.action.download.csv.all" />,
          action: () => requestDataFile({ isDownloadAllData: true, dataflow }),
        },
      ],
    },
    {
      icon: 'link',
      id: 'api',
      label: <FormattedMessage id="de.visualisation.toolbar.action.apiqueries" />,
      action: () => changeActionId(API),
    },
    {
      icon: isFull ? 'minimize' : 'maximize',
      id: 'fullscreen',
      label: <FormattedMessage id="de.visualisation.toolbar.action.fullscreen" />,
      action: () => changeActionId(FULLSCREEN),
    },
  ];

  const filtersBtn = {
    icon: 'filter-list',
    id: 'filters',
    label: <FormattedMessage id="de.side.filters.action" />,
    action: () => changeActionId(FILTERS),
  };

  return R.ifElse(R.always(isNarrow), R.prepend(filtersBtn), R.identity)(actions);
};

//-----------------------------------------------------------------------------------------ToolTable
export const ToolTable = compose(
  connect(
    createStructuredSelector({
      dimensionGetter: getVisDimensionGetter(),
      layout: getVisDimensionLayout,
      itemRenderer: getVisDimensionFormat(),
      itemButton: getTimeDimensionInverted(),
    }),
    { changeLayout, changeDimensionGetter, changeIsTimeDimensionInverted },
  ),
  injectIntl,
  onlyUpdateForKeys(['layout', 'isNarrow', 'isRtl', 'itemRenderer', 'itemButton']),
)(
  ({
    isRtl,
    layout,
    changeLayout,
    changeDimensionGetter,
    changeIsTimeDimensionInverted,
    intl,
    dimensionGetter,
    itemRenderer,
    itemButton,
  }) => (
    <DETableConfig
      isRtl={isRtl}
      layout={layout}
      changeLayout={changeLayout}
      dimensionGetter={dimensionGetter}
      dimensionGetters={getDimensionGetters(intl)}
      changeGetter={changeDimensionGetter}
      onChangeItemButton={changeIsTimeDimensionInverted}
      itemRenderer={itemRenderer}
      itemButton={itemButton}
      labels={{
        commit: <FormattedMessage id="de.table.layout.apply" />,
        cancel: <FormattedMessage id="de.table.layout.cancel" />,
        row: <FormattedMessage id="de.table.layout.x" />,
        column: <FormattedMessage id="de.table.layout.y" />,
        section: <FormattedMessage id="de.table.layout.z" />,
        d: <FormattedMessage id="de.table.layout.getter.dimension" />,
        o: <FormattedMessage id="de.table.layout.getter.observation" />,
        time: <FormattedMessage id="de.table.layout.time" />,
        asc: <FormattedMessage id="de.table.layout.time.asc" />,
        desc: <FormattedMessage id="de.table.layout.time.desc" />,
        help: <FormattedMessage id="de.table.layout.help" />,
        table: <FormattedMessage id="de.table.layout.table" />,
        one: <FormattedMessage id="de.table.layout.value.one" />,
      }}
    />
  ),
);

//-------------------------------------------------------------------------------------------ChartConfig
export const Config = compose(
  connect(
    createStructuredSelector({
      display: getVisDimensionGetter(),
    }),
    { changeDimensionGetter },
  ),
  injectIntl,
  withProps(({ changeDimensionGetter, display, intl, properties }) => {
    const labels = {
      focus: intl.formatMessage({ id: 'chart.config.focus' }),
      highlight: intl.formatMessage({ id: 'chart.config.highlights' }),
      select: intl.formatMessage({ id: 'chart.config.select' }),
      baseline: intl.formatMessage({ id: 'chart.config.baseline' }),
      size: intl.formatMessage({ id: 'chart.config.size' }),
      width: intl.formatMessage({ id: 'chart.config.width' }),
      height: intl.formatMessage({ id: 'chart.config.height' }),
      display: intl.formatMessage({ id: 'de.table.layout.getter.dimension' }),
      displayOptions: {
        label: intl.formatMessage({ id: 'vx.config.display.label' }),
        code: intl.formatMessage({ id: 'vx.config.display.code' }),
        both: intl.formatMessage({ id: 'vx.config.display.both' }),
      },
      series: intl.formatMessage({ id: 'chart.config.series' }),
      scatterDimension: intl.formatMessage({
        id: 'chart.config.dimension.x.y.axes',
      }),
      scatterX: intl.formatMessage({
        id: 'chart.config.dimension.values.x.axis',
      }),
      scatterY: intl.formatMessage({
        id: 'chart.config.dimension.values.y.axis',
      }),
      symbolDimension: intl.formatMessage({
        id: 'chart.config.dimension.for.symbols',
      }),
      stackedDimension: intl.formatMessage({
        id: 'chart.config.dimension.x.axis',
      }),
      stackedMode: intl.formatMessage({
        id: 'chart.config.observations.displayed',
      }),
      stackedModeOptions: {
        values: intl.formatMessage({ id: 'chart.config.stacked.values' }),
        percent: intl.formatMessage({ id: 'chart.config.stacked.percent' }),
      },
      axisX: intl.formatMessage({ id: 'chart.config.x.axis' }),
      axisY: intl.formatMessage({ id: 'chart.config.y.axis' }),
      max: intl.formatMessage({ id: 'chart.config.maximum' }),
      min: intl.formatMessage({ id: 'chart.config.minimum' }),
      pivot: intl.formatMessage({ id: 'chart.config.pivot' }),
      step: intl.formatMessage({ id: 'chart.config.step.period' }),
      freqStep: intl.formatMessage({ id: 'chart.config.step' }),
    };
    return {
      labels,
      properties: {
        ...R.omit(['title', 'subtitle', 'sourceLabel'], properties),
        display: {
          id: 'display',
          isActive: true,
          onChange: changeDimensionGetter,
          options: [{ value: 'label' }, { value: 'code' }, { value: 'both' }],
          value: display,
        },
      },
    };
  }),
)(DEConfig);

//-------------------------------------------------------------------------------------------ToolBar
export const ToolBar = compose(
  connect(
    createStructuredSelector({
      isDownloading: getIsPending('requestingDataFile'),
      dataflow: getDataflow,
      isRtl: getIsRtl,
      refAreaDimension: getRefAreaDimension,
    }),
    { changeMenu: changeViewer, changeActionId, requestDataFile, downloadExcel },
  ),
  withProps(
    ({ isDownloading, isSharing, viewerId, actionId, isRtl, refAreaDimension, ...rest }) => ({
      loadingActionId: isDownloading ? DOWNLOAD : isSharing ? 'share' : null,
      selectedMenuId: viewerId,
      selectedActionId: actionId,
      actions: actions({ ...rest, viewerId }),
      menus: MENUS({ hasRefAreaDimension: R.not(R.isNil(refAreaDimension)) }),
      showLabel: <FormattedMessage id="de.visualisation.toolbar.show" />,
      isRtl,
    }),
  ),
)(DEVisualisationToolbar);

//---------------------------------------------------------------------------------------------Tools
export default compose(
  connect(
    createStructuredSelector({
      actionId: getVisActionId(),
      viewerId: getViewer,
      isSharing: getIsSharing(),
    }),
    { changeActionId },
  ),
  withProps(({ actionId, viewerId }) => ({
    isApi: R.equals(API)(actionId),
    isTableConfig: R.equals(CONFIG)(actionId) && R.equals(TABLE)(viewerId),
    isChartConfig: R.equals(CONFIG)(actionId) && !R.equals(TABLE)(viewerId),
    isShare: R.equals(SHARE)(actionId),
    isFilters: R.equals(FILTERS)(actionId),
  })),
)(
  ({
    isNarrow,
    isRtl,
    isFull,
    isApi,
    isTableConfig,
    isChartConfig,
    isFilters,
    isShare,
    actionId,
    changeActionId,
    properties,
    share,
    excelData,
  }) => (
    <Fragment>
      <ToolBar
        isNarrow={isNarrow}
        isRtl={isRtl}
        isFull={isFull}
        actionId={actionId}
        excelData={excelData}
      />
      <VXFilterContainer isOpen={isApi} noScroll noHeader>
        <APIQueries isNarrow={isNarrow} isRtl={isRtl} />
      </VXFilterContainer>
      <VXFilterContainer isOpen={isTableConfig} noScroll noHeader>
        <ToolTable isNarrow={isNarrow} isRtl={isRtl} />
      </VXFilterContainer>
      <VXFilterContainer isOpen={isChartConfig} noScroll noHeader>
        <Config properties={properties} />
      </VXFilterContainer>
      <VXFilterContainer isOpen={isShare} noScroll noHeader>
        <ShareView close={() => changeActionId(SHARE)} share={share} />
      </VXFilterContainer>
      {isNarrow ? (
        <VXFilterContainer isOpen={isFilters} noScroll noHeader>
          <Side isNarrow={isNarrow} isRtl={isRtl} />
        </VXFilterContainer>
      ) : null}
    </Fragment>
  ),
);
