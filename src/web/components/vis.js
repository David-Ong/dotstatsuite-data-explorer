import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps } from 'recompose';
import { FormattedMessage } from 'react-intl';
import {
  DELayout,
  DELayoutContent,
  DELayoutHeader,
  DELayoutFooter,
  DELayoutSide,
  DELayoutMain,
  DESearchHeader,
  VXLoader,
} from '@sis-cc/dotstatsuite-ui-components';
import { getIsFull } from '../selectors';
import { getIsRtl } from '../selectors/router';
import { getIsPending } from '../selectors/app';
import Header from './header';
import Footer from './footer';
import Breadcrumbs from './breadcrumbs';
import Side from './vis-side';
import withResponsiveness from './with-responsiveness';
import * as Settings from '../lib/settings';

import { ParsedDataVisualisation } from './vis/vis-data';

const Vis = ({ isNarrow, isRtl, isLoadingStructure, logo, isFull }) => {
  const loader = key => (
    <VXLoader
      isNarrow={isNarrow}
      isRtl={isRtl}
      loadingLabel={<FormattedMessage id={key} />}
      isLarge
    />
  );

  return (
    <DELayout isNarrow={isNarrow} isRtl={isRtl} isFull={isFull}>
      <DELayoutHeader>
        <Header isNarrow={isNarrow} isRtl={isRtl} />
        <DESearchHeader isNarrow={isNarrow} isRtl={isRtl} logo={logo} onlyLogo />
      </DELayoutHeader>
      <DELayoutContent>
        <Breadcrumbs />
        {isLoadingStructure ? loader('de.visualisation.loading') : null}
      </DELayoutContent>
      {isLoadingStructure || isNarrow ? null : (
        <DELayoutSide>
          <Side isNarrow={isNarrow} isRtl={isRtl} />
        </DELayoutSide>
      )}
      {isLoadingStructure ? null : (
        <DELayoutMain>
          <ParsedDataVisualisation />
        </DELayoutMain>
      )}
      <DELayoutFooter>
        <Footer isNarrow={isNarrow} isRtl={isRtl} />
      </DELayoutFooter>
    </DELayout>
  );
};

Vis.propTypes = {
  isNarrow: PropTypes.bool,
  isRtl: PropTypes.bool,
  isFull: PropTypes.bool,
  isLoadingStructure: PropTypes.bool,
  logo: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  isLoadingStructure: getIsPending('getStructure'),
  isFull: getIsFull(),
  isRtl: getIsRtl,
});

export default compose(
  withResponsiveness,
  connect(mapStateToProps),
  withProps(() => ({
    logo: Settings.getAsset('subheader'),
  })),
)(Vis);
