import React, { Fragment } from "react";
import Responsive from "react-responsive";
//import { Formats } from '@sis-cc/dotstatsuite-ui-components';

//const Narrow = props => <Responsive {...props} query={`(max-width: ${Formats.NARROW}px)`} />;
//const Default = props => <Responsive {...props} query={`(min-width: ${Formats.NARROW+1}px)`} />;
const Narrow = props => (
  <Responsive {...props} query={`(max-width: ${700}px)`} />
);
const Default = props => (
  <Responsive {...props} query={`(min-width: ${700 + 1}px)`} />
);

const withResponsiveness = Component =>
  class extends React.Component {
    render = () => (
      <Fragment>
        <Narrow>
          <Component {...this.props} isNarrow />
        </Narrow>
        <Default>
          <Component {...this.props} />
        </Default>
      </Fragment>
    );
  };

export default withResponsiveness;
