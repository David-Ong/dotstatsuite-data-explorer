import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps, pure } from 'recompose';
import * as R from 'ramda';
import { injectIntl } from 'react-intl';
import {
  DEUsedFilters,
  DEPeriod,
  spotlightScopeListEngine,
  VXScopeList,
  VXSpotlight,
  VXExpansionPanel,
  VXTag,
  DELastNPeriod,
} from '@sis-cc/dotstatsuite-ui-components';
import {
  changeFilter,
  changeDataquery,
  deleteSpecialFilter,
  deleteAllFilters,
  changeFrequencyPeriod,
  changeLastNObservations,
} from '../ducks/router';
import { PANEL_PERIOD, PANEL_USED_FILTERS } from '../utils/constants';
import { getFilter, getLastNObservations } from '../selectors/router';
import {
  getFilters,
  getSelection,
  getFrequencyDimension,
  getFrequencyOptions,
  getFrequency,
  getDatesFromSdmxPeriod,
  getUsedFilterFrequency,
  getUsedFilterPeriod,
} from '../selectors/sdmx';
import { getPeriodCounter, datesBoundaries } from '../lib/sdmx/frequency.js';
import { countNumberOf } from '../utils';
import { START_PERIOD, END_PERIOD, LASTN, addI18nLabels } from '../utils/used-filter';
//--------------------------------------------------------------------------------------------------
export const UsedFilters = compose(
  connect(
    createStructuredSelector({
      items: getSelection,
    }),
    { onDelete: changeDataquery },
  ),
  pure,
)(DEUsedFilters);

export const SpecialUsedFilters = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      frequency: getUsedFilterFrequency,
      period: getUsedFilterPeriod,
    }),
    {
      onDeleteAll: deleteAllFilters,
      onDelete: deleteSpecialFilter,
    },
  ),
  withProps(({ intl, frequency = [], period = [] }) => {
    const periodLabels = {
      [START_PERIOD]: intl.formatMessage({ id: 'de.period.start' }),
      [END_PERIOD]: intl.formatMessage({ id: 'de.period.end' }),
      [LASTN]: [
        intl.formatMessage({ id: 'de.period.last' }),
        intl.formatMessage({ id: 'de.period.periods' }),
      ],
    };

    return {
      items: R.concat(frequency, addI18nLabels(periodLabels)(period)),
      clearAllLabel: intl.formatMessage({ id: 'vx.filters.current.clear' }),
    };
  }),
  pure,
)(DEUsedFilters);

export const FiltersCurrent = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      items1: getSelection,
      frequency: getUsedFilterFrequency,
      period: getUsedFilterPeriod,
    }),
    { onChangeActivePanel: changeFilter },
  ),
  withProps(({ intl, items1 = [], frequency = [], period = [], activePanelId }) => ({
    isOpen: R.equals(PANEL_USED_FILTERS, activePanelId),
    id: PANEL_USED_FILTERS,
    label: intl.formatMessage({ id: 'vx.filters.current.title' }),
    tag: <VXTag>{countNumberOf(R.concat(items1, R.concat(frequency, period)))}</VXTag>,
  })),
)(VXExpansionPanel);

//--------------------------------------------------------------------------------------------------
const Filter = pure(VXScopeList);
export const Filters = compose(
  injectIntl,
  connect(
    createStructuredSelector({ filters: getFilters, activePanelId: getFilter }),
    { changeSelection: changeDataquery, onChangeActivePanel: changeFilter },
  ),
  withProps(({ intl }) => ({
    optionsSpotlight: {
      hasClearAll: true,
      mainPlaceholder: intl.formatMessage({ id: 'vx.spotlight.placeholder.primary' }),
      secondaryPlaceholder: intl.formatMessage({ id: 'vx.spotlight.placeholder.secondary' }),
    },
    spotlight: {
      engine: spotlightScopeListEngine,
      placeholder: intl.formatMessage({ id: 'vx.spotlight.placeholder' }),
      fields: {
        'vx.spotlight.field.label': {
          id: 'vx.spotlight.field.label',
          accessor: R.pipe(
            R.propOr(null, 'label'),
            R.ifElse(R.isNil, R.always(''), R.identity),
          ),
          isSelected: true,
        },
      },
    },
    topElementComponent: values => (R.gte(R.length(values), 8) ? VXSpotlight : null),
  })),
  pure,
)(({ topElementComponent, filters, ...parentProps }) =>
  R.map(
    ({ id, label, values = [] }) => (
      <Filter
        {...parentProps}
        id={id}
        key={id}
        label={label}
        items={values}
        TopElementComponent={topElementComponent(values)}
      />
    ),
    filters,
  ),
);

//--------------------------------------------------------------------------------------------------
export const Period = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      dates: getDatesFromSdmxPeriod,
      frequencyType: getFrequency,
      frequencyDimension: getFrequencyDimension,
      options: getFrequencyOptions,
    }),
    { changeFrequencyPeriod },
  ),
  withProps(
    ({ frequencyDimension, options, frequencyType, dates, changeFrequencyPeriod, intl }) => {
      const freqId = R.prop('id')(frequencyDimension);
      return {
        frequencyType,
        frequencylabel: R.prop('label')(frequencyDimension),
        frequencyoptions: options.ids,
        boundaries: datesBoundaries,
        dates,
        labels: R.mergeRight({
          year: intl.formatMessage({ id: 'de.period.year' }),
          semester: intl.formatMessage({ id: 'de.period.semester' }),
          quarter: intl.formatMessage({ id: 'de.period.quarter' }),
          month: intl.formatMessage({ id: 'de.period.month' }),
          week: intl.formatMessage({ id: 'de.period.week' }),
          hour: intl.formatMessage({ id: 'de.period.hour' }),
          minute: intl.formatMessage({ id: 'de.period.minute' }),
          full_hour: intl.formatMessage({ id: 'de.period.full_hour' }),
          day: intl.formatMessage({ id: 'de.period.day' }),
          end: intl.formatMessage({ id: 'de.period.end' }),
          start: intl.formatMessage({ id: 'de.period.start' }),
        })(options.labels),
        changeFrequency: frequencyType => changeFrequencyPeriod(freqId, frequencyType, dates),
        changeInterval: dates => changeFrequencyPeriod(freqId, frequencyType, dates),
      };
    },
  ),
  pure,
)(DEPeriod);

export const LastNPeriod = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      lastNObservations: getLastNObservations,
    }),
    { onChange: changeLastNObservations },
  ),
  withProps(({ intl }) => ({
    beforeLabel: intl.formatMessage({ id: 'de.period.last' }),
    afterLabel: intl.formatMessage({ id: 'de.period.periods' }),
    popperLabel: intl.formatMessage({ id: 'de.period.helpLastNPeriods' }),
  })),
)(DELastNPeriod);

export const FilterPeriod = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      dates: getDatesFromSdmxPeriod,
      frequency: getFrequency,
    }),
    { changeFilter, onChangeActivePanel: changeFilter },
  ),
  withProps(({ intl, activePanelId, dates, frequency }) => ({
    label: intl.formatMessage({ id: 'de.filter.period.title' }),
    overflow: true,
    id: PANEL_PERIOD,
    isOpen: R.equals(PANEL_PERIOD, activePanelId),
    tag: <VXTag>{getPeriodCounter(frequency, dates)}</VXTag>,
  })),
)(VXExpansionPanel);

//----------------------------------------------------------------------------------------------Side
const Side = ({ isNarrow, isRtl }) => (
  <Fragment>
    <FiltersCurrent isNarrow={isNarrow} isRtl={isRtl}>
      <UsedFilters />
      <SpecialUsedFilters />
    </FiltersCurrent>
    <FilterPeriod isNarrow={isNarrow} isRtl={isRtl}>
      <Period />
      <LastNPeriod />
    </FilterPeriod>
    <Filters isNarrow={isNarrow} isRtl={isRtl} />
  </Fragment>
);

Side.propTypes = {
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
};

export default Side;
