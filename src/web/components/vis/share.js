import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import { createStructuredSelector } from 'reselect';
import { compose } from 'recompose';
import * as R from 'ramda';
import { changeShareMail, changeShareMode } from '../../ducks/vis';
import {
  getHasShared,
  getIsSharing,
  getShareError,
  getShareMail,
  getShareMode,
} from '../../selectors';
import { FormattedMessage } from 'react-intl';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CircularProgress from '@material-ui/core/CircularProgress';
import Clear from '@material-ui/icons/Clear';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import TextField from '@material-ui/core/TextField';

const styles = {
  action: {
    backgroundColor: '#0965C1',
    boxShadow: 'none',
    color: 'white',
    fontSize: 14,
    width: '100%',
  },
  container: {
    padding: 20,
  },
  content: {
    display: 'flex',
    marginTop: 10,
    width: '100%',
  },
  disclaimer: {
    backgroundColor: 'rgba(19, 124, 189, 0.15)',
    boxShadow: 'none',
    fontSize: 14,
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
    width: '50%',
  },
  header: {
    alignItems: 'flex-start',
    boxShadow: 'none',
    display: 'flex',
    marginBottom: 10,
    width: '100%',
  },
  iconButton: {
    backgroundColor: 'transparent',
    color: '#0965C1',
    height: 15,
    marginLeft: 5,
    padding: '0 !important',
    width: 15,
    '&:hover': {
      backgroundColor: 'transparent',
    },
    '&:active': {
      backgroundColor: 'transparent',
    },
  },
  input: {
    marginBottom: 10,
    marginTop: 10,
    padding: '0 !important',
    width: '100%',
    '& fieldset': {
      padding: '5 !important',
    },
    '& input': {
      paddingBottom: '5px !important',
      paddingLeft: '5px !important',
      paddingRight: '5px !important',
      paddingTop: '5px !important',
      height: 24,
    },
  },
  mailAndShare: {
    alignItems: 'flex-start',
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: 10,
    paddingRight: 10,
  },
  modeSelect: {
    backgroundColor: '#ebf1f5',
    boxShadow: 'none',
    padding: 10,
    width: '100%',
  },
  modeContent: {
    padding: '0 !important',
  },
  modeHeader: {
    padding: '0 !important',
  },
  resultHeader: {
    alignItems: 'center',
    display: 'flex',
    padding: '0 !important',
  },
  separator: {
    borderRight: '1px solid #ccc',
    width: '50%',
  },
  success: {
    backgroundColor: 'green',
    color: 'white',
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
  },
  error: {
    backgroundColor: 'red',
    color: 'white',
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
  },
  disableLink: {
    cursor: 'default',
    pointerEvents: 'none',
    color: 'inherit',
  },
};

const ModeSelect = ({ change, classes, value }) => (
  <Card className={classes.modeSelect}>
    <CardHeader className={classes.modeHeader} title={<FormattedMessage id="share.title" />} />
    <CardContent className={classes.modeContent}>
      <RadioGroup onChange={e => change(e.target.value)} value={value}>
        <FormControlLabel
          control={<Radio variant="outlined" />}
          label={<FormattedMessage id="share.snapshot" />}
          value="snapshot"
        />
        <FormControlLabel
          control={<Radio variant="outlined" />}
          label={<FormattedMessage id="share.latest" />}
          value="latest"
        />
      </RadioGroup>
    </CardContent>
  </Card>
);

ModeSelect.propTypes = {
  change: PropTypes.func.isRequired,
  classes: PropTypes.object,
  value: PropTypes.string,
};

const MailAndShare = ({ changeMail, classes, error, hasShared, isSharing, mail, share }) => {
  if (hasShared) {
    return (
      <Card className={R.isNil(error) ? classes.success : classes.error}>
        <div className={classes.resultHeader}>
          <Icon>{R.isNil(error) ? 'check_circle' : 'cancel'}</Icon>
          <FormattedMessage id={R.isNil(error) ? 'share.success.title' : 'share.error.title'} />
        </div>
        <FormattedMessage id={R.isNil(error) ? 'share.success.message' : 'share.error.message'} />
      </Card>
    );
  }
  return (
    <div className={classes.mailAndShare}>
      <FormattedMessage id="share.mail" />
      <TextField
        className={classes.input}
        onChange={e => changeMail(e.target.value)}
        type="text"
        value={R.isNil(mail) ? '' : mail}
        variant="outlined"
      />
      <Button
        className={classes.action}
        disabled={R.isNil(mail) || R.isEmpty(mail) || isSharing || R.isNil(share)}
        onClick={share}
        variant="contained"
      >
        {isSharing ? <CircularProgress /> : <FormattedMessage id="share.action" />}
      </Button>
    </div>
  );
};

MailAndShare.propTypes = {
  changeMail: PropTypes.func.isRequired,
  classes: PropTypes.object,
  error: PropTypes.object,
  hasShared: PropTypes.bool,
  isSharing: PropTypes.bool,
  mail: PropTypes.string,
  share: PropTypes.func.isRequired,
};

const ShareView = ({
  changeMode,
  changeMail,
  classes,
  close,
  error,
  hasShared,
  isSharing,
  mail,
  mode,
  share,
}) => {
  const privacyPolicy = R.pathOr('', ['SETTINGS', 'share', 'policy'], window);
  return (
    <Card className={classes.container}>
      <div className={classes.header}>
        <ModeSelect change={changeMode} classes={classes} value={mode} />
        <IconButton className={classes.iconButton} color="inherit" disableRipple onClick={close}>
          <Clear />
        </IconButton>
      </div>
      <div className={classes.content}>
        <div className={classes.separator}>
          <MailAndShare
            changeMail={changeMail}
            classes={classes}
            error={error}
            hasShared={hasShared}
            isSharing={isSharing}
            mail={mail}
            share={share}
          />
        </div>
        <Paper className={classes.disclaimer}>
          <FormattedMessage
            id="share.disclaimer"
            values={{
              br: <br />,
              link: (
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className={cx({ [classes.disableLink]: R.isEmpty(privacyPolicy) })}
                  href={privacyPolicy}
                >
                  <FormattedMessage id="share.policy.link" />
                </a>
              ),
            }}
          />
        </Paper>
      </div>
    </Card>
  );
};

ShareView.propTypes = {
  changeMail: PropTypes.func.isRequired,
  changeMode: PropTypes.func.isRequired,
  classes: PropTypes.object,
  close: PropTypes.func.isRequired,
  error: PropTypes.object,
  hasShared: PropTypes.bool,
  isSharing: PropTypes.bool,
  mail: PropTypes.string,
  mode: PropTypes.string,
  share: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  error: getShareError(),
  hasShared: getHasShared(),
  isSharing: getIsSharing(),
  mail: getShareMail(),
  mode: getShareMode(),
});

const mapDispatchToProps = {
  changeMail: changeShareMail,
  changeMode: changeShareMode,
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withStyles(styles, { withTheme: false }),
)(ShareView);
