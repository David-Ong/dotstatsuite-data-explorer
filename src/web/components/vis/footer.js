import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Tooltip from './tooltip';

const styles = {
  font: {
    color: '#7A7A7A',
  },
  tooltip: {
    backgroundColor: '#000000',
  },
  imgContainer: {
    paddingLeft: 12,
  },
  gridContainer: {
    padding: '4px 12px',
  },
};

const Footer = ({ tooltip, label, logo, link, linkLabel, height = 25, classes }) => (
  <Grid container direction="row" alignItems="center" spacing={16}>
    <Grid item xs={1} className={classes.gridContainer}>
      <Tooltip
        placement="top"
        interactive
        title={
          <Typography variant="body1">
            <Typography variant="inherit">{R.prop('label')(tooltip)}</Typography>
            <a href={R.prop('link')(tooltip)}>{R.prop('linkLabel')(tooltip)}</a>
          </Typography>
        }
      >
        <Typography variant="body1" className={classes.font}>
          {R.ifElse(R.isNil, R.always('©'), R.identity)(label)}
        </Typography>
      </Tooltip>
    </Grid>
    <Grid item xs={11} className={classes.gridContainer}>
      <Grid container justify="flex-end">
        <Typography variant="body1" className={classes.font}>
          <a href={link}>{linkLabel}</a>
        </Typography>
        <img className={classes.imgContainer} height={height} src={logo} />
      </Grid>
    </Grid>
  </Grid>
);

Footer.propTypes = {
  tooltip: PropTypes.shape({
    link: PropTypes.string,
    linkLabel: PropTypes.string,
    label: PropTypes.string,
  }),
  label: PropTypes.string,
  logo: PropTypes.string,
  link: PropTypes.string,
  linkLabel: PropTypes.string,
  height: PropTypes.number,
  classes: PropTypes.object,
};

export default withStyles(styles)(Footer);
