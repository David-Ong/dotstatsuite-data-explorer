import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

const styles = () => ({
  container: {
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: '#0297C9',
  },
});

export default withStyles(styles)(({ classes }) => <Divider className={classes.container} />);
