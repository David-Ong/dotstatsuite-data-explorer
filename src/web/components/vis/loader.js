import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Header from './header';
import Footer from './footer';
import Divider from './divider';
import { VXLoader } from '@sis-cc/dotstatsuite-ui-components';

const styles = () => ({
  container: {
    marginTop: 80,
    marginBottom: 80,
  },
});

const Loader = ({ header, footer, loader, classes }) => (
  <React.Fragment>
    <Divider />
    <Header header={header} />
    <div className={classes.container}>
      <VXLoader {...loader} isLarge />
    </div>
    <Footer
      tooltip={{
        link: R.path(['terms', 'link'])(footer),
        linkLabel: R.path(['terms', 'label'])(footer),
        label: R.prop('owner')(footer),
      }}
      logo={R.prop('logo')(footer)}
      link={R.prop('source')(footer)}
      linkLabel={R.prop('sourceLabel')(footer)}
    />
  </React.Fragment>
);

Loader.propTypes = {
  header: PropTypes.object,
  footer: PropTypes.object,
  source: PropTypes.object,
  loader: PropTypes.object,
  classes: PropTypes.object,
};

export default withStyles(styles)(Loader);
