import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, pure, withProps, renderNothing, branch } from 'recompose';
import * as R from 'ramda';
import { FormattedMessage } from 'react-intl';
import { DEAPIQueries } from '@sis-cc/dotstatsuite-ui-components';
import { getDataflow } from '../../selectors/router';
import { getDataUrl, getStructureUrl } from '../../selectors/sdmx';
import { viewer } from '../../lib/settings';

export default compose(
  connect(
    createStructuredSelector({
      dataflow: getDataflow,
      flatDataUrl: getDataUrl({ agnostic: false }),
      timeDataUrl: getDataUrl({ agnostic: true }),
      structureUrl: getStructureUrl,
    }),
  ),
  branch(
    R.pipe(
      R.prop('dataflow'),
      R.isEmpty,
    ),
    renderNothing,
  ),
  withProps(({ flatDataUrl, structureUrl, timeDataUrl }) => ({
    queries: [
      {
        id: 'data',
        title: <FormattedMessage id="de.api.queries.label.data" />,
        disclaimer: (
          <FormattedMessage
            id="de.api.queries.notice"
            values={{
              br: <br />,
              link1: (
                <a target="_blank" rel="noopener noreferrer" href={viewer.api.doc}>
                  <FormattedMessage id="de.api.queries.doc" />
                </a>
              ),
              link2: (
                <a target="_blank" rel="noopener noreferrer" href={viewer.api.contact}>
                  <FormattedMessage id="de.api.queries.contact" />
                </a>
              ),
            }}
          />
        ),
        contents: [
          {
            id: 'flat',
            label: <FormattedMessage id="de.api.queries.format.flat" />,
            value: flatDataUrl,
          },
          {
            id: 'time',
            label: <FormattedMessage id="de.api.queries.format.time.series" />,
            value: timeDataUrl,
          },
        ],
      },
      {
        id: 'structure',
        title: <FormattedMessage id="de.api.queries.label.structure" />,
        disclaimer: <FormattedMessage id="de.api.queries.information" />,
        contents: [{ id: 'structure', value: structureUrl }],
      },
    ],
    clipboard: {
      delay: 1000,
      labels: {
        active: <FormattedMessage id="de.api.queries.active" />,
        inactive: <FormattedMessage id="de.api.queries.inactive" />,
      },
    },
  })),
  pure,
)(DEAPIQueries);
