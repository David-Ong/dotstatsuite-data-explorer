import React, { Fragment } from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Lens from '@material-ui/icons/Lens';

const styles = theme => ({
  title: {
    color: '#0297C9',
  },
  subtitle: {
    color: theme.palette.text.secondary,
    lineHeight: 1.3,
  },
  icon: {
    height: '0.35em',
    color: theme.palette.text.secondary,
  },
});

const Header = ({ header, classes }) => (
  <div className={classes.container}>
    <Typography variant="h5" className={classes.title}>
      {R.path(['title', 'label'], header)}
    </Typography>
    <Typography variant="subtitle1" className={classes.subtitle}>
      {R.pipe(
        R.propOr([], 'subtitle'),
        R.intersperse(<Lens className={classes.icon} />),
        R.addIndex(R.map)((v, index) => (
          <Fragment key={R.toString([R.prop('title')(header), v, index])} />
        )),
      )(header)}
    </Typography>
    <Typography variant="subtitle1" className={classes.subtitle}>
      {R.prop('uprs')(header)}
    </Typography>
  </div>
);

Header.propTypes = {
  header: PropTypes.shape({
    title: PropTypes.object,
    subtitle: PropTypes.array,
    uprs: PropTypes.string,
  }),
  classes: PropTypes.object,
};

export default withStyles(styles)(Header);
