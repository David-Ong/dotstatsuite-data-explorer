import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, mapProps, withProps } from 'recompose';
import * as R from 'ramda';
import { RulesDriver } from '@sis-cc/dotstatsuite-components';
import { VXNoData } from '@sis-cc/dotstatsuite-ui-components';
import { FormattedMessage } from 'react-intl';
import Tools from '../vis-tools';
import Table from '../vis-table';
import Chart from '../vis-chart';
import { share } from '../../ducks/vis';
import {
  getShareMode,
  getTimeDimensionInverted,
  getVisDataDimensions,
  getVisTableLayout,
  getVisDimensionGetter,
  getTableProps,
  getHeaderProps,
  getVisChoroMap,
  getVisIsLoadingMap,
} from '../../selectors';
import { getIsPending } from '../../selectors/app';
import { getViewer, getDataflow } from '../../selectors/router';
import { getData, getDataUrl, getTimelineAxisProc } from '../../selectors/sdmx';
import * as Settings from '../../lib/settings';
import withResponsiveness from '../with-responsiveness';
import { withLocale } from '../../i18n';
import withRefinedViewerProps from '../../utils/viewer';
import Loader from './loader';

const Viewer = ({ isNonIdealState, isNonIdealMapState, type, ...props }) => {
  if (isNonIdealState) {
    return <VXNoData message={<FormattedMessage id="vx.no.data" />} />;
  }
  if (isNonIdealMapState) {
    return <VXNoData message={<FormattedMessage id="vx.no.map" />} />;
  }
  if (type === 'table') {
    return <Table {...props} />;
  }
  return <Chart type={type} {...props} />;
};

const DataVisView = ({
  isLoadingData,
  isLoadingMap,
  isNarrow,
  isRtl,
  isFull,
  toolsProps,
  viewerProps,
}) => {
  const loaderId = R.cond([
    [R.always(isLoadingData), R.always('de.visualisation.data.loading')],
    [R.always(isLoadingMap), R.always('de.vis.map.loading')],
    [R.T, R.identity],
  ])(null);
  return (
    <div>
      <Tools
        isNarrow={isNarrow}
        isRtl={isRtl}
        isFull={isFull}
        {...toolsProps}
        excelData={viewerProps}
      />
      {loaderId ? (
        <Loader
          header={viewerProps.header}
          loader={{
            isNarrow,
            isRtl,
            loadingLabel: <FormattedMessage id={loaderId} />,
          }}
          footer={{
            ...viewerProps.chartConfig,
            ...R.prop('footnotes')(viewerProps),
            ...R.path(['chartData', 'footnotes'])(viewerProps),
          }}
        />
      ) : (
        <Viewer {...viewerProps} />
      )}
    </div>
  );
};

const DataVis = compose(
  connect(
    createStructuredSelector({
      dataflow: getDataflow,
      dimensions: getVisDataDimensions(),
      isLoadingData: getIsPending('getData'),
      isLoadingMap: getVisIsLoadingMap,
      layoutIds: getVisTableLayout(),
      shareMode: getShareMode(),
      timeDimensionInverted: getTimeDimensionInverted(),
      tableProps: getTableProps(),
      headerProps: getHeaderProps(),
    }),
    { share },
  ),
  withProps(({ data, dataflow, locale, tableProps, sdmxUrl }) => ({
    customAttributes: Settings.customAttributes,
    dataflowId: dataflow.dataflowId,
    isNotATable: R.hasPath(['cells', '', '', '', 0], tableProps),
    source: sdmxUrl,
    isNonIdealState: R.isNil(data),
    locale: R.pathOr({ id: locale }, ['i18n', 'locales', locale], Settings),
    sourceHeaders: {
      // to move into Settings or selectors
      Accept: 'application/vnd.sdmx.data+json;version=1.0.0-wd',
      'Accept-Language': locale,
    },
  })),
  withRefinedViewerProps,
  withResponsiveness,
)(DataVisView);

export const ParsedDataVisualisation = compose(
  connect(
    createStructuredSelector({
      display: getVisDimensionGetter(),
      type: getViewer,
      data: getData,
      timelineAxisProc: getTimelineAxisProc,
      dataUrl: getDataUrl({ agnostic: false }),
      map: getVisChoroMap,
    }),
  ),
  withLocale,
  mapProps(({ dataUrl, localeId, timelineAxisProc, map, type, ...rest }) => {
    const isNonIdealMapState = R.allPass([
      R.always(type === 'ChoroplethChart'),
      R.anyPass([R.isNil, R.has('error')]),
    ])(map);
    return {
      sdmxUrl: dataUrl,
      formaterIds: {
        decimals: Settings.getSdmxAttribute('decimals'),
        prefscale: Settings.getSdmxAttribute('prefscale'),
      },
      locale: localeId,
      options: R.when(
        R.always(type === 'TimelineChart'),
        R.set(R.lensPath(['axis', 'x', 'format', 'proc']), timelineAxisProc),
      )(Settings.chartOptions),
      source: { link: window.location.href },
      map: R.when(R.always(isNonIdealMapState), R.always(null))(map),
      type,
      isNonIdealMapState,
      ...rest,
    };
  }),
)(props => (
  <RulesDriver
    {...props}
    render={(
      parsedProps, // chartData, chartOptions, properties (aka chart configs props)
    ) => (
      <DataVis
        {...parsedProps}
        {...R.pick(
          ['data', 'display', 'formaterIds', 'locale', 'type', 'isNonIdealMapState', 'sdmxUrl'],
          props,
        )}
      />
    )}
  />
));
