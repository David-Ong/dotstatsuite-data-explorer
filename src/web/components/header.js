import React from 'react';
import PropTypes from 'prop-types';
import { keys } from 'ramda';
import { FormattedMessage } from 'react-intl';
import { compose, withProps } from 'recompose';
import { DEHeader } from '@sis-cc/dotstatsuite-ui-components';
import { withLocale } from '../i18n';
import { getAsset, locales } from '../lib/settings';

const Header = ({ isNarrow, isRtl, availableLocales, localeId, changeLocale, logo }) => (
  <DEHeader
    availableLocales={availableLocales}
    locale={localeId}
    changeLocale={changeLocale}
    isNarrow={isNarrow}
    isRtl={isRtl}
    LabelRenderer={FormattedMessage}
    logo={logo}
  />
);

Header.propTypes = {
  isNarrow: PropTypes.bool,
  isRtl: PropTypes.bool,
  availableLocales: PropTypes.arrayOf(PropTypes.string),
  localeId: PropTypes.string,
  changeLocale: PropTypes.func,
  logo: PropTypes.string,
};

export default compose(
  withProps({ logo: getAsset('header'), availableLocales: keys(locales) }),
  withLocale,
)(Header);
