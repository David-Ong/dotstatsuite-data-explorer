import { connect } from "react-redux";
import { compose, withProps } from "recompose";
import { injectIntl } from "react-intl";
import { createStructuredSelector } from "reselect";
import { stringify } from "qs";
import * as R from "ramda";
import { SharePopup } from "@sis-cc/dotstatsuite-ui-components";
import {
  getIsSharing,
  getShareIsOpen,
  getShareMode,
  getShareResultUrl
} from "../selectors";
import { closeSharePopup } from "../ducks/vis";

const mapStateToProps = createStructuredSelector({
  isFetching: getIsSharing(),
  isOpen: getShareIsOpen(),
  mode: getShareMode(),
  url: getShareResultUrl()
});

const mapDispatchToProps = {
  onClose: closeSharePopup
};

const dimensionParsing = val => (R.isNil(val) || R.is(NaN, val) ? "100%" : val);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  injectIntl,
  withProps(({ height, intl, title, url, width }) => {
    const labels = {
      "embed.code": intl.formatMessage({ id: "share.embed.code" }),
      "embed.disclaimer": intl.formatMessage({
        id: "share.use.code.to.embed.visualisation"
      }),
      "mode.latest": intl.formatMessage({ id: "share.latest.available.data" }),
      "mode.snapshot": intl.formatMessage({ id: "share.snapshot" }),
      preview: intl.formatMessage({ id: "share.preview.embedded" }),
      "sharing.options": intl.formatMessage({ id: "share.sharing.options" }),
      title: intl.formatMessage({ id: "share.title" }),
      url: intl.formatMessage({ id: "share.permanent.url" }),
      "url.disclaimer": intl.formatMessage({ id: "share.copy.url" })
    };

    const facebookOutput = () => {
      const uf = stringify({ u: url });
      window.open(`https://www.facebook.com/sharer/sharer.php?${uf}`, "_blank");
    };

    const twitterOutput = () => {
      const ut = stringify({ text: `${title}\n${url}` });
      window.open(`https://twitter.com/intent/tweet?${ut}`, "_blank");
    };

    const mailOutput = () => {
      const subject = encodeURIComponent(title);
      const body = encodeURIComponent(`${title}\n${url}`);
      window.location.href = `mailto:?subject=${subject}&body=${body}`;
    };

    const onPreview = () =>
      window.open(url, "preview", `resizable=yes,scrollbars=yes,status=yes`);

    const medias = [
      { id: "facebook", label: "Facebook", onClick: facebookOutput },
      { id: "twitter", label: "Twitter", onClick: twitterOutput },
      { id: "email", label: "E-Mail", onClick: mailOutput }
    ];

    return {
      height: dimensionParsing(height),
      isLatestDisabled: false,
      labels,
      medias,
      onPreview,
      width: dimensionParsing(width)
    };
  })
)(SharePopup);
