import * as R from 'ramda';
import axios from 'axios';
import { configParser, searchParser } from '../lib/search';

let globalConfig = {};
/* eslint-disable-line no-shadow */
const setConfig = config => (globalConfig = { ...globalConfig, ...config });

const endpoint = (path, config = globalConfig) => `${config.endpoint}${path}`;

const getConfig = ({ requestArgs }) =>
  axios.post(endpoint(`/config`), requestArgs).then(
    R.pipe(
      R.prop('data'),
      configParser,
    ),
  );

const getSearch = ({ requestArgs, parserArgs }) =>
  axios.post(endpoint(`/search`), requestArgs).then(
    R.pipe(
      R.prop('data'),
      searchParser(parserArgs),
    ),
  );

const methods = { setConfig, getConfig, getSearch };

const error = method => () => {
  throw new Error(`Unkown method: ${method}`);
};
const main = ({ method, ...rest }) => (methods[method] || error(method))(rest);
R.compose(
  R.forEach(([name, fn]) => (main[name] = fn)),
  R.toPairs,
)(methods);

export default main;
