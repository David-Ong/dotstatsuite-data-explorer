import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import '@babel/polyfill';
import * as R from 'ramda';
import qs from 'qs';
import { I18nProvider } from './i18n';
import { history, Switch } from './router';
import { createStore, reducer } from './core';
import sagas from './sagas';
import { ThemeProvider } from './theme';
import Vis from './components/vis';
import Search from './components/search';
import ErrorBoundary from './components/error';
import { changeLocation } from './ducks/router';
import { getLocation } from './selectors/router';
import '@blueprintjs/core/dist/blueprint.css';
import { initialize as initializeAnalytics } from './utils/analytics';
import { initialize as initializeI18n } from './i18n';
import searchApi from './api/search';
import * as Settings from './lib/settings';
import meta from '../../package.json';

console.info(`${meta.name}@${meta.version}`); // eslint-disable-line no-console

const initialState = {};
const store = createStore(initialState, history, Settings.ga, reducer, sagas);
const routes = { '/': Search, '/vis': Vis };

const historian = (location, action) => {
  const prevPathname = R.prop('pathname', getLocation(store.getState()));
  const nextPathname = R.prop('pathname')(location);
  if (action !== 'POP' && R.equals(prevPathname, nextPathname)) return;
  const params = qs.parse(R.prop('search')(location), { ignoreQueryPrefix: true, comma: true });
  store.dispatch(changeLocation(location, params));
};

searchApi.setConfig(Settings.search);

initializeAnalytics(Settings.ga);
initializeI18n(Settings.i18n);

history.listen(historian);
historian(history.location, 'POP');

render(
  <ErrorBoundary isFinal>
    <Provider store={store}>
      <ThemeProvider theme={Settings.theme}>
        <I18nProvider messages={window.I18N}>
          <ErrorBoundary>
            <Switch routes={routes} />
          </ErrorBoundary>
        </I18nProvider>
      </ThemeProvider>
    </Provider>
  </ErrorBoundary>,
  document.getElementById('root'),
);
