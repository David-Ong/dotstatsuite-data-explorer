import axios from 'axios';
import * as R from 'ramda';
import { getShareMail, getVisChoroMap } from '../selectors';
import { getMap as getRouterMap } from '../selectors/router';
import * as Settings from '../lib/settings';
import { HANDLE_STRUCTURE } from './sdmx';
import {
  CHANGE_DATAFLOW,
  CHANGE_DATAQUERY,
  CHANGE_FREQUENCY_PERIOD,
  CHANGE_FILTER,
  CHANGE_VIEWER,
  RESET_DATAFLOW,
} from './router';
import { createExcelFile } from '../xlsx';

//-----------------------------------------------------------------------------------------constants
export const CSV_SELECTION = 'CSV_SELECTION';
export const CSV_FULL = 'CSV_FULL';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  actionId: undefined,
  layout: undefined,
  isFull: false,
  dimensionGetter: 'label',
  timeDimensionInverted: {}, // table only
  title: undefined,
  share: {
    error: undefined,
    hasShared: false,
    isSharing: false,
    mail: undefined,
    mode: 'snapshot',
  },
  map: null,
  isLoadingMap: false,
});

//-------------------------------------------------------------------------------------------actions
export const CHANGE_ACTION_ID = '@@vis/CHANGE_ACTION_ID';
export const SHARE_CHART = '@@vis/SHARE_CHART';
export const CHANGE_LAYOUT = '@@vis/CHANGE_LAYOUT';
export const CHANGE_DIMENSION_GETTER = '@@vis/CHANGE_DIMENSION_GETTER';
export const CHANGE_IS_TIME_DIMENSION_INVERTED = '@@vis/CHANGE_IS_TIME_DIMENSION_INVERTED';
export const SHARING = '@@vis/SHARING';
export const SHARE_SUCCESS = '@@vis/SHARE_SUCCESS';
export const SHARE_ERROR = '@@vis/SHARE_ERROR';
export const CHANGE_SHARE_MODE = '@@vis/CHANGE_SHARE_MODE';
export const CHANGE_SHARE_MAIL = '@@vis/CHANGE_SHARE_MAIL';
export const LOADING_MAP = '@@vis/LOADING_MAP';
export const LOAD_MAP_SUCCESS = '@@vis/LOAD_MAP_SUCCESS';
export const LOAD_MAP_ERROR = '@@vis/LOAD_MAP_ERROR';
export const DOWNLOADING_EXCEL = '@@vis/DOWNLOADING_EXCEL';
export const DOWNLOAD_EXCEL_SUCCESS = '@@vis/DOWNLOAD_EXCEL_SUCCESS ';
export const DOWNLOAD_EXCEL_ERROR = '@@vis/DOWNLOAD_EXCEL_ERROR';

//--------------------------------------------------------------------------------------thunks (api)
export const changeMode = (mode, data, options) => dispatch => {
  dispatch(changeShareMode(mode));
  dispatch(share(data, options));
};

export const share = data => (dispatch, getState) => {
  dispatch(sharing());
  const email = getShareMail()(getState());
  const endpoint = Settings.shareEndpoint;
  const confirmUrl = Settings.shareConfirmUrl;
  try {
    if (R.isNil(endpoint) || R.isNil(confirmUrl)) {
      throw new Error('missing settings entries');
    }
    const body = {
      confirmUrl,
      data,
      email,
    };
    axios
      .post(endpoint, body)
      .then(() => dispatch(shareSuccess()))
      .catch(error => dispatch(shareError(error)));
  } catch (error) {
    dispatch(shareError(error));
  }
};

export const loadMap = () => (dispatch, getState) => {
  dispatch(loadingMap());
  const { mapId, levelId } = getRouterMap(getState());
  const choroMap = getVisChoroMap(getState());
  if (
    !R.isNil(choroMap) &&
    R.prop('id', choroMap) === mapId &&
    R.prop('areaSelection', choroMap) === levelId
  ) {
    dispatch(loadMapSuccess(choroMap));
    return;
  }
  try {
    if (R.isNil(mapId) || R.isNil(levelId)) {
      throw new Error('missing map entries');
    }
    const map = Settings.getMap(mapId);
    if (R.isNil(map)) {
      throw new Error('inexistant map entry');
    }
    axios
      .get(map.path)
      .then(res => res.data)
      .then(topology => ({ ...map, topology }))
      .then(R.set(R.lensProp('areaSelection'), levelId))
      .then(_map => dispatch(loadMapSuccess(_map)));
  } catch (error) {
    dispatch(loadMapError(error));
  }
};

export const downloadExcel = props => dispatch => {
  dispatch({ type: DOWNLOADING_EXCEL });
  return createExcelFile(props)
    .then(() => dispatch({ type: DOWNLOAD_EXCEL_SUCCESS }))
    .catch(error => dispatch({ type: DOWNLOAD_EXCEL_ERROR, payload: { error } }));
};

//------------------------------------------------------------------------------------------creators
export const changeActionId = actionId => ({
  type: CHANGE_ACTION_ID,
  isVis: true,
  payload: { actionId },
});
export const shareChart = ({ media, sharedData, shareResponse }) => ({
  type: SHARE_CHART,
  payload: { label: media, sharedData, shareResponse },
});
export const changeLayout = layout => ({
  type: CHANGE_LAYOUT,
  isVis: true,
  payload: { layout },
});
export const changeDimensionGetter = getter => ({
  type: CHANGE_DIMENSION_GETTER,
  isVis: true,
  payload: { getter },
});
export const changeIsTimeDimensionInverted = (id, isTimeDimensionInverted) => ({
  type: CHANGE_IS_TIME_DIMENSION_INVERTED,
  isVis: true,
  payload: { id, isTimeDimensionInverted },
});
export const sharing = () => ({
  type: SHARING,
  payload: {},
});
export const shareSuccess = () => ({
  type: SHARE_SUCCESS,
  payload: {},
});
export const shareError = error => ({
  type: SHARE_ERROR,
  payload: { error },
});
export const changeShareMode = mode => ({
  type: CHANGE_SHARE_MODE,
  payload: { mode },
});
export const changeShareMail = value => ({
  type: CHANGE_SHARE_MAIL,
  payload: { value },
});
export const loadingMap = () => ({
  type: LOADING_MAP,
  payload: {},
});
export const loadMapSuccess = map => ({
  type: LOAD_MAP_SUCCESS,
  payload: { map },
});
export const loadMapError = error => ({
  type: LOAD_MAP_ERROR,
  payload: { error },
});

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case HANDLE_STRUCTURE:
      return R.pipe(
        R.set(R.lensProp('layout'), R.path(['structure', 'layout'], action)),
        R.set(R.lensProp('title'), R.path(['structure', 'title'], action)),
      )(state);
    case CHANGE_ACTION_ID:
      var isFull = () => R.equals(action.payload.actionId, 'fullscreen');
      return R.evolve({
        isFull: R.ifElse(isFull, R.not, R.identity),
        actionId: R.cond([
          [isFull, R.identity],
          [R.equals(action.payload.actionId), R.always(undefined)],
          [R.T, R.always(action.payload.actionId)],
        ]),
      })(state);
    case CHANGE_LAYOUT:
      return { ...state, layout: action.payload.layout, actionId: undefined, share: model().share };
    case CHANGE_DIMENSION_GETTER:
      return { ...state, dimensionGetter: action.payload.getter, share: model().share };
    case CHANGE_IS_TIME_DIMENSION_INVERTED:
      // true asc - false desc
      return {
        ...state,
        timeDimensionInverted: R.set(
          R.lensProp(action.payload.id),
          !action.payload.isTimeDimensionInverted,
          state.timeDimensionInverted,
        ),
        share: model().share,
      };
    case LOADING_MAP:
      return { ...state, isLoadingMap: true };
    case LOAD_MAP_SUCCESS:
      return { ...state, map: action.payload.map, isLoadingMap: false };
    case LOAD_MAP_ERROR:
      return { ...state, map: { error: action.payload.error }, isLoadingMap: false };
    case SHARING:
      return { ...state, share: { ...state.share, isSharing: true } };
    case SHARE_SUCCESS:
      return {
        ...state,
        share: {
          ...state.share,
          hasShared: true,
          isSharing: false,
        },
      };
    case SHARE_ERROR:
      return {
        ...state,
        share: {
          ...state.share,
          hasShared: true,
          isSharing: false,
          error: action.payload.error,
        },
      };
    case CHANGE_SHARE_MODE:
      return {
        ...state,
        share: { ...state.share, mode: action.payload.mode, hasShared: false, error: undefined },
      };
    case CHANGE_SHARE_MAIL:
      return { ...state, share: { ...state.share, mail: action.payload.value } };
    case CHANGE_DATAQUERY:
    case CHANGE_FREQUENCY_PERIOD:
    case CHANGE_FILTER:
    case CHANGE_VIEWER:
    case RESET_DATAFLOW:
      return { ...state, share: model().share };
    case CHANGE_DATAFLOW:
      return model();
    default:
      return state;
  }
};
