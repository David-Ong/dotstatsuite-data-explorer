import reducer, {
  model,
  CHANGE_LOCATION,
  CHANGE_LOCALE,
  CHANGE_TERM,
  CHANGE_CONSTRAINTS,
  CHANGE_FACET,
  CHANGE_START,
  CHANGE_DATAFLOW,
  RESET_DATAFLOW,
  CHANGE_DATAQUERY,
  CHANGE_LAST_N_OBS,
  CHANGE_FILTER,
  DEFAULT_FILTER,
  CHANGE_VIEWER,
  CHANGE_FREQUENCY_PERIOD,
  changeTerm,
  changeConstraints,
  changeFacet,
  changeStart,
} from '../router';

jest.mock('../../lib/settings', () => ({ getLocaleId: v => v, theme: { visFont: '' } }));

describe('router actions', () => {
  it('should handle action changeTerm', () => {
    const action = {
      type: CHANGE_TERM,
      payload: { term: 'toto' },
      pushHistory: '/',
      request: 'getSearch',
    };
    expect(changeTerm({ term: 'toto' })).toEqual(action);
  });

  it('should handle action changeConstraints', () => {
    const action = {
      type: CHANGE_CONSTRAINTS,
      payload: { facetId: 'titi', constraintId: 'toto' },
      pushHistory: '/',
      request: 'getSearch',
    };
    expect(changeConstraints('titi', 'toto')).toEqual(action);
  });

  it('should handle action changeFacet', () => {
    const action = {
      type: CHANGE_FACET,
      payload: { facetId: 'titi' },
      pushHistory: '/',
    };
    expect(changeFacet('titi')).toEqual(action);
  });

  it('should handle action changeStart', () => {
    const action = {
      type: CHANGE_START,
      payload: { start: 'tutu' },
      pushHistory: '/',
      request: 'getSearch',
    };
    expect(changeStart('tutu')).toEqual(action);
  });
});
