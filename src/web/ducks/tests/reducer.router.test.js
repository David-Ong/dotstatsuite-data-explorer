import reducer, {
  model,
  CHANGE_LOCATION,
  CHANGE_LOCALE,
  CHANGE_TERM,
  CHANGE_CONSTRAINTS,
  CHANGE_START,
  CHANGE_DATAFLOW,
  RESET_DATAFLOW,
  CHANGE_DATAQUERY,
  CHANGE_LAST_N_OBS,
  CHANGE_FILTER,
  DEFAULT_FILTER,
  CHANGE_VIEWER,
  CHANGE_FREQUENCY_PERIOD,
} from '../router';
jest.mock('../../lib/settings', () => ({ getLocaleId: v => v, theme: { visFont: '' } }));

describe('router reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      location: {},
      params: {
        term: undefined,
        constraints: {},
        facet: undefined,
        locale: undefined,
        dataflow: undefined,
        filter: undefined,
        dataquery: undefined,
        period: undefined,
        viewer: undefined,
        start: 0,
        lastNObservations: undefined,
      },
    });
  });

  it('should handle CHANGE_LOCATION', () => {
    const action = {
      type: CHANGE_LOCATION,
      payload: {
        location: {},
        params: {
          constraints: [['datasource_id', 'ILO']],
        },
      },
    };
    const state = model();
    const expected = {
      ...state,
      location: {},
      params: {
        constraints: {
          '40842cc3ff899f180c4d7d8bb53e2522': { facetId: 'datasource_id', constraintId: 'ILO' },
        },
        locale: undefined,
      },
    };
    expect(reducer(state, action)).toEqual(expected);
  });

  it('should handle CHANGE_LOCALE', () => {
    const action = {
      type: CHANGE_LOCALE,
      payload: { localeId: 'fr' },
    };
    expect(reducer({}, action)).toEqual({ params: { locale: 'fr', start: 0 } });
  });

  it('should handle CHANGE_TERM', () => {
    const action = {
      type: CHANGE_TERM,
      payload: { term: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { term: 'toto', start: 0 } });
  });

  it('should handle CHANGE_CONSTRAINTS', () => {
    const action = (facetId, constraintId) => ({
      type: CHANGE_CONSTRAINTS,
      payload: { facetId, constraintId },
    });
    expect(reducer({}, action('toto', 'tutu'))).toEqual({
      params: {
        facet: 'toto',
        constraints: {
          e4f8e3fe05117ff3bb412250fe6551a8: { constraintId: 'tutu', facetId: 'toto' },
        },
        start: 0,
      },
    });
  });

  it('should handle CHANGE_START', () => {
    const action = {
      type: CHANGE_START,
      payload: { start: 5 },
    };
    expect(reducer({}, action)).toEqual({ params: { start: 5 } });
  });

  it('should handle CHANGE_DATAFLOW', () => {
    const action = {
      type: CHANGE_DATAFLOW,
      payload: { dataflow: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { dataflow: 'toto' } });
  });

  it('should handle RESET_DATAFLOW', () => {
    const action = {
      type: RESET_DATAFLOW,
    };
    expect(reducer({}, action)).toEqual({ params: {} });
  });

  it('should handle CHANGE_DATAQUERY', () => {
    const action = {
      type: CHANGE_DATAQUERY,
      payload: { dataquery: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { dataquery: 'toto' } });
  });

  it('should handle CHANGE_LAST_N_OBS', () => {
    const action = {
      type: CHANGE_LAST_N_OBS,
      payload: { value: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { lastNObservations: 'toto' } });
  });

  it('should handle CHANGE_FILTER', () => {
    const action = {
      type: CHANGE_FILTER,
      payload: { filterId: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { filter: 'toto' } });
  });

  it('should handle CHANGE_VIEWER', () => {
    const action = {
      type: CHANGE_VIEWER,
      payload: { viewerId: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { viewer: 'toto' } });
  });

  it('should handle CHANGE_FREQUENCY_PERIOD', () => {
    const action = {
      type: CHANGE_FREQUENCY_PERIOD,
      payload: { period: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { period: 'toto' } });
  });
});
