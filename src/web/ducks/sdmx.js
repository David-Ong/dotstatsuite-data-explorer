import * as R from 'ramda';
import FileSaver from 'file-saver';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import { getFullDataflowFileRequestArgs, getDataRequestArgs } from '../selectors/sdmx';
import { setPending, pushLog, LOG_ERROR } from './app';
import { getVisDimensionGetter } from '../selectors';
import { getFilename } from '../lib/sdmx';

const isDev = process.env.NODE_ENV === 'development';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  dimensions: [],
  data: undefined,
  layout: {},
});

//-----------------------------------------------------------------------------------------constants
export const HANDLE_STRUCTURE = '@@sdmx/HANDLE_STRUCTURE';
export const REQUEST_STRUCTURE = '@@sdmx/REQUEST_STRUCTURE';
export const PARSE_STRUCTURE = '@@sdmx/PARSE_STRUCTURE';
export const HANDLE_DATA = '@@sdmx/HANDLE_DATA';
export const REQUEST_DATA = '@@sdmx/REQUEST_DATA';
export const PARSE_DATA = '@@sdmx/PARSE_DATA';
export const FLUSH_DATA = '@@sdmx/FLUSH_DATA';

//--------------------------------------------------------------------------------------thunks (api)
const apiCall = ({ method, args, pendingId }) => {
  const error = () => {
    throw new Error(`Unkown method: ${pendingId}`);
  };

  return R.is(Function, method) ? method(args) : error();
};

const request = (dispatch, ctx) => {
  const { pendingId } = ctx;

  // eslint-disable-next-line no-console
  if (isDev) console.info(`request: ${pendingId}`);

  dispatch(setPending(pendingId, true));
  return apiCall(ctx)
    .then(res => {
      dispatch(setPending(pendingId));
      return res;
    })
    .catch(error => {
      const log = error.response
        ? { errorCode: error.response.data.errorCode, statusCode: error.response.status }
        : { error };

      dispatch(setPending(pendingId));
      dispatch(pushLog({ type: LOG_ERROR, log }));

      // required to break the promised chain
      throw error;
    });
};

export const requestDataFile = ({ isDownloadAllData, dataflow } = {}) => (dispatch, getState) => {
  const args = isDownloadAllData
    ? getFullDataflowFileRequestArgs(dataflow)(getState())
    : getDataRequestArgs(getState());
  const format = 'csv';
  const labels = R.ifElse(R.equals('code'), R.identity, R.always('both'))(
    getVisDimensionGetter()(getState()),
  );

  const pendingId = R.ifElse(
    R.has('id'),
    R.pipe(
      R.prop('id'),
      id => `getDataFile/${id}`,
    ),
    R.always('requestingDataFile'),
  )(dataflow);
  return request(dispatch, {
    method: SDMXJS.getDataFile,
    args: { ...args, format, labels },
    pendingId,
  }).then(response => {
    const blob = new Blob([R.prop('data')(response)], {
      type: R.pathOr('text/csv', ['headers', 'content-type'])(response),
    });
    FileSaver.saveAs(blob, `${getFilename(args)}.csv`);
  });
};

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case FLUSH_DATA:
      return R.set(R.lensProp('data'), undefined, state);
    case HANDLE_STRUCTURE:
      return R.pipe(
        R.set(R.lensProp('dimensions'), R.path(['structure', 'dimensions'], action)),
        R.set(R.lensProp('timePeriod'), R.path(['structure', 'timePeriod'], action)),
      )(state);
    case HANDLE_DATA:
      return R.set(R.lensProp('data'), R.prop('data', action), state);
    default:
      return state;
  }
};
