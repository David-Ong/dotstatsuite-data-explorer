import * as R from 'ramda';

//------------------------------------------------------------------------------constants
export const LOG_ERROR = 'LOG_ERROR';

//----------------------------------------------------------------------------------model
export const model = () => ({
  logs: [],
  pending: {},
});

//--------------------------------------------------------------------------------actions
export const PUSH_LOG = '@@app/PUSH_LOG';
export const SET_PENDING = '@@app/SET_PENDING';

//-------------------------------------------------------------------------------creators
export const pushLog = log => ({ type: PUSH_LOG, payload: { log } });
export const setPending = (id, is) => ({ type: SET_PENDING, payload: { id, is } });

//--------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case PUSH_LOG:
      return R.over(R.lensProp('logs'), R.prepend(action.payload.log), state);
    case SET_PENDING:
      return R.assocPath(['pending', action.payload.id], action.payload.is, state);
    default:
      return state;
  }
};
