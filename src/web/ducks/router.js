import * as R from 'ramda';
import md5 from 'md5';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import { RESET_SEARCH } from './search';
import { HANDLE_STRUCTURE } from './sdmx';
import { getLocaleId } from '../lib/settings';
import { setLocale } from '../i18n';
import { history } from '../router';
import { getDataquery, getLocation } from '../selectors/router';
import { getDimensions, getPeriod } from '../selectors/sdmx';
import { getSdmxPeriod } from '../lib/sdmx/frequency';
import { START_PERIOD, END_PERIOD, LASTN } from '../utils/used-filter';
import { PANEL_PERIOD } from '../utils/constants';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  location: {},
  params: {
    term: undefined, // term: '' (search)
    constraints: {}, // list of constraints: {hash: {facetId, constraintId}} (search)
    facet: undefined, // id of the expanded facet: '' (search)
    start: 0, // first index of the dataflow to returned (search)
    locale: getLocaleId(), // id of the current locale
    dataflow: undefined, // dataflow identifiers: {datasourceId, dataflowId, agencyId, version} (sdmx)
    filter: undefined, // id of the expanded filter: '' (vis)
    dataquery: undefined, // dataquery: '' (sdmx)
    period: undefined, // period: [start, end] (sdmx)
    lastNObservations: undefined, // query parameter: '(integer)' (sdmx)
    viewer: undefined, // id of the vis component to display: '' (vis)
    map: undefined,
  },
});

//-----------------------------------------------------------------------------------------constants
export const CHANGE_LOCATION = '@@router/CHANGE_LOCATION';
export const CHANGE_LOCALE = '@@router/CHANGE_LOCALE';
export const CHANGE_TERM = '@@router/search/CHANGE_TERM';
export const CHANGE_CONSTRAINTS = '@@router/search/CHANGE_CONSTRAINTS';
export const CHANGE_FACET = '@@router/search/CHANGE_FACET';
export const CHANGE_START = '@@router/search/CHANGE_START';
export const CHANGE_DATAFLOW = '@@router/sdmx/CHANGE_DATAFLOW';
export const RESET_DATAFLOW = '@@router/sdmx/RESET_DATAFLOW';
export const CHANGE_DATAQUERY = '@@router/sdmx/CHANGE_DATAQUERY';
export const CHANGE_LAST_N_OBS = '@@router/sdmx/CHANGE_LAST_N_OBS';
export const CHANGE_FILTER = '@@router/vis/CHANGE_FILTER';
export const DEFAULT_FILTER = '@@router/vis/DEFAULT_FILTER';
export const CHANGE_VIEWER = '@@router/vis/CHANGE_VIEWER';
export const CHANGE_FREQUENCY_PERIOD = '@@router/vis/CHANGE_FREQUENCY_PERIOD';
//------------------------------------------------------------------------------------------creators
export const changeLocation = (location, params) => (dispatch, getState) => {
  const defaultAction = { type: CHANGE_LOCATION, payload: { location, params } };

  const prevLocation = getLocation(getState());
  const isFromAnotherPath = prevLocation =>
    R.pipe(
      R.prop('pathname'),
      R.both(R.equals('/'), R.complement(R.equals)(R.prop('pathname', prevLocation))),
    );
  const hasSearchChanged = prevLocation => location =>
    R.both(
      R.pipe(
        R.pluck('search'),
        R.complement(R.equals),
      ),
      R.all(
        R.pipe(
          R.prop('pathname'),
          R.equals('/'),
        ),
      ),
    )([prevLocation, location]);
  const shouldRequestSearch = R.either(
    isFromAnotherPath(prevLocation),
    hasSearchChanged(prevLocation),
  )(location);

  if (shouldRequestSearch) return dispatch(R.assoc('request', 'getSearch', defaultAction));

  if (R.not(R.propEq('pathname', '/vis', location))) return dispatch(defaultAction);
  dispatch(
    R.assoc(
      'request',
      R.pipe(
        R.prop('pathname'),
        R.either(R.isNil, R.equals('/')),
      )(getLocation(getState()))
        ? 'getStructure'
        : 'getData',
    )(defaultAction),
  );
};
export const changeTerm = ({ term } = {}) => ({
  type: CHANGE_TERM,
  payload: { term },
  pushHistory: '/',
  request: 'getSearch',
});
export const changeConstraints = (facetId, constraintId) => ({
  type: CHANGE_CONSTRAINTS,
  payload: { facetId, constraintId },
  pushHistory: '/',
  request: 'getSearch',
});
export const changeFacet = facetId => ({
  type: CHANGE_FACET,
  payload: { facetId },
  pushHistory: '/',
});
export const changeStart = start => ({
  type: CHANGE_START,
  payload: { start },
  pushHistory: '/',
  request: 'getSearch',
});

export const changeLocale = localeId => {
  setLocale(localeId);
  const pushHistory = R.path(['location', 'pathname'], history);
  return R.omit([R.equals('/vis')(pushHistory) ? null : 'request'], {
    type: CHANGE_LOCALE,
    payload: { localeId },
    pushHistory,
    request: 'getStructure',
  });
};

export const changeDataflow = dataflow => ({
  type: CHANGE_DATAFLOW,
  payload: { dataflow },
  pushHistory: '/vis',
});
export const resetDataflow = () => ({
  type: RESET_DATAFLOW,
  pushHistory: '/',
});
export const changeFilter = filterId => ({
  type: CHANGE_FILTER,
  payload: { filterId },
  pushHistory: '/vis',
});
export const changeDataquery = (filterId, valueId) => (dispatch, getState) => {
  const dimensions = getDimensions(getState());
  const dataquery = getDataquery(getState());
  dispatch({
    type: CHANGE_DATAQUERY,
    payload: {
      dataquery: SDMXJS.updateDataquery(dimensions, dataquery, filterId, valueId),
    },
    pushHistory: '/vis',
    request: 'getData',
  });
};
export const changeViewer = (id, option) => ({
  type: CHANGE_VIEWER,
  payload: {
    viewerId: R.has('map', option) ? 'ChoroplethChart' : id,
    map: R.prop('map', option),
  },
  pushHistory: '/vis',
  request: R.has('map', option) ? 'getMap' : null,
});

export const changeLastNObservations = value => ({
  type: CHANGE_LAST_N_OBS,
  payload: { value },
  pushHistory: '/vis',
  request: 'getData',
});
export const changeFrequencyPeriod = (filterId, valueId, date) => dispatch => {
  const period = R.or(R.isEmpty(date), R.isNil(date)) ? [undefined, undefined] : date;
  // need request getData, and pushHistory: /vis, actually trigger by changeDataquery
  dispatch({
    type: CHANGE_FREQUENCY_PERIOD,
    payload: {
      period: R.map(getSdmxPeriod(valueId))(period),
    },
  });
  dispatch(changeDataquery(filterId, valueId));
};
export const deleteSpecialFilter = (filterId, id) => (dispatch, getState) => {
  if (R.equals(LASTN, id)) return dispatch(changeLastNObservations('0'));
  if (R.isNil(id)) {
    dispatch(changeLastNObservations('0'));
  }
  const changePeriod = () => {
    const getPayloadPeriod = periods => {
      if (R.equals(START_PERIOD, id)) return [undefined, R.last(periods)];
      if (R.equals(END_PERIOD, id)) return [R.head(periods), undefined];
      return [undefined, undefined];
    };
    return {
      type: CHANGE_FREQUENCY_PERIOD,
      payload: { period: getPayloadPeriod(getPeriod(getState())) },
      pushHistory: '/vis',
      request: 'getData',
    };
  };
  dispatch(changePeriod());
};
export const deleteAllFilters = () => dispatch => {
  dispatch(changeLastNObservations('0'));
  dispatch(changeFrequencyPeriod());
};
//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case CHANGE_LOCATION:
      return R.pipe(
        R.set(R.lensProp('location'), R.path(['payload', 'location'], action)),
        R.set(
          R.lensProp('params'),
          R.evolve(
            {
              constraints: R.reduce(
                (memo, [facetId, constraintId]) =>
                  R.assoc(md5(`${facetId}${constraintId}`), { facetId, constraintId }, memo),
                {},
              ),
            },
            R.pipe(
              R.path(['payload', 'params']),
              R.mergeRight({ locale: getLocaleId() }),
            )(action), // apply default, could *also* be useful for evolve transformations
          ),
        ),
      )(state);
    case CHANGE_FREQUENCY_PERIOD:
      return R.pipe(R.over(R.lensProp('params'), R.flip(R.mergeRight)(R.prop('payload', action))))(
        state,
      );
    case CHANGE_TERM:
      return R.pipe(
        R.over(R.lensProp('params'), R.flip(R.mergeRight)(R.prop('payload', action))),
        R.set(R.lensPath(['params', 'start']), 0),
      )(state);
    case CHANGE_DATAQUERY:
      return R.over(R.lensProp('params'), R.flip(R.mergeRight)(R.prop('payload', action)), state);
    case CHANGE_FACET:
      return R.over(
        R.lensPath(['params', 'facet']),
        facet =>
          R.ifElse(R.equals(facet), R.always(undefined), R.identity)(
            R.path(['payload', 'facetId'], action),
          ),
        state,
      );
    case CHANGE_START:
      return R.set(R.lensPath(['params', 'start']), R.path(['payload', 'start'], action), state);
    case CHANGE_FILTER:
      return R.over(
        R.lensPath(['params', 'filter']),
        filterId => {
          const nextFilterId = R.path(['payload', 'filterId'], action);
          if (R.all(R.isNil)([filterId, nextFilterId])) return PANEL_PERIOD;
          if (R.equals(filterId, nextFilterId)) return undefined;
          return nextFilterId;
        },
        state,
      );
    case CHANGE_VIEWER:
      return R.pipe(
        R.set(R.lensPath(['params', 'viewer']), R.path(['payload', 'viewerId'], action)),
        R.set(R.lensPath(['params', 'map']), R.path(['payload', 'map'], action)),
      )(state);
    case CHANGE_CONSTRAINTS:
      var { facetId, constraintId } = action.payload;

      if (R.isNil(facetId)) {
        // undefined facetId <=> no constraint
        return R.pipe(
          R.set(R.lensPath(['params', 'facet']), null),
          R.set(R.lensPath(['params', 'constraints']), {}),
          R.set(R.lensPath(['params', 'start']), 0),
        )(state);
      }

      if (R.isNil(constraintId)) {
        // undefined constraintId <=> remove facet from constraints
        return R.pipe(
          R.set(R.lensPath(['params', 'facet']), null),
          R.over(R.lensPath(['params', 'constraints']), R.reject(R.propEq('facetId', facetId))),
          R.set(R.lensPath(['params', 'start']), 0),
        )(state);
      }

      var hash = md5(`${facetId}${constraintId}`);
      return R.pipe(
        R.set(R.lensPath(['params', 'facet']), facetId),
        R.ifElse(
          R.hasPath(['params', 'constraints', hash]),
          R.over(R.lensPath(['params', 'constraints']), R.dissoc(hash)),
          R.over(R.lensPath(['params', 'constraints']), R.assoc(hash, { facetId, constraintId })),
        ),
        R.set(R.lensPath(['params', 'start']), 0),
      )(state);
    case RESET_SEARCH:
      return R.over(R.lensProp('params'), R.pick(['locale', 'tenant']), state);
    case CHANGE_LOCALE:
      return R.pipe(
        R.set(
          R.lensPath(['params', 'locale']),
          getLocaleId(R.path(['payload', 'localeId'], action)),
        ),
        R.over(R.lensProp('params'), R.omit(['term', 'constraints', 'facet'])),
        R.set(R.lensPath(['params', 'start']), 0),
      )(state);
    case CHANGE_DATAFLOW:
      return R.pipe(
        R.over(R.lensProp('params'), R.omit(['viewer'])),
        R.over(R.lensProp('params'), R.flip(R.merge)(R.prop('payload', action))),
      )(state);
    case RESET_DATAFLOW:
      return R.over(
        R.lensProp('params'),
        R.omit(['dataflow', 'dataquery', 'filter', 'lastNObservations', 'period', 'viewer']),
        state,
      );
    case HANDLE_STRUCTURE:
      return R.over(
        R.lensProp('params'),
        R.mergeRight(R.pathOr({}, ['structure', 'params'], action)),
      )(state);
    case CHANGE_LAST_N_OBS:
      return R.set(
        R.lensPath(['params', 'lastNObservations']),
        R.path(['payload', 'value'], action),
      )(state);
    default:
      return state;
  }
};
