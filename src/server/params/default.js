import path from 'path';

const config = {
  appId: 'data-explorer',
  env: process.env.NODE_ENV || 'development',
  isProduction: process.env.NODE_ENV === 'production',
  gitHash: process.env.GIT_HASH,
  publicPath: path.join(__dirname, '../../../public'),
  buildPath: path.join(__dirname, '../../../build'),
};

module.exports = config;
