import debug from 'debug';
import cors from 'cors';
import compression from 'compression';
import path from 'path';
import logger from 'morgan-debug';
import express from 'express';
import bodyParser from 'body-parser';
import { healthcheckConnector } from '../services/healthcheck/connector';
import errorHandler from '../middlewares/errors';
import tenant from '../middlewares/tenant';
import ssr from '../ssr';

const loginfo = debug('webapp:express');

const init = ctx => {
  const app = express();
  const {
    services: { healthcheck },
    config,
    configProvider,
  } = ctx;

  app.use(cors({ origin: 'http://localhost' }));
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  app.get('/robots.txt', (req, res) => res.sendFile(path.resolve(__dirname, '../robots.txt')));
  app.use(compression());
  app.use(express.static(path.join(__dirname, '../../../public')));
  app.use(express.static(path.join(__dirname, '../../../build')));
  app.use(tenant(configProvider));
  app.get('/api/healthcheck', healthcheckConnector(healthcheck));
  app.use(logger('webapp:http', 'dev'));
  app.use(ssr(ctx));
  app.use(errorHandler);

  loginfo('routes setup');
  return Promise.resolve({ ...ctx, app });
};

module.exports = init;
