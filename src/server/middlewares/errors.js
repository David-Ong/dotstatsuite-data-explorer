const error = (err, req, res, next) => {
  if (!err) return next();
  if(process.env.NODE_ENV !== 'test')console.error(err.stack); // eslint-disable-line no-console
  const code = err.code || 500;
  return res
    .status(code)
    .json({ message: err.message, code: code, type: err.type });
};

module.exports = error;
