import '@babel/polyfill';
import { pathOr, zipObj, keys, pipe, assocPath, pick } from 'ramda';
import htmlescape from 'htmlescape';
import debug from 'debug';
import { getLanguage } from '../utils/language';

const loginfo = debug('webapp');

const renderHtml = (config, assets, i18n, settings, stylesheetUrl, app) => {
  return `
    <!doctype html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="${app.favicon}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/spinner/echo.css">
        <link rel="stylesheet" href="/css/preloader.css">
        <link rel="stylesheet" href="/static/css/vendors~main.chunk.css">
        <style id="insertion-point-jss"></style>
        <link rel="stylesheet" href="${stylesheetUrl}">
        <title>${app.title}</title>
        <script> CONFIG = ${htmlescape(config)} </script>
        <script> SETTINGS = ${htmlescape(settings)} </script>
        <script> I18N = ${htmlescape(i18n)} </script>
      </head>
      <body>
        <noscript>
          You need to enable JavaScript to run this app.
        </noscript>
        <div id="root">
          <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
          </div>
        </div>
        <script type="text/javascript" src="${assets.vendors}"></script>
        <script type="text/javascript" src="${assets.main}"></script>
      </body>
    </html>
  `;
};

const ssr = ({ config, assets, configProvider }) => async (req, res) => {
  const { tenant } = req;
  const settings = await configProvider.getSettings(tenant);
  const datasources = await configProvider.getDataSources(tenant);
  const tenantDatasources = pick(pathOr([], ['sdmx', 'datasourceIds'], settings), datasources);
  const locales = pipe(
    pathOr([], ['i18n', 'locales']),
    keys,
  )(settings);
  const i18n = await configProvider.getI18n(tenant, locales);
  const stylesheetUrl = settings.styles;
  const html = renderHtml(
    {
      tenant,
      env: config.env,
    },
    assets,
    zipObj(locales, i18n),
    assocPath(['sdmx', 'datasources'], tenantDatasources, settings),
    stylesheetUrl,
    {
      title: pathOr('Data Explorer', ['app', 'title'], settings),
      favicon: pathOr('/favicon.ico', ['app', 'favicon'], settings),
    },
  );
  res.send(html);
  loginfo(`render site for tenant '${tenant.name}'`);
};

export default ssr;
