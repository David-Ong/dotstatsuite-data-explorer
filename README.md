# Webapp Template 

This project is a boilerplate to help crafting web application fully integrated within `kube-rp`'s wonderful world

In the project directory, you can run:

* gitOps pipeline: Continuous CI/CD driven by gitlab
* proxy server: a nodejs reverse proxy to target deployed apps and set a token depending of a request [host, path]
* config server: service to get multi tenants settings 
* redis server:
* mongodb server
* sFs server: SDMX Facetted Search service

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Multitenant

`webapp` is designed to be used by multiple clients, each may require specific configurations, translations and may be UI (theme).

This is where multi tenant principle takes place, depending on a uniq ID supplied when getting the app, returned html will inlude tenant's data. 

To that end, webapp project includes a nodeJS backend to dynamically generate html (Partial Server Side rendering) with data embedded in script tags.

When webapp starts, data are available to be processed without any later requests to the backend.


## Proxy

On can request the webapp with an explicit client ID, but it's not very friendly and error prone.

Best way is to proceed in 2 transparent steps: 

1. request webapp via a dedicated url, that point to an external service definition, without an ID, 
2. route request to webapp internal service with the right ID

Proxy server is in charge of this transformation, it acts as a reverse proxy between internet and a webapp (or any other targeted app)

It's mainly a router that map incoming urls to internals ones associated with an ID

see https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-proxy


## Config

To ease configurations management and avoid to include tenant's setup in all webapps, a config server centralize all required resources used by final webapps.

config server is like an internal bucket shared by internal services to perform tenant's webapp cutomisations.

see https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config


## Webapp technical workflow sample

![archi](./docs/archi.png)

1. Browser requests https://kube-rp.oecd.com/staging/webapp (an alias kube-rp's external static IP address) 
2. Kubernetes ingress service routes request to proxy service
3. proxy was setup to map `siscc.oecd.com` with tenant `SISCC`
4. proxy was setup to map `webapp.staging` to the `webapp` service in `staging` name space of `kube-rp`
5. `webapp` nodeJS server receives request `http://webapp?tenant=SISCC` 
6. `webapp` server requests `config` server to get [`/SISCC/webapp/i18n/<lang>.json`, `/SISCC/webapp/settings.json`, `/SISCC/webapp/theme.json`]
7. `webapp` server returns `index.html` + previous data + webapp's local config (sdmx faceted search server, ...)
8. `webapp`single page application is mounted and welcome SISCC's user in his preferred locale with it's preferred UI theme for a wonderful personnal and dedicated experience ...


NB:

* `webapp` is designed to use a default tenant `default`
* `tenant` is extracted from  as the query params `tenant` or as the `x-tenant` header key (case where proxy is setup to inject it)
* if you use `default` tenant care to have `/configs/default/<appId>` available in config's setup and a `default` entry in `/configs/partners.json`

## Usage

To run your future app you need to start webapp server in development mode and to have an access to a config server instance.

First start a config server:


```
$ docker run -d --name config --restart always -p 5007:80 siscc/dotstatsuite-config-dev:latest
```

If unlikely you cannot run docker images (why?) on your development workstation, see how to [run config server](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config) from source.

Clone the repo.

In development mode, you need hot reload implemented via webpack dev server, so start:

```
$ yarn start:proxy
```

This launch a proxy server with webpackDevServer embedded.

Now start `webapp` server:

```
$ yarn start:srv
...
webapp server started
```

Launch your preferred browser on http://localhost:7000

If it doesn't work, check log messages

## Local setup

Config params are located in `src/params/<process.env.NODE_ENV>.js` file.

You can overwrite existing values by setting shell variables like this:

```
$ SERVER_PORT=9999 yarn start:srv
```

or by adding variables in `.env` file

## hub.docker

webapp's image name is `siscc/dotstatsuite-webapp-template`

see https://hub.docker.com/?namespace=siscc

## Tests

```
$ yarn test --coverage
```

## API

webapp backend exposes `/api/healcheck` probe:

```
$ curl http://localhost:7000/api/healthcheck | json_pp 
 {
    "status" : "OK",
    "configProvider" : "OK",
    "tenant" : {
      "label" : "default",
      "key" : "default"
    },
    "startTime" : "2019-01-26T14:49:47.133Z"
 }
```
